#ifndef _ENG_IDENTITY_H_PRIVATE_
#define _ENG_IDENTITY_H_PRIVATE_

#include "engineer.h"

#define REMOTE_MASK REMOTE_MAX
#define THREAD_MASK THREAD_MAX
#define ENTITY_MASK ENTITY_MAX

typedef enum   Eng_Thread_Order  Eng_Thread_Order;
typedef struct Eng_Thread_Status Eng_Thread_Status;
typedef struct Eng_Entity_Status Eng_Entity_Status;

enum Eng_Identity_Bounds
{
   FAIL_ERROR = 0x0000000000000000,
   REMOTE_MIN = 0x0000000000000001,
   REMOTE_MAX = 0x000000000000FFFF,
   THREAD_MIN = 0x0000000000010000,
   THREAD_MAX = 0x000000000001FFFF,
   ENTITY_MIN = 0x0000000000020000,
   ENTITY_MAX = 0xFFFFFFFFFFFFFFFF,
};

enum Eng_Thread_Order  // The order, from top to bottom, in which each queue is emptied.
{
   PRIORITY_AWAKE            =  0,

   PRIORITY_BUSY_PLEA_RESULT =  1,
   PRIORITY_BUSY_PLEA_ORDER  =  2,
   PRIORITY_BUSY_CALL_RESULT =  3,
   PRIORITY_BUSY_CALL_ORDER  =  4,
   PRIORITY_BUSY_TASK_RESULT =  5,
   PRIORITY_BUSY_TASK_ORDER  =  6,

   PRIORITY_LAZY_PLEA_RESULT =  7,
   PRIORITY_LAZY_PLEA_ORDER  =  8,
   PRIORITY_LAZY_CALL_RESULT =  9,
   PRIORITY_LAZY_CALL_ORDER  = 10,
   PRIORITY_LAZY_TASK_RESULT = 11,
   PRIORITY_LAZY_TASK_ORDER  = 12,

   PRIORITY_ASLEEP           = 13,
   PRIORITY_LOOKUP           = 14,
};

void _eng_identity_boot();
void _eng_identity_halt();

Thread _eng_thread_take_id();
Bool   _eng_thread_give_id(Thread identity);

Indx _eng_thread_attach_reference(Thread identity);
Indx _eng_thread_detach_reference(Thread identity);

Eng_Thread_Order _eng_thread_priority(Thread identity, Eng_Thread_Order new_priority);
Eng_Thread_Order _eng_thread_priority_next(Thread identity, Eng_Thread_Order current);
Bool             _eng_thread_is_alive(Thread identity);
Bool             _eng_thread_is_defunct(Thread identity);
Bool             _eng_thread_terminate(Thread identity);

Entity _eng_entity_take_id(Thread residence);
Bool   _eng_entity_give_id(Entity identity);

Indx _eng_entity_attach_reference(Entity target);
Indx _eng_entity_detach_reference(Entity target);

Thread _eng_entity_residence(Entity target);
Bool   _eng_entity_immigrate(Entity target, Thread residence);
Bool   _eng_entity_terminate(Entity target);

#endif

