#ifndef _ENG_SCENE_H_PRIVATE_
#define _ENG_SCENE_H_PRIVATE_

#include "eng_node.h"

typedef struct Eng_Scene_Module    Eng_Scene_Module;
typedef struct Eng_Scene_Component Eng_Scene_Component;
typedef struct Eng_Scene_System    Eng_Scene_System;

// This is a redundant "overlay" struct used for accessing Component and System Scene_Module
//    structs with the same language.
struct Eng_Scene_Module
{
   #define ENG_SCENE_MODULE_HEADER(module_flavor)                                        \
      Eng_Node_##module_flavor *node;                                                    \
                                                                                         \
      Eina_Hash *dependent_systems;  /* Keyed by Digest, contains Eng_Scene_System*s. */ \
      Set       *dependent_shards;   /* Elements are Shard identities.                */ \

   ENG_SCENE_MODULE_HEADER(Module);
};           // *dependent_systems also used when an Entity has a Component attached.

struct Eng_Scene_Component // Attaches to Thread.
{
   ENG_SCENE_MODULE_HEADER(Component);  // See source/library/engineer.h for definition.
};

struct Eng_Scene_System
{
   ENG_SCENE_MODULE_HEADER(System);

   void *vault;

   Eina_Bool running;
};


/*** Scene Internal API ***/

void * _eng_scene_init(void *data, Eina_Thread id);
void   _eng_scene_free();
Bool   _eng_scene_update(void *data);

void   _eng_shard_update_system(Set *dependent_shards, Digest system);
Thread _eng_scene_sort_entity(Entity target);  // Returns a Shard identity.

Bool _eng_scene_attach_shard(Thread scene, Thread shard);
Bool _eng_scene_detach_shard(Thread scene, Thread shard);

Eng_Scene_Module * _eng_scene_has_module(Thread scene, Digest module);
Eng_Scene_System * _eng_scene_module_has_system(Eng_Scene_Module *module, Digest system);

Eng_Scene_Module * _eng_scene_module_attach_shard(Thread scene, Digest module, Thread shard);
Bool               _eng_scene_module_detach_shard(Thread scene, Digest module, Thread shard);

Bool _eng_scene_system_load(Thread scene, Digest system, Digest dependent_system);
Bool _eng_scene_system_free(Thread scene, Digest system, Digest dependent_system);

Bool _eng_scene_domain_load(Thread scene, String *domain);
Bool _eng_scene_domain_free(Thread scene, String *domain);

#endif

