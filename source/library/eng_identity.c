#include "eng_identity.h"

typedef struct Eng_Identity_Data Eng_Identity_Data;

struct Eng_Identity_Data
{
   Eina_RWLock unused_ids_mutex;
   Set        *unused_thread_ids;
   Set        *unused_entity_ids;

   Eina_RWLock   thread_mutex;
   Eina_Inarray *thread_index;  // Stores Eng_Thread_Status struct, indexed by Thread ID.

   Eina_RWLock   entity_mutex;
   Eina_Inarray *entity_index;  // Stores an Eng_Entity_Status struct, indexed by Entity ID.
};

// We presume that any system we would care to run this on has 64-bit pointers and double-pointer
//   width atomics.
struct Eng_Thread_Status
{
   uint64_t generation;  //
   uint16_t priority;    // Stores the current strata of eina_queue the thread is working on.
   uint16_t is_alive;    //
   uint32_t census;      // The number of Work_Order(s) that are referencing this Thread.
};

struct Eng_Entity_Status
{
   uint64_t generation;  //
   uint16_t residence;   // Tracks on which Thread this Entity is currently stored/running on.
   uint8_t  is_alive;    //
 //uint8_t  is_local;    // Used when an Entity is removed from local memory to a remote host/Node.
   uint32_t census;      // Tracks how many Work_Orders(s) & Components are attached to this Entity.
};

static Eng_Identity_Data *id;


/*** Library Bootstrapping ***/

ENG_PRIVATE void
_eng_identity_boot()
{
   Thread thread_min = THREAD_MIN, thread_max = THREAD_MAX;
   Entity entity_min = ENTITY_MIN, entity_max = ENTITY_MAX;

   id = malloc(sizeof(struct Eng_Identity_Data));

   eina_rwlock_new(&id->unused_ids_mutex);
   id->unused_thread_ids = eng_set_create(64, 1, VOID);
   id->unused_entity_ids = eng_set_create(64, 1, VOID);

   eng_set_attach_interval(id->unused_thread_ids, &thread_min, &thread_max);
   eng_set_attach_interval(id->unused_entity_ids, &entity_min, &entity_max);

   eina_rwlock_new(&id->thread_mutex);
   id->thread_index = eina_inarray_new(sizeof(Eng_Thread_Status), VOID);

   eina_rwlock_new(&id->entity_mutex);
   id->entity_index = eina_inarray_new(sizeof(Eng_Entity_Status), VOID);
}

ENG_PRIVATE void
_eng_identity_halt()
{
   eina_inarray_free(id->entity_index);
   eina_rwlock_free(&id->entity_mutex);

   eina_inarray_free(id->thread_index);
   eina_rwlock_free(&id->thread_mutex);

   eng_set_delete(id->unused_entity_ids);
   eng_set_delete(id->unused_thread_ids);
   eina_rwlock_free(&id->unused_ids_mutex);

   free(id);
}


/*** Thread Identity Management ***/

ENG_PRIVATE Thread
_eng_thread_take_id()
{
   // Grab an Entity ID off the bottom of the selected unallocated ID pool.
   eina_rwlock_take_write(&id->unused_ids_mutex);

   Thread *smallest = eng_set_find_smallest(id->unused_thread_ids);
   if(!smallest) goto FAILURE;

   eng_set_detach(id->unused_thread_ids, smallest);
   Thread identity = *smallest;

   eina_rwlock_release(&id->unused_ids_mutex);

   // Add our newly created thread data to its respective global reference index.
   Eng_Thread_Status *status, _status = { VOID, PRIORITY_ASLEEP, EINA_TRUE, 1 };

   eina_rwlock_take_write(&id->thread_mutex);

   status = eina_inarray_nth(id->thread_index, identity - THREAD_MIN);
   if(status) *status = _status;
   else eina_inarray_push(id->thread_index, &_status);

/* SUCCESS : */ eina_rwlock_release(&id->thread_mutex);      return identity;
   FAILURE :    eina_rwlock_release(&id->unused_ids_mutex);  return VOID;
}

ENG_PRIVATE Bool
_eng_thread_give_id(Thread identity)
{
   // Check to see if the given Thread identity can be returned, then pop off the last value of
   //    thread_index if that value is the same as our given Thread identity.
   eina_rwlock_take_write(&id->thread_mutex);

   Eng_Thread_Status *status = eina_inarray_nth(id->thread_index, identity - THREAD_MIN);
   if(!status || status->census) goto FAILURE;

   Indx thread_population = eina_inarray_count(id->thread_index);
   if(identity - THREAD_MIN == thread_population - INDEXING_OFFSET)
      do {
         eina_inarray_pop(id->thread_index);
         status = eina_inarray_nth(id->thread_index, --thread_population);
      }
      while(!status->census);

   eina_rwlock_release(&id->thread_mutex);

   // Return our no longer used Thread identity to the appropriate pool.
   eina_rwlock_take_write(&id->unused_ids_mutex);

   eng_set_attach(id->unused_thread_ids, &identity);

/* SUCCESS : */ eina_rwlock_release(&id->unused_ids_mutex);  return EINA_TRUE;
   FAILURE :    eina_rwlock_release(&id->thread_mutex);      return EINA_FALSE;
}

ENG_PRIVATE Indx
_eng_thread_attach_reference(Thread identity)
{
   ATOMIC Eng_Thread_Status *status;
          Eng_Thread_Status old_status, new_status;

   eina_rwlock_take_read(&id->thread_mutex);

   status = eina_inarray_nth(id->thread_index, identity - THREAD_MIN);
   if(!status) goto FAILURE;  // We fail if out of range of existing Threads.

   do {
      old_status = LOAD(status);
      if(!old_status.census) goto FAILURE;

      // Increment the given Thread's reference counter by one.
      new_status = old_status;
      new_status.generation++;
      new_status.census++;
   }
   while(!CAS(status, &old_status, new_status));

/* SUCCESS : */ eina_rwlock_release(&id->thread_mutex);  return new_status.census;
   FAILURE :    eina_rwlock_release(&id->thread_mutex);  return VOID;
}

ENG_PRIVATE Indx
_eng_thread_detach_reference(Thread identity)
{
   ATOMIC Eng_Thread_Status *status;
          Eng_Thread_Status old_status, new_status;

   eina_rwlock_take_read(&id->thread_mutex);

   status = eina_inarray_nth(id->thread_index, identity - THREAD_MIN);
   if(!status) goto FAILURE;  // We fail if out of range of existing Threads.

   do {
      old_status = LOAD(status);
      if(!old_status.census) goto FAILURE;

      // Decrement the given Thread's reference counter by one.
      new_status = old_status;
      new_status.generation++;
      new_status.census--;
   }
   while(!CAS(status, &old_status, new_status));

/* SUCCESS : */ eina_rwlock_release(&id->thread_mutex);  return new_status.census;
   FAILURE :    eina_rwlock_release(&id->thread_mutex);  return VOID;
}

ENG_PRIVATE Eng_Thread_Order
_eng_thread_priority(Thread identity, Eng_Thread_Order new_priority)
{
   ATOMIC Eng_Thread_Status *status;
          Eng_Thread_Status old_status, new_status;

   eina_rwlock_take_read(&id->thread_mutex);

   status = eina_inarray_nth(id->thread_index, identity - THREAD_MIN);
   if(!status) goto FAILURE;  // We fail if out of the identity range of existing threads.

   do {
      old_status = LOAD(status);
      if(!old_status.census) goto FAILURE;  // if(0), then the Thread is invalid.

      // Escalate the target Thread's top priority if necesarry.
      if(old_status.priority <= new_priority) { old_status.priority = new_priority; goto SUCCESS; }

      new_status = old_status;
      new_status.generation++;
      new_status.priority = new_priority;
   }
   while(!CAS(status, &old_status, new_status));

   SUCCESS : eina_rwlock_release(&id->thread_mutex);  return old_status.priority;
   FAILURE : eina_rwlock_release(&id->thread_mutex);  return PRIORITY_AWAKE;
}

ENG_PRIVATE Eng_Thread_Order
_eng_thread_priority_next(Thread identity, Eng_Thread_Order current)
{
   ATOMIC Eng_Thread_Status *status;
          Eng_Thread_Status old_status, new_status;

   eina_rwlock_take_read(&id->thread_mutex);

   status = eina_inarray_nth(id->thread_index, identity - THREAD_MIN);

   do {
      old_status = LOAD(status);
      if(old_status.priority != current) goto FAILURE;

      new_status = old_status;
      new_status.generation++;
      new_status.priority = current + 1;
   }
   while(!CAS(status, &old_status, new_status));

/* SUCCESS : */ eina_rwlock_release(&id->thread_mutex);  return new_status.priority;
   FAILURE :    eina_rwlock_release(&id->thread_mutex);  return old_status.priority;
}

ENG_PRIVATE Bool
_eng_thread_is_alive(Thread identity)
{
   ATOMIC Eng_Thread_Status *status;
          Eng_Thread_Status _status;

   eina_rwlock_take_read(&id->thread_mutex);

   status = eina_inarray_nth(id->thread_index, identity - THREAD_MIN);
   if(!status) goto FAILURE;  // We fail if out of the identity range of existing threads.

   _status = LOAD(status);
   if(!_status.is_alive) goto FAILURE;

/* SUCCESS : */ eina_rwlock_release(&id->thread_mutex);  return EINA_TRUE;
   FAILURE :    eina_rwlock_release(&id->thread_mutex);  return EINA_FALSE;
}

ENG_PRIVATE Bool
_eng_thread_is_defunct(Thread identity)
{
   ATOMIC Eng_Thread_Status *status;
          Eng_Thread_Status _status;

   eina_rwlock_take_read(&id->thread_mutex);

   status = eina_inarray_nth(id->thread_index, identity - THREAD_MIN);
   if(!status) goto FAILURE;  // We fail if out of the identity range of existing threads.

   _status = LOAD(status);
   if(_status.census) goto FAILURE;

/* SUCCESS : */ eina_rwlock_release(&id->thread_mutex);  return EINA_TRUE;
   FAILURE :    eina_rwlock_release(&id->thread_mutex);  return EINA_FALSE;
}

ENG_PRIVATE Bool
_eng_thread_terminate(Thread identity)
{
   ATOMIC Eng_Thread_Status *status;
          Eng_Thread_Status  old_status, new_status;

   eina_rwlock_take_read(&id->thread_mutex);

   status = eina_inarray_nth(id->thread_index, identity - THREAD_MIN);
   if(!status) goto FAILURE;  // We fail if out of the identity range of existing threads.

   do {
      old_status = LOAD(status);
      if(!old_status.census) goto FAILURE;

      // Mark the thread as dead and decrement it's self-reference.
      new_status.generation = old_status.generation + 1;
      new_status.priority   = old_status.priority;
      new_status.is_alive   = EINA_FALSE;
      new_status.census     = old_status.census - 1;
   }
   while(!CAS(status, &old_status, new_status));

/* SUCCESS : */ eina_rwlock_release(&id->thread_mutex);  return EINA_TRUE;
   FAILURE :    eina_rwlock_release(&id->thread_mutex);  return EINA_FALSE;
}


/*** Entity Identity Management ***/

ENG_PRIVATE Entity
_eng_entity_take_id(Thread residence)
{
   // Grab an Entity ID off the bottom of the selected unallocated ID pool.
   eina_rwlock_take_write(&id->unused_ids_mutex);

   Entity *smallest = eng_set_find_smallest(id->unused_entity_ids);
   if(!smallest) goto FAILURE;

   eng_set_detach(id->unused_entity_ids, smallest);
   Entity identity = *smallest;

   eina_rwlock_release(&id->unused_ids_mutex);

   // Add our newly created thread data to its respective global reference index.
   Eng_Thread_Status *status, _status = { VOID, (Half)(residence - THREAD_MIN), EINA_TRUE, 1 };

   eina_rwlock_take_write(&id->entity_mutex);

   status = eina_inarray_nth(id->entity_index, identity - ENTITY_MIN);
   if(status) *status = _status;
   else eina_inarray_push(id->entity_index, &_status);

/* SUCCESS : */ eina_rwlock_release(&id->entity_mutex);      return identity;
   FAILURE :    eina_rwlock_release(&id->unused_ids_mutex);  return VOID;
}

ENG_PRIVATE Bool
_eng_entity_give_id(Entity identity)
{
   // Check to see if the given Thread identity can be returned, then pop off the last value of
   //    thread_index if that value is the same as our given Thread identity.
   eina_rwlock_take_write(&id->entity_mutex);

   Eng_Thread_Status *status = eina_inarray_nth(id->entity_index, identity - ENTITY_MIN);
   if(!status || status->census) goto FAILURE;

   Indx entity_population = eina_inarray_count(id->entity_index);
   if(identity - ENTITY_MIN == entity_population - INDEXING_OFFSET)
      do {
         eina_inarray_pop(id->entity_index);
         status = eina_inarray_nth(id->entity_index, --entity_population);
      }
      while(!status->census);

   eina_rwlock_release(&id->entity_mutex);

   // Return our no longer used Thread identity to the appropriate pool.
   eina_rwlock_take_write(&id->unused_ids_mutex);

   eng_set_attach(id->unused_entity_ids, &identity);

/* SUCCESS : */ eina_rwlock_release(&id->unused_ids_mutex);  return EINA_TRUE;
   FAILURE :    eina_rwlock_release(&id->entity_mutex);      return EINA_FALSE;
}

ENG_PRIVATE Indx
_eng_entity_attach_reference(Entity identity)
{
   ATOMIC Eng_Entity_Status *status;
          Eng_Entity_Status old_status, new_status;

   eina_rwlock_take_read(&id->entity_mutex);

   status = eina_inarray_nth(id->entity_index, identity - ENTITY_MIN);
   if(!status) goto FAILURE;  // We fail if out of range of existing Entities.

   do {
      old_status = LOAD(status);
      if(!old_status.census) goto FAILURE;

      // Increment the given Entity's reference counter by one.
      new_status = old_status;
      new_status.generation++;
      new_status.census++;
   }
   while(!CAS(status, &old_status, new_status));

/* SUCCESS : */ eina_rwlock_release(&id->entity_mutex);  return new_status.census;
   FAILURE :    eina_rwlock_release(&id->entity_mutex);  return VOID;
}

ENG_PRIVATE Indx
_eng_entity_detach_reference(Entity identity)
{
   ATOMIC Eng_Entity_Status *status;
          Eng_Entity_Status old_status, new_status;

   eina_rwlock_take_read(&id->entity_mutex);

   status = eina_inarray_nth(id->entity_index, identity - ENTITY_MIN);
   if(!status) goto FAILURE;  // We fail if out of range of existing Entities.

   do {
      old_status = LOAD(status);
      if(!old_status.census) goto FAILURE;

      // Decrement the given Entity's reference counter by one.
      new_status = old_status;
      new_status.generation++;
      new_status.census--;
   }
   while(!CAS(status, &old_status, new_status));

/* SUCCESS : */ eina_rwlock_release(&id->entity_mutex);  return new_status.census;
   FAILURE :    eina_rwlock_release(&id->entity_mutex);  return VOID;
}

ENG_PRIVATE Thread
_eng_entity_residence(Entity identity)
{
   ATOMIC Eng_Entity_Status *status;
          Eng_Entity_Status _status;

   eina_rwlock_take_read(&id->entity_mutex);

   status = eina_inarray_nth(id->entity_index, identity - ENTITY_MIN);
   if(!status) goto FAILURE;  // We fail if out of range of existing Entities.

   _status = LOAD(status);
   if(!_status.census) goto FAILURE;

/* SUCCESS : */ eina_rwlock_release(&id->entity_mutex);  return _status.residence + THREAD_MIN;
   FAILURE :    eina_rwlock_release(&id->entity_mutex);  return VOID;
}

ENG_PRIVATE Bool
_eng_entity_immigrate(Entity identity, Thread residence)
{
   ATOMIC Eng_Entity_Status *status;
          Eng_Entity_Status  old_status, new_status;

   eina_rwlock_take_read(&id->entity_mutex);

   status = eina_inarray_nth(id->entity_index, identity - ENTITY_MIN);
   if(!status) goto FAILURE;  // We fail if out of range of existing Entities.

   do {
      old_status = LOAD(status);
      if(!old_status.census) goto FAILURE;

      // Set our new residence.  We may want to validate the residence in the future.
      new_status.generation = old_status.generation + 1;
      new_status.residence  = residence;
      new_status.is_alive   = old_status.is_alive;
      new_status.census     = old_status.census;
   }
   while(!CAS(status, &old_status, new_status));

/* SUCCESS : */ eina_rwlock_release(&id->entity_mutex);  return EINA_TRUE;
   FAILURE :    eina_rwlock_release(&id->entity_mutex);  return EINA_FALSE;

}

ENG_PRIVATE Bool
_eng_entity_terminate(Entity identity)
{
   ATOMIC Eng_Entity_Status *status;
          Eng_Entity_Status  old_status, new_status;

   eina_rwlock_take_read(&id->entity_mutex);

   status = eina_inarray_nth(id->entity_index, identity - ENTITY_MIN);
   if(!status) goto FAILURE;  // We fail if out of the identity range of existing threads.

   do {
      old_status = LOAD(status);
      if(!old_status.census) goto FAILURE;

      // Mark the thread as dead and decrement it's self-reference.
      new_status.generation = old_status.generation + 1;
      new_status.residence  = old_status.residence;
      new_status.is_alive   = EINA_FALSE;
      new_status.census     = old_status.census - 1;
   }
   while(!CAS(status, &old_status, new_status));

   // If the Entity is already naked, give up it's identity immediately.
   //if(!new_status.census) _eng_entity_give_id(identity); // Has deadlocking issues.

/* SUCCESS : */ eina_rwlock_release(&id->entity_mutex);  return EINA_TRUE;
   FAILURE :    eina_rwlock_release(&id->entity_mutex);  return EINA_FALSE;
}

