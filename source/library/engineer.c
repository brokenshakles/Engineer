///
/// @file
/// @brief These routines are used for Engineer library interaction.

// Deps : eng_shard.h -> eng_scene.h -> eng_node.h -> eng_thread.h -> eng_identity.h -> engineer.h
#include "eng_shard.h"

struct Engineer_Environment
{
   Thread node;

   int    log_domain;
};

static struct Engineer_Environment environment = { VOID, VOID };

#define SEED 244824
#define ROTL(x, r) (x << r) | (x >> (32 - r));
#define getblock(p, i) (p[i])

static Digest
_eng_hash_murmur3(String *key)
{
  const uint32_t len = strlen(key);
  const uint8_t* data = (const uint8_t*)key;
  const int nblocks = len / 4;
  int i;

  uint32_t h1 = SEED;

  uint32_t c1 = 0xcc9e2d51;
  uint32_t c2 = 0x1b873593;

  // body

  const uint32_t* blocks = (const uint32_t*)(data + nblocks * 4);

  for(i = -nblocks; i; i++)
  {
    uint32_t k1 = getblock(blocks, i);

    k1 *= c1;
    k1 = ROTL(k1, 15);
    k1 *= c2;

    h1 ^= k1;
    h1 = ROTL(h1, 13);
    h1 = h1 * 5 + 0xe6546b64;
  }

  // tail

  const uint8_t* tail = (const uint8_t*)(data + nblocks * 4);

  uint32_t k1 = 0;

  switch(len & 3)
  {
     case 3: k1 ^= tail[2] << 16; __attribute__ ((fallthrough));
     case 2: k1 ^= tail[1] << 8;  __attribute__ ((fallthrough));
     case 1: k1 ^= tail[0];
             k1 *= c1; k1 = ROTL(k1,15 ); k1 *= c2; h1 ^= k1;
  };

  // finalization

  h1 ^= len;

  h1 ^= h1 >> 16;
  h1 *= 0x85ebca6b;
  h1 ^= h1 >> 13;
  h1 *= 0xc2b2ae35;
  h1 ^= h1 >> 16;

  return h1 + ((uint64_t)SEED << 32);
}

#undef getblock
#undef ROTL
#undef SEED


/// @brief Init / shutdown functions.
/// @defgroup Init  Init / Shutdown
///
/// @{
///
/// Functions of obligatory usage, handling proper initialization
/// and shutdown routines.
///
/// Before the usage of any other function, Engineer should be properly
/// initialized with @ref eng_init() and the last call to Engineer's
/// functions should be @ref eng_shutdown(), so everything will be correctly freed.
///
/// Engineer logs everything with Eina Log, using the "engineer" log domain.
///    NOTE: Not yet implemented.


/// Initialize Engineer.
///
/// Initializes Engineer, its dependencies and modules. Should be the first
/// function of Engineer to be called.
///
/// @return The init counter value.
///
/// @see engineer_shutdown().
///
/// @ingroup Init

EAPI Thread
eng_init(const Efl_Event *event, void (*efl_opts)(int argc, char **argv))
{
   if(environment.node) goto PASS;

   // Initialize Eina and enable rendering with 3d accel.
   eina_init();
   elm_config_accel_preference_set("opengl");

   // Parse and handle any command line inputs and then set our EFL Application info.
   if(efl_opts)
   {
      Efl_Loop_Arguments *args = event->info;
      uint32_t            argc = eina_array_count(args->argv);
      if(argc > 0) efl_opts(argc, eina_array_data_get(args->argv, 0));
   }

   // Set up logging.
   eina_log_timing(environment.log_domain, EINA_LOG_STATE_STOP, EINA_LOG_STATE_INIT);
   environment.log_domain = eina_log_domain_register("Engineer", EINA_COLOR_CYAN);
   if(environment.log_domain < 0)
   {
      EINA_LOG_ERR("Engineer can not create its log domain.");
      goto HALT;
   }

   environment.node = _eng_node_boot();

   // Initialize the threading environment and start up the Node's Eng_Thread interface.
   PASS : return environment.node;
   HALT : return eng_halt();
}


/// Shutdown Engineer
///
/// Shutdown Engineer. If init count reaches 0, all the internal structures will
/// be freed. Any Engineer library call after this point will lead to an error.
///
/// @return Engineer's init counter value.
///
/// @see engineer_init().
///
/// @ingroup Init
///
EAPI int
eng_halt()
{
   _eng_thread_halt();

   if(environment.log_domain >= 0)
   {
      eina_log_domain_unregister(environment.log_domain);
      eina_log_timing(environment.log_domain, EINA_LOG_STATE_START, EINA_LOG_STATE_SHUTDOWN);
      environment.log_domain = -1;
   }
   else return 1;

   eina_shutdown();

   return 0;
}


///
/// @}


/// @brief Main group API that wont do anything
/// @defgroup Main Main
///
/// @{
///
/// @brief A function that doesn't do any good nor any bad
/// @ingroup Main
///
EAPI void
eng_run() {}
///
/// @}

ENG_API Thread
eng_node() { return environment.node; }

ENG_API Digest
eng_digest(String *key) { return _eng_hash_murmur3(key); }

ENG_API void
eng_empty_cb(void *data EINA_UNUSED) {}

ENG_API void
eng_free_cb(void *data) { free(data); }


/*** Scene API ***/

ENG_API Thread
eng_scene_create(String *name)
{
   Eng_Thread_Init  info     = { .name = name };
   Eina_Barrier    *dispatch = &info.dispatch;
   eina_barrier_new(dispatch, 2);

   // We need to store the Eina_Thread id and then join() it once the thread is done to prevent a memory leak.
   Eina_Thread id;  Eina_Thread_Priority priority = EINA_THREAD_BACKGROUND;  int8_t cpu = -1;
   eina_thread_create(&id, priority++, cpu, _eng_scene_init, &info);  eina_barrier_wait(dispatch);
   eina_thread_create(&id, priority++, cpu, _eng_shard_init, &info);  eina_barrier_wait(dispatch);

   eina_barrier_free(dispatch);
   return info.scene;
}

ENG_API void
eng_scene_delete(Thread scene)
{
   _eng_scene_free(scene);
}


/*** Entity API ***/

// Provide either a scene, shard, or other pre-existing non-node entity to this as a "scene".
// If a Scene, the selected Scene will choose a shard for you.
// If a Shard, the Entity will spawn in that Shard, and be assigned to that Shard's Scene.
// If any other Entity, the newly created Entity will spawn in the given Entities Shard.
ENG_API Entity
eng_entity_create(Thread scene)
{
   // If the target scene is our Node Entity, return a failure indicator.
   if(!(scene & THREAD_MASK)) return VOID;

   Entity entity = _eng_entity_take_id(scene);

   printf("Entity Created.     | ID: %ld, Scene: %ld.\n", entity, scene);

   return entity;
}

ENG_API void
eng_entity_delete(Entity target)
{
   _eng_entity_terminate(target);

   // Resolved in _eng_shard_component_detach_entity();
}

ENG_API Thread
eng_entity_scene(Entity target)
{
   Thread *scene = (Thread*)_eng_thread_that(_eng_entity_residence(target));
   if(scene) return *scene;
   else      return VOID;
}


/*** Component API ***/

ENG_API Bool
eng_component_create(Entity target, String *component, void *initial)
{
   // Fail if the target is not ordinary, or if it does not exist, or if it is marked dead.
   Thread residence = _eng_entity_residence(target);
   if(target < ENTITY_MIN || residence < 1) return EINA_FALSE;

   Indx others = _eng_entity_attach_reference(target);
   if(others == 1)
      { Thread shard = _eng_scene_sort_entity(target); _eng_entity_immigrate(target, shard); }

   Bool attached = _eng_shard_component_attach_entity(eng_digest(component), target, initial);
   if(!attached)
   {
      others = _eng_entity_detach_reference(target);
      if(!others) _eng_entity_immigrate(target, eng_entity_scene(target));

      return EINA_FALSE;
   }

   printf("Component Attached. | Entity: %ld, Symbol: %s\n", target, component);

   return EINA_TRUE;
}

ENG_API void *
eng_component_delete(Entity target, String *component)
{
   // Fail if the target is not ordinary, or if it does not exist.
   Thread residence = _eng_entity_residence(target);
   if(target < ENTITY_MIN || !residence) return NULL;

   void *detached = _eng_shard_component_detach_entity(eng_digest(component), target);
   if(!detached) return NULL;

   Indx others = _eng_entity_detach_reference(target);
   if(!others) _eng_entity_immigrate(target, eng_entity_scene(target));

   printf("Component Detached. | Entity: %ld, Digest: %s\n", target, component);

   return detached;
}


/*** System API ***/

ENG_API Bool
eng_system_load(Thread scene, String *system)
   { return _eng_scene_system_load(scene, eng_digest(system), VOID); }

ENG_API Bool
eng_system_free(Thread scene, String *system)
   { return _eng_scene_system_free(scene, eng_digest(system), VOID); }


/*** Domain API ***/

ENG_API Bool
eng_domain_load(Thread scene, String *domain) { return _eng_scene_domain_load(scene, domain); }

ENG_API Bool
eng_domain_free(Thread scene, String *domain) { return _eng_scene_domain_free(scene, domain); }


/*** Archive API ***/
/*
ENG_API void
eng_entity_archive(Entity target EINA_UNUSED)
{
}

ENG_API void
eng_entity_recall(Entity target EINA_UNUSED)
{
}
*/

