#include "eng_shard.h"

typedef struct Eng_Shard_Data Eng_Shard_Data;

struct Eng_Shard_Data
{
   Thread scene;
   Indx   batches;

   Eina_Hash *modules;  // Keyed by digest. Contains Eng_Shard_Module*'s.
};

static THREAD_LOCAL Eng_Shard_Data *shard;


/*** Thread Handling ***/

ENG_PRIVATE void *
_eng_shard_init(void *data, Eina_Thread id)
{
   Eng_Thread_Init *info = data;

   // Start up the underlying thread archtecture.
   info->shard = _eng_thread_init(info->scene, id);

   shard          = malloc(sizeof(Eng_Shard_Data));
   shard->scene   = info->scene;
   shard->batches = 32;

   shard->modules = eina_hash_int64_new(eng_empty_cb);

   // Attach the starting Shard to it's Scene.
   _eng_scene_attach_shard(shard->scene, info->shard);

   printf("\nShard Creation Checkpoint.\n");
   printf("============================\n");

   eina_barrier_wait(&info->dispatch);
   _eng_thread_dispatch();

   return NULL;
}

ENG_PRIVATE void
_eng_shard_free()
{
   printf("\nShard Exit Checkpoint.\n");
   printf("========================\n");

   _eng_scene_detach_shard(shard->scene, _eng_thread_this());

   eina_hash_free(shard->modules);

   _eng_thread_free();

   free(shard);
}


/*** Component/System Shard_Module Utilities ***/

static Eng_Shard_Module *
_module_init(Eng_Shard_Module *module, Eng_Scene_Module *scene)
{
   module->node  = scene->node;
   module->scene = scene;

   module->dependent_systems = eina_hash_int64_new(eng_empty_cb);
   module->operatives        = module->node->shard_module_init();

   eina_hash_add(shard->modules, &module->node->digest, module);

   return module;
}

static Bool
_module_free(Eng_Shard_Module *module)
{
   eina_hash_del_by_key(shard->modules, &module->node->digest);
   module->node->shard_module_free(module->operatives);

   eina_hash_free(module->dependent_systems);
   free(module);

   return EINA_TRUE;
}

static Indx
_module_has_entity(Eng_Shard_Module *module, Entity target)
{
   return eng_vault_row(module->operatives, target);
}

#define ATTACH_SHARD _eng_scene_module_attach_shard

static Eng_Shard_System *
_system_load(Digest system)
{
   Eng_Shard_System *module = eina_hash_find(shard->modules, &system);
   if(module) return module;

   Eng_Scene_Module *scene = ATTACH_SHARD(shard->scene, system, _eng_thread_this());
   if(!scene) return NULL;

   module             = (void*)_module_init(malloc(sizeof(Eng_Shard_System)), scene);
   module->candidates = eng_vault_create();
   module->invalid    = NULL;

   Indx column = eng_vault_column(module->operatives, "credentials");
   Indx gauge  = eng_vault_gauge(module->operatives, column);
        eng_vault_column_create(module->candidates, "credentials", gauge, NULL);

   module->buffer = NULL;

   return module;
}

static Bool
_system_free(Digest system)
{
   Eng_Shard_System *module = eina_hash_find(shard->modules, &system);
   if(!module) return EINA_FALSE;

   if(!eng_vault_length(module->operatives)) return EINA_FALSE;
   if(!eng_vault_length(module->candidates)) return EINA_FALSE;

   eng_vault_delete(module->candidates);

   return _module_free((void*)module);
}

static Eng_Shard_Component *
_component_load(Digest component)
{
   Eng_Shard_Component *module = eina_hash_find(shard->modules, &component);
   if(module) return module;

   Eng_Scene_Module *scene = ATTACH_SHARD(shard->scene, component, _eng_thread_this());
   if(!scene) return NULL;

   return (void*)_module_init(malloc(sizeof(Eng_Shard_Component)), scene);
}

static Bool
_component_free(Digest component)
{
   Eng_Shard_Component *module = eina_hash_find(shard->modules, &component);
   if(!module) return EINA_FALSE;

   if(!eng_vault_length(module->operatives)) return EINA_FALSE;

   return _module_free((void*)module);
}

#undef ATTACH_SHARD


/*** Shard_System Operative Entity Tracking ***/

static int
_entity_ordinal_comparison(const void *target_1, const void *target_2)
{
   if(*(Entity*)target_1 < *(Entity*)target_2) { return -1; }
   if(*(Entity*)target_1 > *(Entity*)target_2) { return  1; }
                                                 return  0;
}

static Eng_Vault *
_get_operatives(Digest module)
   { Eng_Shard_Module *get = eina_hash_find(shard->modules, &module); return get->operatives; }

static void
_system_attach_entity(Eng_Shard_System *module, Entity *target)
{
   if(module->invalid && eng_set_detach(module->invalid, target)) return;

   Byte state[module->node->entity_sizeof()];
   module->node->entity_import(1, target, (Entity_State*)state, _get_operatives);
   module->node->entity_attach(  *target, (Entity_State*)state, _eng_work_this());
   module->node->entity_export(1, target, (Entity_State*)state, _get_operatives);
}

static void
_system_detach_entity(Eng_Shard_System *module, Entity *target)
{
   if(module->invalid && eng_set_attach(module->invalid, target)) return;

   Byte state[module->node->entity_sizeof()];
   module->node->entity_import(1, target, (Entity_State*)state, _get_operatives);
   module->node->entity_detach(  *target, (Entity_State*)state, _eng_work_this());
   module->node->entity_export(1, target, (Entity_State*)state, _get_operatives);
}

#define TARGETING_HEADER                                                                     \
   /* Allocate some stack space for a credential buffer. */                                  \
   Indx            prereq_count  = eina_hash_population(module->node->component_ignored());  \
                   prereq_count += eina_hash_population(module->node->component_prereqs());  \
   Byte credential[prereq_count];  memset(credential, VOID, prereq_count);                   \
                                                                                             \
   /* Figure out which enumeration our prerequisite Component uses and mark it. */           \
   Indx ignore = (uintptr_t)eina_hash_find(module->node->component_ignored(), &component);   \
   Indx prereq = (uintptr_t)eina_hash_find(module->node->component_prereqs(), &component);   \
                                                                                             \
   /* Check to see if we already have information on the Entity. */                          \
   Indx operative = eng_vault_row(module->operatives, target);                               \
   Indx candidate = eng_vault_row(module->candidates, target);                               \
                                                                                             \
   /* If so, copy it into our credential buffer. */                                          \
   if(operative)                                                                             \
      memcpy(credential, eng_vault_fetch(module->operatives, target, 1), prereq_count); else \
   if(candidate)                                                                             \
      memcpy(credential, eng_vault_fetch(module->candidates, target, 1), prereq_count)       \

// Add a target Entity to be processed by this system. This should only be called by a
//    Component.so module.  It happends during system event dispatch.
static void
_system_attach_entity_credential(Eng_Shard_System *module, Entity target, Digest component)
{
   if(!module) module = _system_load(module->node->digest); // Used to include target.
   if(!module) return;

   TARGETING_HEADER;

   credential[ignore + prereq] = EINA_TRUE;

   if(!module->node->entity_is_ignored(credential) &&
       module->node->entity_is_targeted(credential) )
   {
      if(operative) { eng_vault_store(module->operatives, 1, target, credential);  return; }
      if(candidate)
      {
         eng_vault_row_delete(module->candidates, target);
         eng_vault_row_create(module->operatives, target);
         eng_vault_store(module->operatives, 1, target, credential);  goto ATTACH;
      }
      eng_vault_row_create(module->operatives, target);
      eng_vault_store(module->operatives, 1, target, credential);  goto ATTACH;
   } else {
      if(candidate) { eng_vault_store(module->candidates, 1, target, credential);  return; }
      if(operative)
      {
         eng_vault_row_delete(module->operatives, target);
         eng_vault_row_create(module->candidates, target);
         eng_vault_store(module->candidates, 1, target, credential); goto DETACH;
      }
      eng_vault_row_create(module->candidates, target);
      eng_vault_store(module->candidates, 1, target, credential);  return;
   }
   ATTACH : _system_attach_entity(module, &target);  return;
   DETACH : _system_detach_entity(module, &target);  return;
}

// If an Entity loses a Component Credential this System requires, remove it from the target list.
static void
_system_detach_entity_credential(Eng_Shard_System *module, Entity target, Digest component)
{
   TARGETING_HEADER;

   credential[ignore + prereq] = EINA_FALSE;

   if(!module->node->entity_is_ignored(credential) &&
       module->node->entity_is_targeted(credential) )
   {
      if(operative) { eng_vault_store(module->operatives, 1, target, credential);  return; }
      if(candidate)
      {
         eng_vault_row_delete(module->candidates, target);
         eng_vault_row_create(module->operatives, target);
         eng_vault_store(module->operatives, 1, target, credential); goto ATTACH;
      }
   } else {
      if(candidate) { eng_vault_store(module->candidates, 1, target, credential);  goto DELETE; }
      if(operative)
      {
         eng_vault_row_delete(module->operatives, target);
         eng_vault_row_create(module->candidates, target);
         eng_vault_store(module->candidates, 1, target, credential); goto DETACH;
      }
   }
   ATTACH : _system_attach_entity(module, &target);  return;
   DETACH : _system_detach_entity(module, &target);  // We fall thru here.

   DELETE : for(Indx count = 0; count < prereq_count; count++)
               if(credential[count]) return;
            if(operative) eng_vault_row_delete(module->operatives, target); else
            if(candidate) eng_vault_row_delete(module->candidates, target);

   if(eng_vault_length(module->operatives) ||
      eng_vault_length(module->candidates)) return;

   _system_free(module->node->digest);
}

#undef TARGETING_HEADER


/*** Shard_Component Operative Entity Tracking ***/

struct Shard_Component_Info
{
   Digest component;
   Entity target;
   Byte   initial[];
};

static void
_eng_shard_component_attach_entity__work(void *args, void *result)
{
   struct Shard_Component_Info *info = args;

   Eng_Shard_Component *module = _component_load(info->component);
   if(!module ||
      !module->node->entity_attach(module->operatives, info->target, (Entity_State*)info->initial))
      return;

   Eina_Iterator *dependent_systems;  Eng_Shard_System *dependent_system;
   dependent_systems = eina_hash_iterator_key_new(module->dependent_systems);
   while(eina_iterator_next(dependent_systems, (void**)&dependent_system))
      _system_attach_entity_credential(dependent_system, info->target, info->component);
   eina_iterator_free(dependent_systems);

   _eng_entity_attach_reference(info->target);

   *(Bool*)result = EINA_TRUE;
}

ENG_PRIVATE Bool
_eng_shard_component_attach_entity(Digest component, Entity target, void *initial)
{
   Eng_Node_Component *node = (Eng_Node_Component*)_eng_node_has_module(component);

   struct Shard_Component_Info *call =
      eng_work_call(target, _eng_shard_component_attach_entity__work,
                    sizeof(struct Shard_Component_Info) + node->component_sizeof(),
                    sizeof(Bool));

   call->component = component;
   call->target    = target;
   memcpy(call->initial, initial, node->component_sizeof());

   return *(Bool*)eng_work_wait(call);
}

static void
_eng_shard_component_detach_entity__work(void *args, void *result)
{
   struct Shard_Component_Info *info = args;

   Eng_Shard_Component *module = eina_hash_find(shard->modules, &info->component);
   if(!module || !_module_has_entity((Eng_Shard_Module*)module, info->target)) return;

   _eng_entity_detach_reference(info->target);

   Eina_Iterator *dependent_systems;  Eng_Shard_System *dependent_system;
   dependent_systems = eina_hash_iterator_key_new(module->dependent_systems);
   while(eina_iterator_next(dependent_systems, (void**)&dependent_system))
      _system_detach_entity_credential(dependent_system, info->target, info->component);
   eina_iterator_free(dependent_systems);

   module->node->entity_detach(module->operatives, info->target, (Entity_State*)result);

   if(!eng_vault_length(module->operatives)) _component_free(info->component);
}

ENG_PRIVATE void *
_eng_shard_component_detach_entity(Digest component, Entity target)
{
   Eng_Node_Component *globals = (void*)_eng_node_has_module(component);
   if(!globals) return EINA_FALSE;

   struct Shard_Component_Info *call =
      eng_work_call(target, _eng_shard_component_detach_entity__work,
                    sizeof(struct Shard_Component_Info), globals->component_sizeof());

   call->component = component;
   call->target    = target;

   return *(void**)eng_work_wait(call);
}


/*** System Entity Update Process ***/

struct Split
{
   void (*operate)(Indx, Entity*, Entity_State*, Entity*);
   Indx    *batch, width, length, has_extra;
   Entity **target;  Byte **buffer;
};

// The following function(s) gobbles it's input data in what I call the "Mad Hatters Table Trick".
//   The **'s in the above struct make it so each echo increments a common pointer.  It really
//   doesn't matter in which order each echo happens, echo wise, since they all take up the proper
//   execution order regardless.  See eng_thread.c for _eng_work_sync().
static void
_split__work(void *args, void *result EINA_UNUSED)
{
   struct Split *info = args;

   if(*info->batch < info->has_extra) info->length++;

   *info->batch  += 1;
   *info->target += info->length;
   *info->buffer += info->length * info->width;

   info->operate(info->length, *info->target, *(Entity_State**)info->buffer, _eng_work_this());
}

static void
_split(void (*operate)(Indx, Entity*, Entity_State*, Entity*), Indx width, Indx length,
   Entity *target, Entity_State *buffer, Eng_Vault *operatives)
{
   Set *batches = eng_set_create(64, 1, VOID);
   Indx packed  = length / shard->batches;
   Indx remain  = length % shard->batches;

   Indx batch = 0;
   for(Indx row = 1; row <= length; row += packed)
   {
      eng_set_attach(batches, eng_vault_fetch(operatives, 0, row));
      if(batch++ < remain) row++;
   }
   batch = 0;

   struct Split *echo = _eng_work_sync(batches, _split__work, sizeof(struct Split), VOID);

   *echo = (struct Split) { operate, &batch, width, packed, remain, &target, (Byte**)&buffer};

   eng_work_wait(echo);

   eng_set_delete(batches);
}

struct Buffer
{
   Thread            shard;
   Digest           *system;
   Eng_Shard_System *module;

   Indx   width;
   Indx   length;
   Entity target[];
};

static void
_eng_shard_update_system__work(void *args, void *result EINA_UNUSED)
{
   struct Buffer   *info    = args;
   Eng_Node_System *globals = info->module->node;
   Entity          *target  = info->target;
   Entity_State    *buffer  = (Entity_State*)(info->target + info->length);

   // Set up our invalidation tracker for the duration of the current update.
   info->module->invalid = eng_set_create(64, 1, 0);

   // Make a copy of the index of our current table of operatives and then sort it.
   memcpy(target, eng_vault_fetch(info->module->operatives, 0, 1), sizeof(Entity) * info->length);
    qsort(target, info->length, sizeof(Entity), _entity_ordinal_comparison);

   // Work thru each phase of our System's Entity Update.
   globals->entity_import(info->length, target, buffer, _get_operatives);

// globals->entity_attach(buffer); //  Is done immediately when an Entity is attached to a System.

   _split(globals->entity_upkeep, info->width, info->length, target, buffer, info->module->operatives);

   Digest entity_event_system = eng_digest("Entity.Event.System");
   if(eina_hash_find(globals->system_prereqs(), &entity_event_system))
      _split(globals->entity_notify, info->width, info->length, target, buffer, info->module->operatives);

   _split(globals->entity_update, info->width, info->length, target, buffer, info->module->operatives);

   Entity *invalid;  Indx count = 0;
   ENG_SET_FOREACH(info->module->invalid, invalid)
   {
      while(*invalid != *(target + count)) count++;

      Entity_State *buf = (Entity_State*)(((Byte*)buffer) + (count * info->width));
      globals->entity_detach(*invalid, buf, _eng_work_this());
   }

   globals->entity_export(info->length, target, buffer, _get_operatives);

   eng_set_delete(info->module->invalid);
   info->module->invalid = NULL;
}

static void
_eng_shard_update_system__setup_buffer(void *args, void *result EINA_UNUSED)
{
   struct Buffer setup;

   setup.shard  = _eng_thread_this();
   setup.system = args;
   setup.module = eina_hash_find(shard->modules, system);
   setup.length = eng_vault_length(setup.module->operatives);
   setup.width  = setup.module->node->entity_sizeof();

   Indx  buffer = setup.length * (sizeof(Entity) + setup.width);

   Set           *this = eng_set_create(64, 1, VOID);  eng_set_attach(this, &setup.shard);
   struct Buffer *echo = _eng_work_sync(this, _eng_shard_update_system__work,
                                        sizeof(struct Buffer) + buffer, VOID);
                 *echo = setup;

   eng_work_wait(echo);  eng_set_delete(this);
}

// Delcared in eng_scene.h.  Only invocated from eng_scene.c:26
ENG_PRIVATE void
_eng_shard_update_system(Set *dependent_shards, Digest system)
{
   Digest *echo = _eng_work_sync(dependent_shards, _eng_shard_update_system__setup_buffer,
                                 sizeof(Digest),   VOID);
          *echo = system;

   eng_work_wait(echo);
}

