#include "tool/vault.h"
#include "../engineer.h"

#define INITIAL_ROW 0
#define BACK_COLUMN 0
#define THIS_COLUMN ((Eina_Inarray**)(vault->columns->members) - column)
#define THIS_ENTITY ((Entity*)((*entities)->members) - entity)
#define COLUMN_SIZE (*column)->member_size

struct Eng_Vault // NOTICE : Both row 0 and column 0 arrays contain control data.
{
   Eina_Hash    *lookup;   // Table of row indices keyed by Entity.
   Eina_Inarray *fields;   // Contains each columns Field Symbol *. // Currently a Digest.
   Eina_Inarray *is_value; // Stores whether the field is a value or a reference.
   Eina_Inarray *columns;  // Holds a list of Eina_Inarray*'s, one for each column.
};


/*** Vault Management ***/

Eng_Vault *
eng_vault_create()
{
   Eng_Vault *vault = malloc(sizeof(Eng_Vault));

   vault->lookup   = eina_hash_string_superfast_new(NULL);
   vault->fields   = eina_inarray_new(sizeof(String*),       0);
   vault->is_value = eina_inarray_new(sizeof(Bool),          0);
   vault->columns  = eina_inarray_new(sizeof(Eina_Inarray*), 0);

   // Column 0 is created by default and always holds an Entity backreference.
   eng_vault_column_create(vault, "entity", sizeof(Entity), NULL);

   // Row 0 is created by default, keyed by a VOID Digest, and contains default data for new rows.
   eng_vault_row_create(vault, INITIAL_ROW);

   return vault;
}

void
eng_vault_delete(Eng_Vault *vault)
{
   Eina_Inarray **column;
   EINA_INARRAY_REVERSE_FOREACH(vault->columns, column) eng_vault_column_delete(vault, THIS_COLUMN);
   eina_inarray_free(*(Eina_Inarray**)eina_inarray_pop(vault->columns));

   eina_inarray_free(vault->columns);
   eina_inarray_free(vault->is_value);
   eina_inarray_free(vault->fields);
   eina_hash_free(vault->lookup);

   free(vault);
}

Indx
eng_vault_width(Eng_Vault *vault)
{
   return eina_inarray_count(vault->columns) - 1;
}

Indx
eng_vault_length(Eng_Vault *vault)
{
   return eina_hash_population(vault->lookup);
}


/*** Column Management ***/

static int
_is_target(const void *field, const void *target)
{
   if(*(Digest*)field == eng_digest((String*)target)) return 0;
   else                                               return 1;
}

Indx
eng_vault_column(Eng_Vault *vault, String *field)
{
   // WARNING : This uses a linear walk, so it's best to order columns by frequency of usage.
   Digest digest = eng_digest(field);
   Indx   column = (Indx)eina_inarray_search(vault->fields, &digest, _is_target);
   if(column == INDX_MAX) column = VOID;

   return column;
}

// Returns VOID on failure, the column index on success.
Indx
eng_vault_column_create(Eng_Vault *vault, String *field, Indx size, void *initial)
{
   void * (*reference_alloc)(void *free) = initial;

   Indx          column;
   Bool          is_value   = size ? EINA_TRUE : EINA_FALSE;
   Eina_Inarray *new_column = eina_inarray_new(size, 0);
   if(!new_column) return VOID;

   field = eina_stringshare_add(field);

                  eina_inarray_push(vault->fields,   &field);
                  eina_inarray_push(vault->is_value, &is_value);
   column = (Indx)eina_inarray_push(vault->columns,  &new_column);

   // Populate the new column with rows for existing Entity fields.
   Eina_Inarray **entities = eina_inarray_nth(vault->columns, BACK_COLUMN);
   Entity        *entity;
   EINA_INARRAY_FOREACH(*entities, entity)
      if(is_value) eina_inarray_push(new_column, initial);
      else {
         initial = reference_alloc(NULL);
         eng_vault_store(vault, column, THIS_ENTITY, &initial);
      }

   return column;
}

Bool
eng_vault_column_delete(Eng_Vault *vault, Indx column)
{
   void * (*reference_alloc)(void *free);

   //Bool          *is_value   = eina_inarray_nth(vault->is_value, column);
   Eina_Inarray **old_column = eina_inarray_nth(vault->columns,  column);

   if(!old_column || !eng_vault_width(vault)) return EINA_FALSE;

   eina_stringshare_del(eng_vault_column_symbol(vault, column));

   // If we have a (*reference_alloc) defined, use it to free any remaining reference fields.
   void **field;
   reference_alloc = *(void**)eng_vault_fetch(vault, column, INITIAL_ROW);
   if(reference_alloc) EINA_INARRAY_FOREACH(*old_column, field) reference_alloc(*field);
   eina_inarray_free(*old_column);

   eina_inarray_remove_at(vault->columns,  column);
   eina_inarray_remove_at(vault->is_value, column);
   eina_inarray_remove_at(vault->fields,   column);

   return EINA_TRUE;
}

String *
eng_vault_column_symbol(Eng_Vault *vault, Indx column)
{
   return *(String**)eina_inarray_nth(vault->fields, column);
}


/*** Row Management ***/

Indx
eng_vault_row(Eng_Vault *vault, Entity target)
{
   Indx *row = eina_hash_find(vault->lookup, &target);
   if(!row) return VOID;
   else     return *row;
}

Indx
eng_vault_row_create(Eng_Vault *vault, Entity target)
{
   void * (*reference_alloc)(void *free) = NULL;

   // If the Entity is already attached, return its row instead.
   Indx row = eng_vault_row(vault, target);
   if(row) return row;

   // Create a new field in each available column for the new row.
   Eina_Inarray **column;
   EINA_INARRAY_FOREACH(vault->columns, column)
      if(*(Bool**)eina_inarray_nth(vault->is_value, THIS_COLUMN))
        row = eina_inarray_push(*column, eng_vault_fetch(vault, THIS_COLUMN, INITIAL_ROW));
      else {
         reference_alloc = *(void**)eng_vault_fetch(vault, THIS_COLUMN, INITIAL_ROW);
         void *alloc = reference_alloc(NULL);
         if(reference_alloc) row = eina_inarray_push(*column, &alloc);
      }

   // Set the Entity lookup and back-reference here.
   Eina_Inarray **entities = eina_inarray_nth(vault->columns, BACK_COLUMN);
   eina_inarray_replace_at(*entities, row, &target);
   eina_hash_set(vault->lookup, &target, (void*)(intptr_t)row);

   return row;
}

static Bool
_eng_vault_row_swap(Eng_Vault *vault, Entity target_a, Entity target_b)
{
   Indx row_a = (intptr_t)eina_hash_find(vault->lookup, &target_a); if(!row_a) return EINA_FALSE;
   Indx row_b = (intptr_t)eina_hash_find(vault->lookup, &target_b); if(!row_b) return EINA_FALSE;

   eina_hash_set(vault->lookup, &target_a, (void*)(intptr_t)row_b);
   eina_hash_set(vault->lookup, &target_b, (void*)(intptr_t)row_a);

   Eina_Inarray **column;
   EINA_INARRAY_FOREACH(vault->columns, column)
   {
      uint8_t field[COLUMN_SIZE];
      memcpy(field,                           eina_inarray_nth(*column, row_a), COLUMN_SIZE);
      eina_inarray_replace_at(*column, row_a, eina_inarray_nth(*column, row_b));
      eina_inarray_replace_at(*column, row_b, field);
   }

   return EINA_TRUE;
}

Bool
eng_vault_row_delete(Eng_Vault *vault, Entity target)
{
   void * (*reference_alloc)(void *free) = NULL;

   // If the Entity is not already attached, refuse.
   Indx row = eng_vault_row(vault, target);
   if(!row || !eng_vault_length(vault)) return EINA_FALSE;

   // Make sure the row we want to remove is at the pop-end of the table.
   Eina_Inarray **entities = eina_inarray_nth(vault->columns, BACK_COLUMN);
   Entity        *pop_end  = eina_inarray_nth(*entities, eng_vault_length(vault));
   if(target != *pop_end) _eng_vault_row_swap(vault, target, *pop_end);

   Eina_Inarray **column;
   EINA_INARRAY_FOREACH(vault->columns, column)
      if(*(Bool**)eina_inarray_nth(vault->is_value, THIS_COLUMN)) eina_inarray_pop(*column);
      else {
         void **field = eina_inarray_pop(*column);
         if(*field) reference_alloc = *(void**)eng_vault_fetch(vault, THIS_COLUMN, INITIAL_ROW);
         if(reference_alloc) reference_alloc(*field);
      }
   eina_hash_del_by_key(vault->lookup, &target);

   return EINA_TRUE;
}


/*** Field Management ***/

Indx
eng_vault_gauge(Eng_Vault *vault, Indx column)
{
   Eina_Inarray **gauge_column = eina_inarray_nth(vault->columns, column);
   if(!gauge_column) return 0;

   return (*gauge_column)->member_size;
}

// Storing to row 0 will set the initial data for that columns field, for new Entities.
Bool
eng_vault_store(Eng_Vault *vault, Indx column, Indx row, void *data)
{
   if(!vault || !column) return EINA_FALSE; // Attempting to overwrite column 0 is invalid.

   Eina_Inarray **store_column = eina_inarray_nth(vault->columns, column);
   if(!store_column) return EINA_FALSE;

   eina_inarray_replace_at(*store_column, row, data);

   return EINA_TRUE;
}

// Fetching row 0 will return the fields set initial data. Fetching column 0 will return the Entity.
void *
eng_vault_fetch(Eng_Vault *vault, Indx column, Indx row)
{
   if(!vault || (!column && !row)) return NULL; // Setting both column and row to 0 is invalid.

   Eina_Inarray **fetch_column = eina_inarray_nth(vault->columns, column);
   if(!fetch_column) return NULL;

   return eina_inarray_nth(*fetch_column, row);
}

