#include "tool/cord.h"
#include "../engineer.h"

/*** NON-PORTABLE Assembler Macros ***/

#if defined(__x86_64__)
   #include "cord.asm.x86_64"
#elif defined(__PPC64__)
   #include "cord.asm.ppc64"
#elif defined(__aarch64__)
   #include "cord.asm.aarch64"
#elif defined(__riscv)
   #include "cord.asm.riscv"
#endif

// Not used on bare metal, caveats about calling abort() within library code apply.
#if defined (__unix__) || defined (__APPLE__)
#include <stdlib.h>

// It is strongly suggested to always make the following a power of 2, written "(1 << X)".
#define CHUNK_SIZE (1 <<  6) /* 64 blocks, or 32 megabytes.           */
#define BLOCK_SIZE (1 << 19) /* 0x80000 bytes, or half of a megabyte. */

// Determine hardware aligned stack size/offset relative to the block pointer.
#define STACK_SIZE (BLOCK_SIZE - sizeof(struct Context) - sizeof(struct Footing))
#define ALIGN_SIZE (STACK_SIZE - (STACK_SIZE % STACK_ALIGNMENT))

struct Context
{
   void *entry;  // Points to the next instruction that will be called during continuation.
   void *stack;  // Points to the top (push/pop) end of the Cord's call stack.
   void *frame;  // Points to the bottom end (base) of the top stack frame.
   void *retrn;  // Points to the inactive coroutine, not used in x86_64 systems.
};

struct Footing
{
   Cord *other; // If this Cord is *active, then other == parent, else other == active child.
   Indx  block; // Used to find this Cord's Chunk and Block.

   void (*volatile main)(void *); // This pointer is nulled when the function has returned.
   void *payload;                 // Argument to pass to the above function, also used to pass
};                                //   values between parent and child via await() and yield().

struct Cord
{
          Byte    stack[ALIGN_SIZE];
   struct Context inactive;
   struct Footing metadata;
};

static THREAD_LOCAL Set  *available; // Stores the block indices that are available for Cords.
static THREAD_LOCAL void *stacks;    // Points to an Eina_Inarray.
static THREAD_LOCAL Cord *active;    // Points to the currently executing Cord context.

ENG_PRIVATE void
_cord_thread_init()
{
   available = eng_set_create(64, 1, 0); // Maybe change back to SET_U64?
   stacks    = eina_inarray_new(sizeof(void*), 0);
   active    = NULL;
}

ENG_PRIVATE void
_cord_thread_free()
{
   eina_inarray_free(stacks);
   eng_set_delete(available);
}

static Cord *
_cord_alloc()
{
   // Reserve an available Block allocation.  If none are available, allocate a new Chunks worth.
   Indx  *block = eng_set_find_smallest(available);
   Byte **chunk = eina_inarray_nth(stacks, *block / CHUNK_SIZE);
   if(!chunk)
   {
      void *new_chunk;
      if(posix_memalign(&new_chunk, STACK_ALIGNMENT, BLOCK_SIZE * CHUNK_SIZE)) abort();
      chunk = eina_inarray_nth(stacks, eina_inarray_push(stacks, &new_chunk));
   }

   // Remove the Block's index from the list of available indicies.
   eng_set_detach(available, block);

   // Offset the Chunk pointer to and stencil the new Cord struct on the appropriate Block.
   Cord *cord =   (Cord*)(*chunk + (BLOCK_SIZE * (*block % CHUNK_SIZE)));
   cord->metadata.block = *block;

   return cord;
}

static void
_cord_free(Cord *cord) // FIXME : Does not actually deallocate.
{
   eng_set_attach(available, &cord->metadata.block);
}

static void
_cord_swap(Cord *cord)
{
    cord->metadata.other = active;
    active               = cord;

    SWAP_CONTEXT(&cord->inactive);
}

__attribute((noreturn))
static void
_cord_bootstrapper(uint8_t *context)
{
    Cord *new_cord = (Cord*)(context - ALIGN_SIZE);

    void * const       payload = new_cord->metadata.payload;
    new_cord->metadata.payload = NULL;

    new_cord->metadata.main(payload);  // Call the designated main() with the given argument.
    new_cord->metadata.main = NULL;    // Reached when the Cord's main() finishes.

    _cord_swap(new_cord);              // Leave the child context forever after it completes.
    __builtin_unreachable();
}

ENG_API Cord *
cord_spawn(void (*main)(void *), void *payload)
{
   Cord *cord = _cord_alloc();

   // Initialize the Cord header.
   cord->metadata.main    = main;
   cord->metadata.payload = payload;
   cord->metadata.other   = active;
   active                 = cord;

   // Call the bit of asm that fills the inactive context and jumps to the bootstrapper.
   BOOTSTRAP_CONTEXT(cord, _cord_bootstrapper);

   // Control flow returns here via the first cord_yield in the child.
   if(cord->metadata.main) return cord;
   _cord_free(cord);       return NULL;
}

// Return the given child Cord's current payload. Usually used on the output of cord_spawn().
ENG_API void *
cord_query(Cord *cord)
{
   if(cord) return cord->metadata.payload;
   else     return NULL;
}

// This operation descends from a parent coroutine to its child, handing off a payload both ways.
ENG_API void *
cord_await(Cord *cord, void *payload)
{
   if(!cord->metadata.main) return NULL;
   if(payload) active->metadata.payload = payload;

   _cord_swap(cord);

   payload = cord->metadata.payload;
   if(!cord->metadata.main) _cord_free(cord);

   return payload;
}

// This operation ascends from a child Cord to its parent, handing off a payload both ways.
ENG_API void *
cord_yield(void *payload)
{
   if(payload) active->metadata.payload = payload;

   _cord_swap(active->metadata.other);

   return active->metadata.payload;
}

// This will immidately nullify/terminate a child cord while returning it's most recent payload.
ENG_API void *
cord_annul(Cord *cord)
{
   void *payload = cord_query(cord);
                   _cord_free(cord);
   return payload;
}

// Same as cord_yield, but this also self-terminates the child Cord while yielding a final payload.
__attribute((noreturn))
ENG_API void
cord_abort(void *payload)
{
   active->metadata.main = NULL;
   cord_yield(payload);
   __builtin_unreachable();
}

#endif

/*  Special thanks to contributors from #musl on Freenode.net who wish to remain obscure
 *     for providing this code. Code is used with permission.
 *
 * Coroutines for generator functions, sequential pipelines, state machines, and other uses in c.

 Tested on : x86_64 linux actual hardware

 The intended use case is when concurrency, but not parallelism, is required, with a possibly very
 high rate of switching between coroutines, in a possibly hard-realtime context (such as an audio
 subsystem).

 This implementation relies only on some inline asm, and is therefore useful in uncooperative
 environments such as iOS or bare metal. It is possible to implement this same API using pthreads,
 but the cost of switching between coroutines would be many orders of magnitude greater, and it
 would not be possible to meet hard-realtime deadlines.

 Unlike protothreads and other stackless methods, each coroutine has its own fully functional call
 stack, so local variables in the coroutines preserve their values, multiple instances of the same
 function may be in flight at once, and coroutines may yield from anywhere within the call stack,
 not just the top level function.

 Unlike pthreads and other kernel-scheduled preemptive multitasking thread implementations, it is
 extremely cheap to switch between coroutines, and hard-realtime requirements may be met while
 doing so. thus it is possible to have a generator-type coroutine that yields individual audio
 samples (or small batches of them) to a hard-realtime callback function inside an audio subsystem.

 This code consists of the coroutine starting and switching primitives, as well as some convenience
 functions (yield, await) for implementing generator functions and sequential pipelines and things
 of that sort. these generator functions may yield a pointer to local storage, a pointer to heap
 storage (which may or may not be expected to be freed by the yielded-to code), or any nonzero value
 that can fit within the memory occupied by a void pointer. a coroutine may NOT yield NULL, as this
 has special meaning to the accepting code (it indicates to a waiting parent that a child function
 has already returned, or indicates to a waiting child that a parent function wants it to clean up
 and return). if a coroutine yields a pointer to a local variable, the accepting code may only read
 through that pointer, not write through it (unless the local variable is marked volatile). See the
 documentation of setjmp/longjmp for more details, and assume all the same restrictions apply, even
 though this implementation does not use setjmp/longjmp.

 Porting this to a new platform is a matter of writing two macros which expand to some
 platform-specific inline assembly.
 */

