#ifndef _ENG_CORD_NONPORTABLE_ASM_
#define _ENG_CORD_NONPORTABLE_ASM_

# Warning : This is not working correctly with position independent executables!
#define STACK_ALIGNMENT 64

#define BOOTSTRAP_CONTEXT(buf, func) do {                                      \
register void * _buf  asm("r3") = buf;                                         \
register void * _func asm("r4") = func;                                        \
asm volatile(                                                                  \
"bl 0f\n"          /* Calculate end of this block address, so we can jump. *\  \
"0: mflr 8\n"      /* Back to it from the created child. */                    \
"addi 8, 8, 1f-0b\n"                                                           \
"std 8, 0(%0)\n"   /* Store it in the first slot of the buffer. */             \
"std 1, 8(%0)\n"   /* Store the parent's stack pointer in the next slot. */    \
"std 31, 16(%0)\n" /* Store the parent's frame pointer in the next slot. */    \
"std 2, 24(%0)\n"  /* Store the parent's toc pointer in the last slot. */      \
"mr 1, %0\n"       /* Use "buf" as the stack pointer for the created child. */ \
"subi 1, 1, 24\n"  /* Subtract 24 to enforce stack alignment. */               \
"mr 3, %0\n"       /* Put "buf" in the first function call argument. */        \
"mtctr %1\n"       /* Put address of "func" in ctr register. */                \
"bctr\n"           /* Call "func" (this does not return). */                   \
"1:\n"                                                                         \
: "+r"(_buf), "+r"(_func) : :                                                  \
    "r0",  "r5",  "r6",  "r7",  "r8",  "r9", "r10", "r11",                     \
   "r12", "r13", "r14", "r15", "r16", "r17", "r18", "r19",                     \
   "r20", "r21", "r22", "r23", "r24", "r25", "r26", "r27",                     \
   "r28", "r29", "r30",  "32",  "33",  "34",  "35",  "36",                     \
    "37",  "38",  "39",  "40",  "41",  "42",  "43",  "44",                     \
    "45",  "46",  "47",  "48",  "49",  "50",  "51",  "52",                     \
    "53",  "54",  "55",  "56",  "57",  "58",  "59",  "60",                     \
    "61",  "62",  "63",  "v0",  "v1",  "v2",  "v3",  "v4",                     \
    "v5",  "v6",  "v7",  "v8",  "v9", "v10", "v11", "v12",                     \
   "v13", "v14", "v15", "v16", "v17", "v18", "v19", "v20",                     \
   "v21", "v22", "v23", "v24", "v25", "v26", "v27", "v28",                     \
   "v29", "v30", "v31", "memory",      "cc", "xer", "ctr", "cr0",              \
   "cr1", "cr2", "cr3", "cr4", "cr5", "cr6", "cr7",  "lr"); } while (0)

#define SWAP_CONTEXT(buf) do { \
register void * _buf asm("r3") = buf; \
asm volatile( \
"ld 9, 0(%0)\n" /* retrieve inactive thread's pc from buffer */ \
"ld 10, 8(%0)\n" /* retrieve inactive stack pointer */ \
"ld 11, 16(%0)\n" /* retrieve inactive frame pointer */ \
"ld 12, 24(%0)\n" /* retrieve inactive toc pointer */ \
"bl 0f\n" /* calculate address of end of this block, so we can jump back to it on the next context switch */ \
"0: mflr 8\n" \
"addi 8, 8, 1f-0b\n" \
"std 8, 0(%0)\n" /* store the active thread's pc in the buffer */ \
"std 1, 8(%0)\n" /* store the active thread's stack pointer */ \
"std 31, 16(%0)\n" /* store the active thread's frame pointer */ \
"std 2, 24(%0)\n" /* store the active thread's toc pointer */ \
"mr 1, 10\n" /* switch the stack pointer to the loaded value */ \
"mr 31, 11\n" /* switch the frame pointer to the loaded value */ \
"mr 2, 12\n" /* switch the toc pointer to the loaded value */ \
"mtctr 9\n" /* jump to the stored program counter */ \
"bctr\n" \
"1:\n" \
: "+r"(_buf) : : "r0", "r4", "r5", "r6", "r7", "r8", "r9", "r10", "r11", "r12", "r13", "r14", "r15", "r16", "r17", "r18", "r19", "r20", "r21", "r22", "r23", "r24", "r25", "r26", "r27", "r28", "r29", "r30", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63", "v0", "v1", "v2", "v3", "v4", "v5", "v6", "v7", "v8", "v9", "v10", "v11", "v12", "v13", "v14", "v15", "v16", "v17", "v18", "v19", "v20", "v21", "v22", "v23", "v24", "v25", "v26", "v27", "v28", "v29", "v30", "v31", "memory", "cc", "xer", "ctr", "cr0", "cr1", "cr2", "cr3", "cr4", "cr5", "cr6", "cr7", "lr"); } while (0)

#endif

