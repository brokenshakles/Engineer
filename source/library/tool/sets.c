#include "tool/sets.h"
#include "../engineer.h"

#define TYPE_SWITCH(func, target)       \
   switch(target->type)                 \
   {                                    \
      case   8 : func(uint8_t);  break; \
      case  16 : func(uint16_t); break; \
      case  32 : func(uint32_t); break; \
      case  64 : func(uint64_t); break; \
      case  -8 : func(int8_t);   break; \
      case -16 : func(int16_t);  break; \
      case -32 : func(int32_t);  break; \
      case -64 : func(int64_t);  break; \
   }                                    \

struct _Set
{
   Eina_Inarray *intervals;

   Indx   offset;  // Only numbers that are divisible by the modulus after subtracting the offset
   Indx   modulus; //    are allowed to be attached to the set. // Was Indx interval;
   int8_t type;
};

static int
_set_comparison(Set *set, Indx index, const void *target)
{
   #define SET_COMPARISON(type) {                                           \
   type *lower = eina_inarray_nth(set->intervals, index + INDEXING_OFFSET); \
   type *upper = lower + 1;                                                 \
                                                                            \
   if(*(type*)lower > *(type*)target) { return -1; }                        \
   if(*(type*)upper < *(type*)target) { return  1; }                        \
                                        return  0; }

   TYPE_SWITCH(SET_COMPARISON, set);
   #undef SET_COMPARISON

   return 0; // Execution should never get here.
}

static Indx
_set_search(Set *set, void *target)
{
   Indx index = (eina_inarray_count(set->intervals) - INDEXING_OFFSET) / 2;
   Indx jump  = index;
   int  result;                                     // Example Set and Return Vaules by Index.
                                                    //    0     1     2      3       4
   while(jump)                                      // | 2-3 | 5-6 | 8-9 | 11-13 | 15-17 |
   {                                                //
      result = _set_comparison(set, index, target); //  0 : 1,2,3
      if(!result) return index;                     //  1 : 4,5,6
      if(result < 0) index -= jump;                 //  2 : 7,8,9
      if(result > 0) index += jump;                 //  3 : 10,11,12,13
      jump /= 2;                                    //  4 : 14,15,16,17
   }                                                //  5 : 18..

   if(index >= eina_inarray_count(set->intervals)) return VOID;
   else                                            return index + INDEXING_OFFSET;
}

ENG_API Set *
eng_set_create(int8_t type, Indx modulus, Indx offset)
{
   Indx size;

   Set *set     = malloc(sizeof(Set));
   set->type    = type;
   set->offset  = offset;
   set->modulus = modulus;

   TYPE_SWITCH(size = sizeof, set);
   set->intervals = eina_inarray_new(size * 2, 0);

   eina_inarray_push(set->intervals, NULL); // Probably need to replace NULL with a valid default value.

   return set;
}

ENG_API void
eng_set_delete(Set *set)
{
   eina_inarray_free(set->intervals);

   free(set);
}

static Bool
_set_attach(Set *set, void *lower, void *upper, Indx prev, Indx next)
{
   Indx edit = 0;

   #define SET_ATTACH(type) {                                                            \
   type *north   = eina_inarray_nth(set->intervals, next);                               \
   type *south   = eina_inarray_nth(set->intervals, prev); south++;                      \
   type  modulus = set->modulus;                                                         \
                                                                                         \
   /* If our set already contains elements of this interval, exit immediately. */        \
   if(north && *north <= *(type*)lower && *(type*)upper <= *south) return EINA_FALSE;    \
                                                                                         \
   /* Check to see if our new element can be appendended to the previous nearby Node. */ \
   if(north && *north == *(type*)lower - modulus) { *north = *(type*)upper; edit++; }    \
                                                                                         \
   /* Check to see if our new element can be appendended to the next nearby Node. */     \
   if(south && *south == *(type*)upper + modulus) { *south = *(type*)lower; edit++; }    \
                                                                                         \
   /* If neither adjacent interval has been appended to, insert a new node. */           \
   if(!edit)                                                                             \
   {                                                                                     \
      eina_inarray_insert_at(set->intervals, next, NULL);                                \
      north = eina_inarray_nth(set->intervals, next); *north = *(type*)lower;            \
      south = north + 1;                              *south = *(type*)upper;            \
   } }                                                                                   \

   TYPE_SWITCH(SET_ATTACH, set);
   #undef SET_ATTACH

   // If both the previous and next interval have been appended to, merge them.
   if(edit == 2)
   {
      eina_inarray_replace_at(set->intervals, next, eina_inarray_nth(set->intervals, prev));
      eina_inarray_remove_at(set->intervals, prev);
   }

   return EINA_TRUE;
}

static Bool
_set_detach(Set *set, void *lower, void *upper, Indx prev, Indx next)
{
   Indx edit = 0;

   #define SET_DETACH(type) {                                                       \
   type *north   = eina_inarray_nth(set->intervals, prev);                          \
   type *south   = north + 1;                                                       \
   type  modulus = set->modulus;                                                    \
                                                                                    \
   /* If our found interval does not contain the input, exit immediately. */        \
   if(*north > *(type*)lower || *(type*)upper > *south) return EINA_FALSE;          \
                                                                                    \
   /* Check to see if our element is on the lower end of the found interval. */     \
   if(*north == *(type*)lower) { *north = *(type*)upper + modulus; edit++; }        \
                                                                                    \
   /* Check to see if our element is on the upper end of the found interval. */     \
   if(*south == *(type*)upper) { *south = *(type*)lower - modulus; edit++; }        \
                                                                                    \
   /* If the element is in the middle of the found interval, split the interval. */ \
   if(!edit)                                                                        \
   {                                                                                \
      type new_lower = *(type*)upper + set->modulus;                                \
      type new_upper = *(type*)lower - set->modulus;                                \
                                                                                    \
      eina_inarray_insert_at(set->intervals, next, NULL);                           \
      north = eina_inarray_nth(set->intervals, next); *north   = new_lower;         \
      south = eina_inarray_nth(set->intervals, prev); *south++ = new_upper;         \
   } }                                                                              \

   TYPE_SWITCH(SET_DETACH, set);
   #undef SET_DETACH

   // If the found interval is equal to the input interval, detach it.
   if(edit == 2) eina_inarray_remove_at(set->intervals, prev);

   return EINA_TRUE;
}

ENG_API Bool
eng_set_attach_interval(Set *set, void *lower, void *upper)
{
   Indx prev, next;

   prev = eng_set_has_interval(set, lower, upper);
   if(prev == INDX_MAX) return EINA_FALSE;
   next = prev + 1;

   return _set_attach(set, lower, upper, prev, next);
}

ENG_API Bool
eng_set_detach_interval(Set *set, void *lower, void *upper)
{
   Indx prev, next;

   prev = eng_set_has_interval(set, lower, upper);
   if(prev == INDX_MAX) return EINA_FALSE;
   next = prev + 1;

   return _set_detach(set, lower, upper, prev, next);
}

ENG_API Indx
eng_set_has_interval(Set *set, void *lower, void *upper)
{
   Indx next = _set_search(set, upper);
   Indx prev = _set_search(set, lower);

   // We do not have the interval if all of it's elements are not present.
   if(next != prev) return VOID;
   else             return next;
}

ENG_API Bool
eng_set_attach(Set *set, void *element)
{
   Indx next = _set_search(set, element);
   Indx prev = next - 1;

   return _set_attach(set, element, element, prev, next);
}

ENG_API Bool
eng_set_detach(Set *set, void *element)
{
   Indx prev = _set_search(set, element);
   Indx next = prev + 1;

   return _set_detach(set, element, element, prev, next);
}

ENG_API Indx
eng_set_has_element(Set *set, void *element)
{
   return _set_search(set, element);
}


/*** Set Query Functions ***/

ENG_API Sclr
eng_set_find_size(Set *set)
{
   Indx intervals = eina_inarray_count(set->intervals) - INDEXING_OFFSET;
   Sclr size;

   #define SET_FIND_SIZE(type)                              \
   for(Indx count = 1; count < intervals; count++)          \
   {                                                        \
     type *lower = eina_inarray_nth(set->intervals, count); \
     type *upper = lower + 1;                               \
                                                            \
     size += *upper - *lower;                               \
   }

   TYPE_SWITCH(SET_FIND_SIZE, set);
   #undef SET_FIND_SIZE

   return size / set->modulus;
}

ENG_API void *
eng_set_find_smallest(Set *set)
{
   eina_inarray_replace_at(set->intervals, 0, eina_inarray_nth(set->intervals, 1));
   return eina_inarray_nth(set->intervals, 0);
}

ENG_API void *
eng_set_find_largest(Set *set)
{
   Indx top_interval = eina_inarray_count(set->intervals) - INDEXING_OFFSET;
   eina_inarray_replace_at(set->intervals, 0, eina_inarray_nth(set->intervals, top_interval));
   Byte *lower = eina_inarray_nth(set->intervals, 0);
   Byte *upper = eina_inarray_nth(set->intervals, 1);

   return (upper -= (lower - upper) / 2);
}


/*** Inter-Set Operations ***/

ENG_API Set *
eng_set_union(Set *a, Set *b)
{
   // Currently we require that Sets have the same underlying metadata to be merged.
   if(a->type != b->type || a->offset != b->offset || a->modulus != b->modulus) return NULL;

   Set *result = eng_set_create(a->type, a->modulus, a->offset);

   Indx i = 0, m = eina_inarray_count(a->intervals);
   Indx j = 0, n = eina_inarray_count(b->intervals);

   #define SET_UNION(type) {                                                                       \
   type target_a, target_b;                                                                        \
   while(i < m && j < n)                                                                           \
   {                                                                                               \
      target_a = *(type*)eina_inarray_nth(a->intervals, i);                                        \
      target_b = *(type*)eina_inarray_nth(b->intervals, j);                                        \
                                                                                                   \
           if (target_a <  target_b)  { eina_inarray_push(result->intervals, &target_a); i += 1; } \
      else if (target_a >  target_b)  { eina_inarray_push(result->intervals, &target_b); j += 1; } \
      else  /*(target_a == target_b)*/{ eina_inarray_push(result->intervals, &target_a); i += 1;   \
                                                                                         j += 1; } \
   }                                                                                               \
                                                                                                   \
   /* Only one of these will run, the other will short-circuit due to the above while(). */        \
   while(i < m) { eina_inarray_push(result->intervals, &target_a); i += 1; }                       \
   while(j < n) { eina_inarray_push(result->intervals, &target_b); j += 1; } }

   TYPE_SWITCH(SET_UNION, result);
   #undef SET_UNION

   return result;
}
/*
ENG_API Set *
eng_set_intersection(Set *a, Set *b)
{
}

ENG_API Set *
eng_set_difference(Set *a, Set *b)
{
}

ENG_API Set *
eng_set_complement(Set *set)
{
}
*/

