#include "tool/cordic.h"
#include "../engineer.h"

// Gains
Sclr cordic_gain_c;
Sclr cordic_gain_h;

// LUTs
Sclr cordic_lut_c[Angl_SCALE]; // Circular Lookup Table.
Sclr cordic_lut_h[Angl_SCALE]; // Hyperbolic Lookup Table.
Sclr cordic_lut_m[Angl_SCALE]; // Hyperbolic Repeat Mask Lookup Table.

void
cordic_init()
{
   #ifdef CORDIC_LINEAR
   cordic_linear_init();
   #endif
   cordic_circular_init();
   cordic_hyperbolic_init();
}

/*** Linear CORDIC ***/

#ifdef CORDIC_LINEAR
// This is mostly useless, it's included as a simplified example of a working CORDIC.
// For educational purposes only.
Vctr cordic_lut_l[Angl_SCALE]; // Linear Lookup Table.

void
cordic_linear_init()
{
   // Compute our Lookup Table for the linear CORDIC algorithims.
   Vctr t = Vctr_BASIS;

   for (uint16_t i = 0; i < Vctr_SCALE; ++i)
   {
      cordic_lut_l[i] = t;
      t = t >> 1;
   }
}

void
cordic_linear_ymode(Vctr *x, Vctr *y, Vctr *z)
{
   Vctr ds = 0;

   for (uint16_t i = 0; i < Vctr_SCALE; ++i)
   {
      ds = *y >> (Vctr_SCALE - 1); // Most Significant Bit

      *y = *y - (((*x >> i)         ^ ds) - ds);
      *z = *z + (((cordic_lut_l[i]) ^ ds) - ds);
   }
}

void
cordic_linear_zmode(Vctr *x, Vctr *y, Vctr *z)
{
   Vctr ds = 0;

   for (uint16_t i = 0; i < Vctr_SCALE; ++i)
   {
      ds = *z >> (Vctr_SCALE - 1); // Most Significant Bit

      *y = *y + (((*x >> i)         ^ ds) - ds);
      *z = *z - (((cordic_lut_l[i]) ^ ds) - ds);
   }
}
#endif


/*** Circular CORDIC ***/

void
cordic_circular_init()
{
   // Compute our Lookup Table for the circular CORDIC computers.
   double t = 1.0;
   for (uint16_t i = 0; i < Angl_SCALE; ++i)
   {
      cordic_lut_c[i] = (Sclr)(atan(t) * Angl_BASIS);
      t /= 2;
   }

   // Calculate circular gain by evaluating cos(0) without inverse gain.
   Angl x = Angl_BASIS,
        y = 0,
        z = 0;

   cordic_circular_zmode(&x, &y, &z);
   cordic_gain_c = (Angl_BASIS << Angl_RADIX) / x;
}

void
cordic_circular_ymode(Vctr *x, Vctr *y, Vctr *z)
{
   Vctr mask = 0;
   Vctr xbuf = 0;

   for (uint16_t i = 0; i < Angl_SCALE; i++)
   {
      mask = *y >> (Angl_SCALE - 1);  // Most Significant Bit.
      xbuf = *x + (((*y >> i)         ^ mask) - mask);
      *y   = *y - (((*x >> i)         ^ mask) - mask);
      *z   = *z + (((cordic_lut_c[i]) ^ mask) - mask);
      *x   = xbuf;
   }
}

void
cordic_circular_aymode(Vctr *x, Vctr *y, Vctr *z)
{
   Vctr mask = 0;
   Vctr xbuf = 0;
   Vctr ybuf = *y;  *y = 0;

   for (uint32_t i = 0; i < Angl_SCALE; i++)
   {
      mask = (*y + ybuf) >> (Angl_SCALE - 1);  // Most Significant Bit.
      xbuf = *x - (((*y >> i)         ^ mask) - mask);
      *y   = *y + (((*x >> i)         ^ mask) - mask);
      *z   = *z - (((cordic_lut_c[i]) ^ mask) - mask);
      *x   = xbuf;
   }
}

void
cordic_circular_zmode(Vctr *x, Vctr *y, Vctr *z)
{
   Vctr mask = 0;
   Vctr xbuf = 0;

   for (uint16_t i = 0; i < Angl_SCALE; i++)
   {
      mask = *z >> (Angl_SCALE - 1);  // Most Significant Bit.
      xbuf = *x - (((*y >> i)         ^ mask) - mask);
      *y   = *y + (((*x >> i)         ^ mask) - mask);
      *z   = *z - (((cordic_lut_c[i]) ^ mask) - mask);
      *x   = xbuf;
   }
}


/*** Hyperbolic CORDIC ***/
// The 'x' iteration has the opposite sign. Iterations 4, 7, .. 3k+1 are repeated.
// Iteration 0 is not performed.

void
cordic_hyperbolic_init() // Fixme //
{
   Vctr     t;
   uint32_t i;

   // Compute our Lookup Table for the hyperbolic CORDIC computers.
   t = Angl_BASIS;
   for (i = 0; i < Angl_SCALE; i++) // ++i?
   {
      t = t >> 1;
      cordic_lut_h[i] = (Sclr)(log((Angl_BASIS + t) / (Angl_BASIS - t))) >> 1;
   }

   // Set up our usage mask for the cyclical doubled iteration of the hyperbolic cordic.
   t = 3;
   for (i = 0; i < Angl_SCALE; i++)
   {
     if(t) { cordic_lut_m[i] = 0;                          t--;   }
     else  { cordic_lut_m[i] = (Sclr)ldexp(1, Angl_SCALE); t = 2; }
   }

   // Calculate hyperbolic gain.
   Vctr x = Angl_BASIS,
        y = 0,
        z = 0;
   cordic_hyperbolic_zmode(&x, &y, &z);
   cordic_gain_h = (Angl_BASIS << Angl_RADIX) / x;
}

void
cordic_hyperbolic_ymode(Vctr *x, Vctr *y, Vctr *z)
{
   Vctr mask = 0;
   Vctr xbuf = 0;

   for (unsigned int i = 0; i < Angl_SCALE; i++)
   {
      mask = *y >> (Angl_SCALE - 1);  // Most Significant Bit
      xbuf = *x - (((*y >> i)         ^ mask) - mask);
      *y   = *y - (((*x >> i)         ^ mask) - mask);
      *z   = *z + (((cordic_lut_c[i]) ^ mask) - mask);
      *x   = xbuf;

      xbuf = *x - ((((*y >> i)         ^ mask) - mask) & cordic_lut_m[i]);
      *y   = *y - ((((*x >> i)         ^ mask) - mask) & cordic_lut_m[i]);
      *z   = *z + ((((cordic_lut_c[i]) ^ mask) - mask) & cordic_lut_m[i]);
      *x   = xbuf;
   }
}

void
cordic_hyperbolic_zmode(Vctr *x, Vctr *y, Vctr *z)
{
   Vctr mask = 0;
   Vctr xbuf = 0;

   for (unsigned int i = 0; i < Angl_SCALE; i++)
   {
      mask = *z >> (Angl_SCALE - 1);  // Most Significant Bit
      xbuf = *x + (((*y >> i)         ^ mask) - mask);
      *y   = *y + (((*x >> i)         ^ mask) - mask);
      *z   = *z - (((cordic_lut_c[i]) ^ mask) - mask);
      *x   = xbuf;

      xbuf = *x + ((((*y >> i)         ^ mask) - mask) & cordic_lut_m[i]);
      *y   = *y + ((((*x >> i)         ^ mask) - mask) & cordic_lut_m[i]);
      *z   = *z - ((((cordic_lut_c[i]) ^ mask) - mask) & cordic_lut_m[i]);
      *x   = xbuf;
   }
}

