#include "eng_node.h"

typedef struct Eng_Node_Data Eng_Node_Data;

struct Eng_Node_Data
{
   String *game;  // The title/label of the game the node is (partially) hosting.

   // Networking Configuration. (Rough Draft)
   //String    *domain;  // The DNS address of the peer cluster's host domain.
   //Eina_Hash *peers;   // The set of connected server peer nodes.
   //Indx       port;    // The IP port that the game prefers to communicate on.
   //Indx       mode;    // The network mode of this Node, standalone(0), client(1), server(2).

   // Scene Table.
   Eina_Hash *scenes;  // Keyed by identity, contains Eng_Thread_Data*s.  Should be changed to an Eng_Set, since _eng_thread_that() already serves that purpose.

   // Node Scope Module(Component/System) and Asset Data, usually loaded from a file.
   String    *modules_path;  // Where the Module folder is located relative to the root drive.
   Eina_Hash *modules;       // Contains Eng_Node_Modules *s, keyed by Module Digest.

   String    *commons_path;  // Where the Asset folder is located relative to the root drive.
   Eina_Hash *commons;       // Contains Eina_Hash*s, keyed by Asset Flavor.

   //String    *storage_path;  // Where the Storage folder is located relative to the root drive.
   //Eina_Hash *storage;       // Contains Eina_Stringshare*s, keyed by Module Digest.

   // EFL GUI widget tables.
   Eina_Hash *consoles;
   Eina_Hash *viewports;
};

static Eng_Node_Data *node;


/*** Thread Handling ***/

ENG_PRIVATE Thread
_eng_node_boot()
{
   // Start up the underlying threading archtecture.
   Thread identity = _eng_thread_boot();

   node = malloc(sizeof(Eng_Node_Data));

   // Set up our path elements.
   if(node->game == NULL) node->game = "Default Game Title";

   //node->peers = eina_hash_string_superfast_new(eng_free_cb);
   //node->mode  = 0; // Standalone = 0; Client = 1; Server = 2;.

   node->scenes = eina_hash_int64_new(eng_free_cb);

   node->modules_path = ".."; // Alternative : getcwd(pd->path, sizeof(PATH_MAX));
   node->modules = eina_hash_int64_new(eng_empty_cb);

   node->commons_path = "..";
   node->commons = eina_hash_string_superfast_new(eng_free_cb);

   //node->storage_path = NULL;
   //node->storage = eina_hash_int64_new(eng_empty_cb);

   printf("Module Path: %s, Title: %s\n", node->modules_path, node->game);
   printf("Active Threads Test: %d\n", ecore_thread_available_get());

   return identity;
}

ENG_PRIVATE void
_eng_node_halt()
{
   //eina_hash_free(node->storage);
   eina_hash_free(node->commons);
   eina_hash_free(node->modules);

   eina_hash_free(node->scenes);

   //eina_hash_free(node->peers);

   free(node);

   _eng_thread_halt();
}


/*** Node Property Handlers. ***/

ENG_PRIVATE void
_eng_node_game_set(String *game)
{
   eina_stringshare_del(game);
   node->game = eina_stringshare_add(game);
}

ENG_PRIVATE String *
_eng_node_game_get()
{
   return node->game;
}

ENG_PRIVATE void
_eng_node_modules_path_set(String *path)
{
   eina_stringshare_del(path);
   node->modules_path = eina_stringshare_add(path);
}

ENG_PRIVATE String *
_eng_node_modules_path_get()
{
   return node->modules_path;
}

ENG_PRIVATE void
_eng_node_commons_path_set(String *path)
{
   eina_stringshare_del(path);
   node->commons_path = eina_stringshare_add(path);
}

ENG_PRIVATE String *
_eng_node_commons_path_get()
{
   return node->commons_path;
}
/*
ENG_PRIVATE void
_eng_node_storage_path_set(String *path)
{
   eina_stringshare_del(path);
   node->storage_path = eina_stringshare_add(path);
}

ENG_PRIVATE String *
_eng_node_storage_path_get()
{
   return node->storage_path;
}
*/


/*** Node_Module Filename Parsing ***/
/*
static String *
_eng_node_file_flavor(Digest module)
{
   String *file = eina_hash_find(node->storage, &module);
   if(!file) return NULL;

   Indx length = strlen(file);
   if(strstr(file, ".Component.so")) return "Component"; else
   if(strstr(file, ".System.so"))    return "System";    else
                                     return  NULL;
}
*/
static void
_eng_node_file_list__work(void *args, void *result)
{
   Eina_List *files = ecore_file_ls(node->modules_path), *this, *next;
   String    *file, *target = *(String**)args;

   // A target cam be a Domain, a Domain.Module, or a Domain.Module.Flavor string.
   if(target)
   {
      char _target[strlen(target) + 2];  strcpy(_target, target);
           _target[strlen(target) + 1] = *".";
           _target[strlen(target) + 2] = *"\0";

      EINA_LIST_FOREACH_SAFE(files, this, next, file)
         if(file != strstr(file, _target))
         {
            files = eina_list_remove_list(files, this);
            free((char*)file);
         }
   }

   *(Eina_List**)result = files;
}

ENG_PRIVATE Eina_List *
_eng_node_file_list(String *target)
{
   String **call = eng_work_call(THREAD_MIN,  _eng_node_file_list__work,
                                 sizeof(String*), sizeof(Eina_List*));
   *call = target;

   return *(Eina_List**)eng_work_wait(call);
}

ENG_PRIVATE Digest
_eng_node_file_digest(String *file)
{
   char _file[strlen(file) + 1];  strcpy(_file, file);

   char *suffix = strstr(_file, ".so");
        *suffix = *"\0";

   return eng_digest(_file);
}


/*** Node_Module Management ***/

static void
_eng_node_has_module__work(void *args, void *result)
{
   Digest *module = args;

   *(Eng_Node_Module**)result = eina_hash_find(node->modules, module);
}

ENG_PRIVATE Eng_Node_Module *
_eng_node_has_module(Digest module)
{
   Digest *call = eng_work_call(THREAD_MIN, _eng_node_has_module__work,
                                sizeof(Digest), sizeof(Eng_Node_Module*));
   *call = module;

   return *(Eng_Node_Module**)eng_work_wait(call);
}

static Bool
_eng_node_attach_module(Eng_Node_Module *module)
{
   return eina_hash_add(node->modules, &module->digest, module);
}

static Bool
_eng_node_detach_module(Eng_Node_Module *module)
{
   return eina_hash_del_by_key(node->modules, &module->digest);
}


/*** Node_Module Dependent_Scene Management ***/

struct Node_Module_Work
{
   Eng_Node_Module *module;
   Entity           scene;
};

static void
_eng_node_module_has_scene__work(void *args, void *result)
{
   struct Node_Module_Work *info = args;

   *(Bool*)result = eng_set_has_element(info->module->dependent_scenes, &info->scene);
}

ENG_PRIVATE Bool
_eng_node_module_has_scene(Eng_Node_Module *module, Entity scene)
{
   struct Node_Module_Work *call = eng_work_call(THREAD_MIN, _eng_node_module_has_scene__work,
                                                 sizeof(struct Node_Module_Work), sizeof(Bool));
   call->module = module;
   call->scene  = scene;

   return *(Bool*)eng_work_wait(call);
}

static Bool
_eng_node_module_attach_scene(Eng_Node_Module *module, Entity scene)
{
   return eng_set_attach(module->dependent_scenes, &scene);
}

static Bool
_eng_node_module_detach_scene(Eng_Node_Module *module, Entity scene)
{
   return eng_set_detach(module->dependent_scenes, &scene);
}


/*** Node_Module Loading ***/

static String *
_eng_node_module_load(Eng_Node_Module *module)
{
   // Make sure our Module file exists before trying to load it.
   Eina_List        *files = ecore_file_ls(node->modules_path);
   char             *file;
   Eina_Stringshare *match = NULL;
   uint8_t           error;

   EINA_LIST_FREE(files, file)
   {
      if(_eng_node_file_digest(file) == module->digest) match = eina_stringshare_add(file);
      free(file);
   }
   if(!match) { error = 1; goto JAIL; }

   module->eina = eina_module_new(match);
   if(!module->eina) { error = 2; goto JAIL; }
   eina_module_load(module->eina);

   module->get_symbol = eina_module_symbol_get(module->eina, "_eng_module__get_symbol");
   if(!module->get_symbol) { error = 3; goto JAIL; }
   module->get_domain = eina_module_symbol_get(module->eina, "_eng_module__get_domain");
   if(!module->get_domain) { error = 3; goto JAIL; }
   module->get_module = eina_module_symbol_get(module->eina, "_eng_module__get_module");
   if(!module->get_module) { error = 3; goto JAIL; }
   module->get_flavor = eina_module_symbol_get(module->eina, "_eng_module__get_flavor");
   if(!module->get_flavor) { error = 3; goto JAIL; }

   module->node_module_init = eina_module_symbol_get(module->eina, "_eng_node_module_init");
   if(!module->node_module_init) { error = 3; goto JAIL; }
   module->node_module_free = eina_module_symbol_get(module->eina, "_eng_node_module_free");
   if(!module->node_module_free) { error = 3; goto JAIL; }

   module->scene_module_init = eina_module_symbol_get(module->eina, "_eng_scene_module_init");
   if(!module->scene_module_init) { error = 3; goto JAIL; }
   module->scene_module_free = eina_module_symbol_get(module->eina, "_eng_scene_module_free");
   if(!module->scene_module_free) { error = 3; goto JAIL; }

   module->shard_module_init = eina_module_symbol_get(module->eina, "_eng_shard_module_init");
   if(!module->shard_module_init) { error = 3; goto JAIL; }
   module->shard_module_free = eina_module_symbol_get(module->eina, "_eng_shard_module_free");
   if(!module->shard_module_free) { error = 3; goto JAIL; }

   module->dependent_scenes = eng_set_create(64, 1, 0);

   return match;

   JAIL :

   switch(error)
   {
      case 3 : printf("ERROR: Module %s load failed, File missing API function.\n",  match); break;
      case 2 : printf("ERROR: Module %s load failed, File is not an Eina_Module.\n", match); break;
      case 1 : printf("ERROR: Module %"PRIu64" load failed, File not found.\n",
                                                                            module->digest); break;
   }

   switch(error)
   {
      case 3 : eina_module_free(module->eina);  eina_stringshare_del(match);
      case 2 :
      case 1 :
   }
   return NULL;
}

static void
_eng_node_module_free(Eng_Node_Module *module)
{
   eng_set_delete(module->dependent_scenes);
   eina_module_free(module->eina);
   free(module);
}


/*** Node_Component Loading ***/

struct Node_Component_Work
{
   Digest component;
   Entity scene;
};

static Indx
_component_column(Digest component, Digest field)
{
   Eng_Node_Component *module = (void*)_eng_node_has_module(component);

   if(module) return module->component_column(field);
   else       return VOID;
}

static void
_eng_node_component_load__work(void *args, void *result)
{
   struct Node_Component_Work *info = args;
          uint8_t              error;

   // If the Node_Component already exists, attach the target Scene to it.
   Eng_Node_Component *module = (void*)_eng_node_has_module(info->component);
   if(module) { _eng_node_module_attach_scene((void*)module, info->scene); goto PASS; }

   // If not, initialize it!
   module         = malloc(sizeof(Eng_Node_Component));
   module->digest = info->component;

   String *file = _eng_node_module_load((void*)module);

   if(!file)                                               { error = 0; goto JAIL; }
   if(eng_digest(module->get_symbol()) != info->component) { error = 1; goto JAIL; }

   // Initialize the Node_Component Method Access Table.
   module->component_sizeof = eina_module_symbol_get(module->eina, "_eng_component__sizeof");
   if(!module->component_sizeof) { error = 2; goto JAIL; }
   module->component_column = eina_module_symbol_get(module->eina, "_eng_component__column");
   if(!module->component_column) { error = 2; goto JAIL; }

   module->entity_attach = eina_module_symbol_get(module->eina, "_eng_component__entity_attach");
   if(!module->entity_attach) { error = 2; goto JAIL; }
   module->entity_detach = eina_module_symbol_get(module->eina, "_eng_component__entity_detach");
   if(!module->entity_detach) { error = 2; goto JAIL; }
   module->entity_status = eina_module_symbol_get(module->eina, "_eng_component__entity_status");
   if(!module->entity_status) { error = 2; goto JAIL; }

   // Set up the specified Node_Component's downstream functions and internal data.
   module->node_module_init(info->component, NULL);

   _eng_node_attach_module((void*)module);
   _eng_node_module_attach_scene((void*)module, info->scene);

   printf("Component File loaded.  | Pointer: %p | Digest: %"PRIu64" | File: %s\n",
      module, module->digest, file);

   PASS : *(Eng_Node_Component**)result = module; return;

   JAIL :

   switch(error)
   {
      case 2 : printf("ERROR : Module %s load failed, File missing API function.\n", file); break;
      case 1 : printf("ERROR : Module %s load failed, Component Symbol faulty.\n",   file); break;
      case 0 :                                                                              break;
   }

   switch(error)
   {
      case 2 :
      case 1 : _eng_node_module_free((void*)module);  [[fallthrough]];
      case 0 : *(Eng_Node_Component**)result = NULL;
   }
}

ENG_PRIVATE Eng_Node_Component *
_eng_node_component_load(Digest component, Entity scene)
{
   struct Node_Component_Work *call = eng_work_call(THREAD_MIN, _eng_node_component_load__work,
                                   sizeof(struct Node_Component_Work), sizeof(Eng_Node_Component*));
   call->component = component;
   call->scene     = scene;

   return *(Eng_Node_Component**)eng_work_wait(call);
}

static void
_eng_node_component_free__work(void *args, void *result)
{
   struct Node_Component_Work *info = args;

   Eng_Node_Module *module = _eng_node_has_module(info->component);
   if(!module) { *(Bool*)result = EINA_FALSE; return; }

   // If there are still other Scenes attached to the target Node_Component, return without freeing.
   if(!_eng_node_module_detach_scene(module, info->scene)) { *(Bool*)result = EINA_FALSE; return; }
   if(eng_set_find_size(module->dependent_scenes))         { *(Bool*)result = EINA_TRUE;  return; }

   _eng_node_detach_module(module);
   module->node_module_free();

   //eng_vault_delete(module->field_info);

   _eng_node_module_free(module);

   *(Bool*)result = EINA_TRUE;
}

ENG_PRIVATE Bool
_eng_node_component_free(Digest component, Entity scene)
{
   struct Node_Component_Work *call = eng_work_call(THREAD_MIN, _eng_node_component_free__work,
                                                  sizeof(struct Node_Component_Work), sizeof(Bool));
   call->component = component;
   call->scene     = scene;

   return *(Bool*)eng_work_wait(call);
}


/*** System Class Module Loading ***/

struct Node_System_Work
{
   Digest system;
   Entity scene;
};

static void
_eng_node_system_load__work(void *args, void *result)
{
   struct Node_System_Work *info = args;
          uint8_t           error;

   // If the Node_System already exists, attach the target Scene to it.
   Eng_Node_System *module = (void*)_eng_node_has_module(info->system);
   if(module) { _eng_node_module_attach_scene((void*)module, info->scene); goto PASS; }

   // If not, initialize it!
   module = malloc(sizeof(Eng_Node_System));
   module->digest = info->system;

   String *file = _eng_node_module_load((void*)module);

   if(!file)                                            { error = 0; goto JAIL; }
   if(eng_digest(module->get_symbol()) != info->system) { error = 1; goto JAIL; }

   // Set up our System Class API.
   #define SYMBOL_GET(a, b) eina_module_symbol_get(a, b)

   module->system_sizeof = eina_module_symbol_get(module->eina, "_eng_system__system_sizeof");
   if(!module->system_sizeof) { error = 2; goto JAIL; }
   module->entity_sizeof = eina_module_symbol_get(module->eina, "_eng_system__entity_sizeof");
   if(!module->entity_sizeof) { error = 2; goto JAIL; }

   module->system_prereqs    = SYMBOL_GET(module->eina, "_eng_system__system_prereqs");
   if(!module->system_prereqs)    { error = 2; goto JAIL; }
   module->component_prereqs = SYMBOL_GET(module->eina, "_eng_system__component_prereqs");
   if(!module->component_prereqs) { error = 2; goto JAIL; }
   module->component_ignored = SYMBOL_GET(module->eina, "_eng_system__component_ignored");
   if(!module->component_ignored) { error = 2; goto JAIL; }

   module->entity_is_targeted = SYMBOL_GET(module->eina, "_eng_system__entity_is_targeted");
   if(!module->entity_is_targeted) { error = 2; goto JAIL; }
   module->entity_is_ignored  = SYMBOL_GET(module->eina, "_eng_system__entity_is_ignored");
   if(!module->entity_is_ignored)  { error = 2; goto JAIL; }

   module->entity_attach = SYMBOL_GET(module->eina, "_eng_system__entity_attach");
   if(!module->entity_attach) { error = 2; goto JAIL; }
   module->entity_detach = SYMBOL_GET(module->eina, "_eng_system__entity_detach");
   if(!module->entity_detach) { error = 2; goto JAIL; }

   module->entity_import = SYMBOL_GET(module->eina, "_eng_system__entity_import");
   if(!module->entity_import) { error = 2; goto JAIL; }
   module->entity_export = SYMBOL_GET(module->eina, "_eng_system__entity_export");
   if(!module->entity_export) { error = 2; goto JAIL; }

   module->entity_upkeep = SYMBOL_GET(module->eina, "_eng_system__entity_upkeep");
   if(!module->entity_upkeep) { error = 2; goto JAIL; }
   module->entity_notify = SYMBOL_GET(module->eina, "_eng_system__entity_notify");
   if(!module->entity_notify) { error = 2; goto JAIL; }
   module->entity_update = SYMBOL_GET(module->eina, "_eng_system__entity_update");
   if(!module->entity_update) { error = 2; goto JAIL; }

   #undef SYMBOL_GET

   // Set up the specified Node_System's downstream functions and static global data.
   module->node_module_init(info->system, _component_column);

   _eng_node_attach_module((void*)module);
   _eng_node_module_attach_scene((void*)module, info->scene);

   printf("System File loaded.  | Pointer: %p | Digest: %"PRIu64" | File: %s\n",
      module, module->digest, file);

   PASS : *(Eng_Node_System**)result = module; return;

   JAIL :

   switch(error)
   {
      case 2 : printf("ERROR: Module %s load failed, File missing API function.\n", file); break;
      case 1 : printf("ERROR: Module %s load failed, System digest faulty.\n",      file); break;
      case 0 :                                                                             break;
   }

   switch(error)
   {
      case 2 :
      case 1 : _eng_node_module_free((void*)module);  [[fallthrough]];
      case 0 : *(Eng_Node_System**)result = NULL;
   }
}

ENG_PRIVATE Eng_Node_System *
_eng_node_system_load(Digest system, Entity scene)
{
   struct Node_System_Work *call = eng_work_call(THREAD_MIN, _eng_node_system_load__work,
                                                 sizeof(struct Node_System_Work),
                                                 sizeof(Eng_Node_System*));
   call->system = system;
   call->scene  = scene;

   return *(Eng_Node_System**)eng_work_wait(call);
}

static void
_eng_node_system_free__work(void *args, void *result)
{
   struct Node_System_Work *info = args;

   Eng_Node_Module *module = _eng_node_has_module(info->system);
   if(!module) { *(Bool*)result = EINA_FALSE; return; }

   // If there are still other Scenes dependent on the target Node_System, return without freeing.
   if(!_eng_node_module_detach_scene(module, info->scene)) { *(Bool*)result = EINA_FALSE; return; }
   if(eng_set_find_size(module->dependent_scenes))         { *(Bool*)result = EINA_TRUE;  return; }

   _eng_node_detach_module(module);
   module->node_module_free();

   _eng_node_module_free(module);

   *(Bool*)result = EINA_TRUE;
}

ENG_PRIVATE Bool
_eng_node_system_free(Digest system, Entity scene)
{
   struct Node_System_Work *call = eng_work_call(THREAD_MIN, _eng_node_system_free__work,
                                                 sizeof(struct Node_System_Work),
                                                 sizeof(Bool));
   call->system = system;
   call->scene  = scene;

   return *(Bool*)eng_work_wait(call);
}

