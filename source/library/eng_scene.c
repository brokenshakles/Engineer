#include "eng_scene.h"

typedef struct Eng_Scene_Data Eng_Scene_Data;

struct Eng_Scene_Data
{
   String         *name;
   Eng_Scene_Phase phase;

   Indx          clockrate;    // This value controls the FPS the scene is set to run at.
   Ecore_Timer  *iterator;     // Triggers our frame update, the frequency can be changed.
   Eina_Inarray *timecard;     // Stores the completion time for the present and past frames.
   Eina_Inarray *system_order; // Stores the order that the Scene's attached Systems are updated in.

   Eina_Hash *shards;          // Keyed by Identity, contains Eng_Thread_Data *s.  Should be changed to an Eng_Set, since _eng_thread_that() already serves that purpose.
   Eina_Hash *modules;         // Keyed by Digest. Contains Eng_Scene_Module/Component/System *s.
};

static THREAD_LOCAL Eng_Scene_Data *scene;


/*** Thread Handling ***/

ENG_PRIVATE void *
_eng_scene_init(void *data, Eina_Thread id) // Maybe id is for parent thread?
{
   Eng_Thread_Init *info = data;

   // Start up the underlying thread archtecture.
   info->scene = _eng_thread_init(THREAD_MIN, id);

   scene       = malloc(sizeof(Eng_Scene_Data));
   scene->name = info->name;

   // Set up the Scene iterator default clockrate.
   scene->system_order = eina_inarray_new(sizeof(uint64_t), 0);
   scene->timecard     = eina_inarray_new(sizeof(uint64_t), 3);
   scene->clockrate    = 32;
   scene->iterator     = ecore_timer_add((double)1/scene->clockrate, _eng_scene_update, NULL);

   // Set up the Scene's shard and module tracking.
   scene->shards  = eina_hash_int64_new(eng_empty_cb);
   scene->modules = eina_hash_int64_new(eng_empty_cb);

   scene->phase = SCENE_SLEEP;  // We might want some sort of explicit Initialize label here.

   eina_barrier_wait(&info->dispatch);
   _eng_thread_dispatch();

   return NULL;
}

static void
_eng_scene_free__work(void *args EINA_UNUSED, void **result EINA_UNUSED)
{
   ecore_timer_del(scene->iterator);
   eina_inarray_free(scene->timecard);
   eina_inarray_free(scene->system_order);

   eina_hash_free(scene->modules);
   eina_hash_free(scene->shards);

   free(scene);

   _eng_thread_free();
}

ENG_PRIVATE void
_eng_scene_free(Thread scene)
{
   void *call = eng_work_call(scene, _eng_scene_free__work,
                              VOID,  VOID);
   eng_work_wait(call);
}

ENG_PRIVATE Bool
_eng_scene_update(void *data EINA_UNUSED)
{
   double timestamp;

   printf("\nScene Thread Update Checkpoint.\n");
   printf("=================================\n");

   STORE(&scene->phase, SCENE_UPKEEP);

   // Store the timestamp for when this Scene's clock tick begins.
   timestamp = ecore_time_get();

   STORE(&scene->phase, SCENE_UPDATE);

   Eng_Scene_System *module;
   Digest           *system;
   EINA_INARRAY_FOREACH(scene->system_order, system)
   {
      module = (Eng_Scene_System*)_eng_scene_has_module(_eng_thread_this(), *system);
      _eng_shard_update_system(module->dependent_shards, *system);
   }

   STORE(&scene->phase, SCENE_CLEANUP);

   // foreach shard in scene :
   // Delete old entities.

   // Check to see if we are taking more than 85% of our tick time, if so, decrease the clock rate.
   timestamp -= ecore_time_get();
   if (scene->clockrate != 1 && timestamp > 1/scene->clockrate * 0.85)
   {
      scene->clockrate--;
      ecore_timer_interval_set(scene->iterator, 1/scene->clockrate);
   }
   // If TiDi is engaged, but the tick time load is less than 70%, if so, increase the clock rate.
   else if (scene->clockrate != 32 && timestamp < 1/scene->clockrate * 0.70)
   {
      scene->clockrate++;
      ecore_timer_interval_set(scene->iterator, 1/scene->clockrate);
   }
   // Set our completion timestamp for this frame.
   timestamp = ecore_time_get();
   eina_inarray_push(scene->timecard, &timestamp);

   printf("Scene Update End Checkpoint. Clockrate: %d Htz, Timestamp: %1.5f\n",
      scene->clockrate, timestamp);

   return ECORE_CALLBACK_RENEW;
}


/*** EOLIAN Methods ***/
/*
EOLIAN static void
_eng_scene_entity_set(Eo *obj EINA_UNUSED, Eng_Scene_Data *pd,
        Entity scene)
{
   if(pd->entity != NULL_ENTITY) return;

   pd->entity = scene;
}

EOLIAN static Entity
_eng_scene_entity_get(Eo *obj EINA_UNUSED, Eng_Scene_Data *pd)
{
   return pd->entity;
}

EOLIAN static void
_eng_scene_name_set(Eo *obj EINA_UNUSED, Eng_Scene_Data *pd,
        const char *name)
{
   pd->name = eina_stringshare_add(name);
}

EOLIAN static const char *
_eng_scene_name_get(Eo *obj EINA_UNUSED, Eng_Scene_Data *pd)
{
  return pd->name;
}

EOLIAN static void
_eng_scene_phase_set(Eo *obj EINA_UNUSED, Eng_Scene_Data *pd,
        uint32_t phase)
{
   STORE(&pd->phase, phase);
}

EOLIAN static uint32_t
_eng_scene_phase_get(const Eo *obj EINA_UNUSED, Eng_Scene_Data *pd)
{
   return LOAD(&pd->phase);
}
*/

/*** Child Shard Management ***/

ENG_PRIVATE Thread
_eng_scene_sort_entity(Entity target)
{
   // If the given target is an Entity, return it's assigned shard.
   if((target) >= ENTITY_MIN) return eng_entity_shard(target); // Needs to be changed to a real existence test.

   // If the given target is a Thread, return that Thread;
   if((target) >= THREAD_MIN) return target;

   // If there is only one Shard available in the Scene, use that.
   //    Will replace later with performance metric based default selection.
   if(eina_hash_population(scene->shards) == 1)
   {
      Eina_Iterator   *iterator = eina_hash_iterator_key_new(scene->shards);
      Thread          *shard;
      Bool EINA_UNUSED hasnext  = eina_iterator_next(iterator, (void**)&shard);
                                  eina_iterator_free(iterator);
        return *shard;
   }
   else return VOID;
}

static void
_eng_scene_attach_shard__work(void *args, void *result)
{
   Eng_Thread_Data *shard = _eng_thread_that(*(Thread*)args);

   *(Bool*)result = eina_hash_add(scene->shards, shard, shard);
}

ENG_PRIVATE Bool
_eng_scene_attach_shard(Thread scene, Thread shard)
{
   Thread *call = eng_work_call(scene, _eng_scene_attach_shard__work,
                                sizeof(Thread), sizeof(Bool));
  *call = shard;

   return *(Bool*)eng_work_wait(call);
}

static void
_eng_scene_detach_shard__work(void *args, void *result)
{
   Eng_Thread_Data *shard = _eng_thread_that(*(Thread*)args);

   *(Bool*)result = eina_hash_del_by_key(scene->shards, shard);
}

ENG_PRIVATE Bool
_eng_scene_detach_shard(Thread scene, Thread shard)
{
   Thread *call = eng_work_call(scene, _eng_scene_detach_shard__work,
                                sizeof(Thread), sizeof(Bool));
  *call = shard;

   return *(Bool*)eng_work_wait(call);
}


/*** Scene_Module Utility ***/

static void
_eng_scene_has_module__work(void *args, void *result)
{
   Eng_Scene_Module **module = eina_hash_find(scene->modules, args);
   if(!module) return;

   *(Eng_Scene_Module**)result = *module;
}

ENG_PRIVATE Eng_Scene_Module *
_eng_scene_has_module(Thread scene, Digest module)
{
   Digest *call = eng_work_call(scene,          _eng_scene_has_module__work,
                                sizeof(Digest), sizeof(Eng_Scene_Module*));
  *call = module;

   return *(Eng_Scene_Module**)eng_work_wait(call);
}

ENG_PRIVATE Eng_Scene_System *
_eng_scene_module_has_system(Eng_Scene_Module *module, Digest system)
{
   return eina_hash_find(module->dependent_systems, &system);
}


/*** Scene_Module Dependent Shard Tracking ***/

struct Shard_Info
{
   Digest module;
   Entity shard;
};

static void
_eng_scene_module_attach_shard__work(void *args, void *result)
{
   struct Shard_Info *info   = args;
   Eng_Scene_Module  *module = _eng_scene_has_module(_eng_thread_this(), info->module);

   if(module && eng_set_attach(module->dependent_shards, &info->shard))
        *(Eng_Scene_Module**)result = module;
   else *(Eng_Scene_Module**)result = NULL;
}

ENG_PRIVATE Eng_Scene_Module *
_eng_scene_module_attach_shard(Thread scene, Digest module, Thread shard)
{
   struct Shard_Info *call = eng_work_call(scene, _eng_scene_module_attach_shard__work,
                                           sizeof(struct Shard_Info), sizeof(Eng_Scene_Module*));
   call->module = module;
   call->shard  = shard;

   return *(Eng_Scene_Module**)eng_work_wait(call);
}

static void
_eng_scene_module_detach_shard__work(void *args, void *result)
{
   struct Shard_Info *info   = args;
   Eng_Scene_Module  *module = _eng_scene_has_module(_eng_thread_this(), info->module);
   if(!module) return;

   *(Bool*)result = eng_set_detach(module->dependent_shards, &info->shard);
}

ENG_PRIVATE Bool
_eng_scene_module_detach_shard(Thread scene, Digest module, Thread shard)
{
   struct Shard_Info *call = eng_work_call(scene, _eng_scene_module_detach_shard__work,
                                           sizeof(struct Shard_Info), sizeof(Bool));
   call->module = module;
   call->shard  = shard;

   return *(Bool*)eng_work_wait(call);
}


/*** Component Module Loading and Unloading ***/

struct Component_Info
{
   Digest component;
   Digest dependent_system;
};

static void
_component_load__work(void *args, void *result)
{
   struct Component_Info *info = args;

   // Check to see if the target Scene_Component is already loaded into the Scene.
   Eng_Scene_Component *module = (void*)_eng_scene_has_module(_eng_thread_this(), info->component);
   if(module)
      { eina_hash_set(module->dependent_systems, &info->dependent_system, module);  goto RETURN; }

   // If the Node_Component is already loaded, its refcount is incremented instead.
   Eng_Node_Component *node = _eng_node_component_load(info->component, _eng_thread_this());
   if(!node) return;

   // If not, allocate and initialize a new Eng_Scene_Component_Scene entry.
   module                    = malloc(sizeof(Eng_Scene_Component));
   module->node              = node;
   module->dependent_systems = eina_hash_int64_new(eng_empty_cb);
   module->dependent_shards  = eng_set_create(64, 1, VOID);

   // Add this Component_Modules parent to it's dependent_systems, then register it with this Scene.
   eina_hash_set(module->dependent_systems, &info->dependent_system, module);
   eina_hash_set(scene->modules, &info->component, module);

   RETURN : *(Bool*)result = EINA_TRUE;
}

static Bool
_component_load(Thread scene, Digest component, Digest dependent_system)
{
   struct Component_Info *call = eng_work_call(scene, _component_load__work,
                                               sizeof(struct Component_Info), sizeof(Bool));
   call->component        = component;
   call->dependent_system = dependent_system;

   return *(Bool*)eng_work_wait(call);
}

static void
_component_free__work(void *args, void *result)
{
   struct Component_Info *info = args;

   Eng_Scene_Module *module = _eng_scene_has_module(_eng_thread_this(), info->component);
   if(!module) return;

   // Short-Circuit if we still have other dependent_systems.
      eina_hash_del_by_key(module->dependent_systems, &info->dependent_system);
   if(eina_hash_population(module->dependent_systems)) goto RETURN;

   eina_hash_del_by_key(scene->modules, &info->component);  free(module);
   _eng_node_component_free(info->component, _eng_thread_this());

   RETURN : *(Bool*)result = EINA_TRUE;
}

static Bool
_component_free(Entity scene, Digest component, Digest dependent_system)
{
   struct Component_Info *call = eng_work_call(scene, _component_free__work,
                                               sizeof(struct Component_Info), sizeof(Bool));
   call->component        = component;
   call->dependent_system = dependent_system;

   return *(Bool*)eng_work_wait(call);
}


/*** System Module Loading and Unloading ***/

struct System_Info
{
   Digest system;
   Digest dependent_system;
};

static void
_system_sorter(Set *system_set, Digest *system, Eina_Inarray *order_inverse)
{
   // This makes sure we visit each System only once.
   if(!eng_set_detach(system_set, system)) return;

   Eng_Scene_System *module = (void*)_eng_scene_has_module(_eng_thread_this(), *system);

   Eina_Iterator *prereqs;  void *prereq;
   prereqs = eina_hash_iterator_key_new(module->node->system_prereqs());
   while(eina_iterator_next(prereqs, &prereq)) _system_sorter(system_set, prereq, order_inverse);
   eina_iterator_free(prereqs);

   eina_inarray_push(order_inverse, system);
}

static Eina_Inarray *
_system_sort(Set *system_set)
{
   Eina_Inarray *order         = eina_inarray_new(sizeof(uint64_t), 0);
   Eina_Inarray *order_inverse = eina_inarray_new(sizeof(uint64_t), 0);

   Digest *system;
   ENG_SET_FOREACH(system_set, system) eina_inarray_push(order, system);

   EINA_INARRAY_FOREACH(order, system) _system_sorter(system_set, system, order_inverse);
   eina_inarray_flush(order);

   // Read our order_inverse inarray into our order inarray from back to front.
   EINA_INARRAY_FOREACH(order_inverse, system) eina_inarray_push(order, system);

   eng_set_delete(system_set);
   eina_inarray_free(order_inverse);
   if(!eina_inarray_count(order)) { eina_inarray_free(order); return NULL; }
   else return order;
}

static Set *
_system_load_prereqs(Thread _scene, Digest system, Digest dependent_system)
{
   static Set *system_set, *parent_set;
   static Indx depth = VOID;

   Eng_Scene_System *module;

   if(!depth++)
   {
      system_set = eng_set_create(64, 1, VOID);
      parent_set = eng_set_create(64, 1, VOID);
   }

   // If our target prerequisite System is already an element in the parent_set, we have a cycle.
   if(!eng_set_attach(parent_set, &system)) goto FAILED;

   // If the target prerequisite is in the system_set, then it is already loaded.
   if(eng_set_has_element(system_set, &system))
   {
      module = (Eng_Scene_System*)_eng_scene_has_module(_eng_thread_this(), system);
      eina_hash_set(module->dependent_systems, &dependent_system, module); goto RETURN;
   }

   // Attempt to load the underlying Node_System.
   Eng_Node_System *node = _eng_node_system_load(system, _scene);
   if(!node) goto FAILED;

   // Allocate and initialize a new Eng_Scene_System entry.
   module                    = malloc(sizeof(Eng_Scene_System));
   module->node              = node;
   module->dependent_systems = eina_hash_int64_new(eng_empty_cb);
   module->dependent_shards  = eng_set_create(64, 1, VOID);

   module->running = EINA_TRUE;

   // Add this System_Modules parent to it's dependent_systems, then register it with this Scene.
   //eng_set_attach(module->dependent_systems, &dependent_system);
   eina_hash_set(module->dependent_systems, &dependent_system, module);
   eina_hash_set(scene->modules, &system, module);

   // If any of our prerequisite load attemps returns false, abort. Otherwise, we need to add the
   //    current System Globals to the system_set.
   Eina_Iterator *prereqs;  Digest **prereq;
   prereqs = eina_hash_iterator_key_new(module->node->system_prereqs());
   EINA_ITERATOR_FOREACH(prereqs, prereq)
      if(!_system_load_prereqs(_eng_thread_this(), **prereq, system))
         { eina_iterator_free(prereqs); goto ABORTS; }
   eina_iterator_free(prereqs);


   // Load all prerequisite Components and/or add this System to thier dependent_systems.
   //    This is a simple affair, since Components themselves never have any prerequisites.
   prereqs = eina_hash_iterator_key_new(module->node->component_prereqs());
   EINA_ITERATOR_FOREACH(prereqs, prereq)
      if(!_component_load(_eng_thread_this(), **prereq, system))
         { eina_iterator_free(prereqs); goto ABORTS; }
   eina_iterator_free(prereqs);

   // Remove this System from the parent_set.
   eng_set_detach(parent_set, &system);

   RETURN : if(!depth--) { eng_set_delete(parent_set); }
            return system_set;

   ABORTS : _eng_scene_system_free(_eng_thread_this(), system, dependent_system);

   FAILED : if(!depth--) { eng_set_delete(parent_set); eng_set_delete(system_set); }
            return NULL;
}

static void
_eng_scene_system_load__work(void *args, void *result)
{
   struct System_Info *info = args;

   Digest       *system;
   Set          *system_set; // This tracks which parts of the graph have already been visited.
   Eina_Inarray *system_order;

   // If the System is already present in the Scene, return the number of Shards with this system.
   Eng_Scene_System *module = (void*)_eng_scene_has_module(_eng_thread_this(), info->system);
   if(module) goto RETURN;

   // In order to find the new build order, we have to find the union of the set of the old
   //    system_order and the new System module plus its dependencies.  This will return NULL
   //    if the associated System Globals cannot be loaded.
   system_set = _system_load_prereqs(_eng_thread_this(), info->system, info->dependent_system);
   if(!system_set) return;

   // Find the union between the old system_order and the new system_set.
   EINA_INARRAY_FOREACH(scene->system_order, system) eng_set_attach(system_set, system);

   // Figure out what the new system_order is with the new System loaded.
   system_order = _system_sort(system_set);
   if(!system_order) return;

   eina_inarray_free(scene->system_order);
   scene->system_order = system_order;

   RETURN : *(Bool*)result = EINA_TRUE;
}

ENG_PRIVATE Bool
_eng_scene_system_load(Thread scene, Digest system, Digest dependent_system)
{
   struct System_Info *call = eng_work_call(scene, _eng_scene_system_load__work,
                                            sizeof(struct System_Info), sizeof(Bool));
   call->system           = system;
   call->dependent_system = dependent_system;

   return *(Bool*)eng_work_wait(call);
}

static void
_eng_scene_system_free__work(void *args, void *result)
{
   struct System_Info *info = args;

   Eng_Scene_System *module = (void*)_eng_scene_has_module(_eng_thread_this(), info->system);
   if(!module) return;

   // Short-Circuit if we still have other dependent_systems.
   eina_hash_del_by_key(module->dependent_systems, &info->dependent_system);
   if(eina_hash_population(module->dependent_systems)) goto RETURN;

   Eina_Iterator *prereqs;  Digest **prereq;
   prereqs = eina_hash_iterator_key_new(module->node->component_prereqs());
   EINA_ITERATOR_FOREACH(prereqs, prereq)
      _component_free(_eng_thread_this(), **prereq, info->dependent_system);
   eina_iterator_free(prereqs);

   prereqs = eina_hash_iterator_key_new(module->node->system_prereqs());
   EINA_ITERATOR_FOREACH(prereqs, prereq)
      _eng_scene_system_free(_eng_thread_this(), **prereq, info->dependent_system);
   eina_iterator_free(prereqs);

   eina_hash_del_by_key(scene->modules, &info->system);  free(module);
   _eng_node_system_free(info->system, _eng_thread_this());

   RETURN : *(Bool*)result = EINA_TRUE;
}

ENG_PRIVATE Bool
_eng_scene_system_free(Thread scene, Digest system, Digest dependent_system)
{
   struct System_Info *call = eng_work_call(scene, _eng_scene_system_free__work,
                                            sizeof(struct System_Info), sizeof(Bool));
   call->system           = system;
   call->dependent_system = dependent_system;

   return *(Bool*)eng_work_wait(call);
}


/*** Domain Handling API ***/

static void
_eng_scene_domain_load__work(void *args, void *result)
{
   // Get a list of Domain files from ../libraries
   Eina_List *domain_files = _eng_node_file_list(*(String**)args);
   String    *file;

   EINA_LIST_FREE(domain_files, file)
      if(strstr(file, ".System.so"))
         _eng_scene_system_load(_eng_thread_this(), _eng_node_file_digest(file), VOID);

   *(Bool*)result = EINA_TRUE;
}

ENG_PRIVATE Bool
_eng_scene_domain_load(Thread scene, String *domain)
{
   String **call = eng_work_call(scene,           _eng_scene_domain_load__work,
                                 sizeof(String*), sizeof(Bool));
   *call = domain;

   return *(Bool*)eng_work_wait(call);
}

static void
_eng_scene_domain_free__work(void *args, void *result)
{
   // Get a list of System files from ../libraries
   Eina_List *domain_files = _eng_node_file_list(*(String**)args);
   String    *file;

   EINA_LIST_FREE(domain_files, file)
      if(strstr(file, ".System.so"))
         _eng_scene_system_free(_eng_thread_this(), _eng_node_file_digest(file), VOID);

   *(Bool*)result = EINA_TRUE;
}

ENG_PRIVATE Bool
_eng_scene_domain_free(Thread scene, String *domain)
{
   String **call = eng_work_call(scene,           _eng_scene_domain_free__work,
                                 sizeof(String*), sizeof(Bool));
   *call = domain;

   return *(Bool*)eng_work_wait(call);
}

