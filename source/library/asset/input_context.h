#ifndef _ENG_INPUT_CONTEXT_H_PRIVATE_
#define _ENG_INPUT_CONTEXT_H_PRIVATE_

#include "engineer.h"

typedef struct
{
   void *symbol;
   void *payload;
}
Eng_Node_Input;

typedef struct
{
   Entity     entity;
   Eo        *origin;
   Eina_Hash *context;

   String              *efl_key_name;
   void               (*efl_cb_func)(void *data, Evas *e, Evas_Object *obj, void *event_info);
   Evas_Callback_Type   efl_cb_symbol;
}
Eng_Node_Input_Callback;

ENG_API void eng_node_input_context_origin_update_cb(void *data);

ENG_API Eina_Bool eng_node_input_context_entity_notify_from_efl(const Eina_Hash *hash,
        const void *key, void *data, void *fdata);

#endif

