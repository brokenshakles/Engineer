#ifndef _ENG_ASSET_MODEL_H_
#define _ENG_ASSET_MODEL_H_

#include <assimp/cimport.h>        // Plain-C interface
#include <assimp/scene.h>          // Output data structure
#include <assimp/postprocess.h>    // Post processing flags

typedef struct
{
   Mtrx orientation; // Relative to parent.

   Eina_Inarray *vertices;
   Eina_Inarray *elements;
   Eina_Inarray *textures;

   Eina_Inarray *children;

   //const void *material;
   //const void *normals;
   //const void *faces;
}
Eng_Mesh;

typedef struct
{
   Eina_Inarray *vertices;
   Eina_Inarray *faces;
   Eina_Inarray *normals;

   const void *material;
   const void *textures;
}
Eng_Mesh;

#endif

