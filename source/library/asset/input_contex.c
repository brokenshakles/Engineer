#include "input_context.h"

/*** Input Context Internal API ***/

typedef struct
{
   Eina_Hash *contexts;
   String    *name;
   String    *input;
   String    *notice;
}
Eng_Node_Input_Assignment;

EOLIAN static Eina_Hash *
_eng_node_input_contexts_get(Eo *obj EINA_UNUSED, Eng_Node_Data *pd)
{
   return pd->input_contexts;
}

EOLIAN static void
_eng_node_input_context_new(Eo *obj EINA_UNUSED, Eng_Node_Data *pd,
        String *context_name)
{
   Eina_Hash *context;

   if(!eina_hash_find(pd->input_contexts, context_name))
   {
      context = eina_hash_string_superfast_new(NULL);

      eina_hash_add(pd->input_contexts, context_name, context);
   }
}

/*  // This func will be looked at once we have a working Input_Context subsystem to test with.
EOLIAN static void
_eng_node_input_context_load(Eo *obj, Eng_Node_Data *pd,
        String *filename)
{
   Eng_System_Class *class;
   Eo               *module;
   System_State     *system;
   Eina_Hash        *context;

   if((class  = eng_node_system_class_query_loaded(eng_node(), "Input.Context.System"))
   && (module = eng_scene_system_module_query_loaded(scene, class)))
   {
      system  = (System_State*)class->system_state(module);
      context = eina_hash_string_superfast_new(NULL);

      // If filename is NULL, load a default context.

      eina_hash_add((Eina_Hash*)system->context_list, filename, context);
   }

}
*/

static void
_eng_node_input_context_assign_cb(void *data)
{
   Eng_Node_Input_Assignment *request = data;
   Eina_Hash                 *context;

   if( (context = eina_hash_find(request->contexts, request->name)) )
      eina_hash_add(context, request->input, request->notice);

   free(request);
}

EOLIAN static void
_eng_node_input_context_assign(Eo *obj EINA_UNUSED, Eng_Node_Data *pd,
        String *context_name, String *input, String *notice)
{
   Eng_Node_Input_Assignment *request = malloc(sizeof(Eng_Node_Input_Assignment));

   request->contexts = pd->input_contexts;
   request->name     = context_name;
   request->input    = input;
   request->notice   = notice;

   ecore_main_loop_thread_safe_call_async(_eng_node_input_context_assign_cb, request);
}

EAPI void
eng_node_input_context_origin_update_cb(void *data)
{
   Eng_Node_Input_Callback *callback = data;
   Eina_Hash               *targets;

   targets = efl_key_data_get(callback->origin, callback->efl_key_name);

   if(callback->origin)
   {
      if(!targets)
      {
         targets = eina_hash_int64_new(NULL);

         efl_key_data_set(callback->origin, callback->efl_key_name, targets);

         evas_object_event_callback_add(callback->origin,
                                        callback->efl_cb_symbol,
                                        callback->efl_cb_func,
                                        targets);
      }

      eina_hash_add(targets, &callback->entity, callback->context);
   }
   else
   {
      if(targets)
      {
         eina_hash_del_by_key(targets, &callback->entity);

         if(!eina_hash_population(callback->context))
         {
            evas_object_event_callback_del(callback->origin,
                                           callback->efl_cb_symbol,
                                           callback->efl_cb_func);

            efl_key_data_set(callback->origin, callback->efl_key_name, NULL);

            eina_hash_free(targets);
         }
      }
   }

   free(callback);
}

// This is a eina_foreach callback, it iterates over a Eina_Hash table, executing on each element.
EAPI Eina_Bool
eng_node_input_context_entity_notify_from_efl(const Eina_Hash *hash EINA_UNUSED,
        const void *key, void *data, void *fdata)
{
   Entity          entity  = *(EntityID*)key;
   Eina_Hash      *context = data;
   Eng_Node_Input *input   = fdata;

   Eng_Scene *scene   = eng_node_entity_scene_get(eng_node(), entity);
   String    *notice  = eina_hash_find(context, input->symbol);

   if(scene && notice) eng_scene_entity_notify(scene, entity, notice, input->payload);
   else return EINA_FALSE;

   return EINA_TRUE;
}

