#include "model.h"

/*
void
_process_node(const aiNode *node)
{
   for(int child = 0; child < node->mNumChildren; child++)
      _process_node(node->mChildren[child]);
}
*/

void
eng_asset_model_load_process_node(aiNode *node)
{


   eng_asset_model_load_process_node(node);
}

const aiScene *
eng_asset_model_load(String *file)
{
   // Start the import on the given file with some example postprocessing
   // Usually - if speed is not the most important aspect for you - you'll t
   // probably to request more postprocessing than we do in this example.
   const aiScene *scene = aiImportFile(file,
                             aiProcess_CalcTangentSpace      |
                             aiProcess_Triangulate           |
                             aiProcess_JoinIdenticalVertices |
                             aiProcess_SortByPType);

   // If the import failed, report it
   if(!scene)
   {
      //DoTheErrorLogging(aiGetErrorString());
      return NULL;
   }

   // Now we can access the file's contents
   //_process_node(scene->mRootNode);

  return scene;
}

void
eng_asset_model_unload(aiScene *scene)
{
   // We're done. Release all resources associated with this import
   aiReleaseImport(scene);
}

