
static Eina_Bool
_eng_console_class_initializer(Efl_Class *klass)
{
   const Efl_Object_Ops *opsp = NULL;

   const Efl_Object_Property_Reflection_Ops *ropsp = NULL;

#ifdef ENG_CONSOLE_EXTRA_OPS
   EFL_OPS_DEFINE(ops, ENG_CONSOLE_EXTRA_OPS);
   opsp = &ops;
#endif

   return efl_class_functions_set(klass, opsp, ropsp);
}

static const Efl_Class_Description _eng_console_class_desc = {
   EO_VERSION,
   "Eng.Console",
   EFL_CLASS_TYPE_REGULAR,
   sizeof(Eng_Console_Data),
   _eng_console_class_initializer,
   NULL,
   NULL
};

EFL_DEFINE_CLASS(eng_console_class_get, &_eng_console_class_desc, EFL_OBJECT_CLASS, NULL);
