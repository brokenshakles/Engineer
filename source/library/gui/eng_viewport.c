#include "gui/eng_viewport.h"

struct Eng_Viewport_Data
{
   Entity camera;  // The camera that the Viewport is looking through.

   void (*init)(Evas_Object *obj);
   void (*free)(Evas_Object *obj);
   void (*render)(Evas_Object *obj);
   void (*resize)(Evas_Object *obj);
};

static void
_eng_viewport_render_error(Eo *obj)
{
   Evas_Object *label = elm_label_add(obj);

   elm_box_pack_end(obj, label);

   elm_object_text_set(label,
      "<align=left> Render backend engine is not supported.<br/>"
      " 1. Check your back-end engine or<br/>"
      " 2. Run elementary_test with engine option or<br/>"
      "    ex) $ <b>ELM_ACCEL=gl</b> elementary_test<br/>"
      " 3. Change your back-end engine from elementary_config.<br/></align>");

   evas_object_size_hint_align_set(label, EVAS_HINT_FILL, EVAS_HINT_FILL);
   evas_object_size_hint_weight_set(label, 0.0, 0.0);

   evas_object_show(label);
}

static Eina_Bool
_eng_viewport_gles_init(Eo *obj, Eng_Viewport_Data *pd)
{
   Elm_Glview *glview = elm_glview_version_add(obj, EVAS_GL_GLES_3_X);

   if(glview)
   {
      elm_glview_mode_set(glview, 0
         | ELM_GLVIEW_ALPHA
         | ELM_GLVIEW_DEPTH
         | ELM_GLVIEW_DIRECT
      );

      elm_glview_init_func_set(glview,   pd->init);
      elm_glview_del_func_set(glview,    pd->free);
      elm_glview_render_func_set(glview, pd->render);
      elm_glview_resize_func_set(glview, pd->resize);

      elm_glview_render_policy_set(glview, ELM_GLVIEW_RENDER_POLICY_ON_DEMAND);
      elm_glview_resize_policy_set(glview, ELM_GLVIEW_RESIZE_POLICY_RECREATE);

      evas_object_size_hint_align_set(glview, EVAS_HINT_FILL, EVAS_HINT_FILL);
      evas_object_size_hint_weight_set(glview, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);

      elm_box_pack_end(obj, glview);
      evas_object_show(glview);

      return EINA_TRUE;
   }
   else
   {
      _eng_viewport_render_error(obj);

      return EINA_FALSE;
   }
}

static void
_eng_viewport_gles_free(Eo *obj EINA_UNUSED, Eng_Viewport_Data *pd EINA_UNUSED)
{
}

EOLIAN static Entity
_eng_viewport_look(Eo *obj, Eng_Viewport_Data *pd, Entity new_camera)
{
   Entity old_camera = pd->camera;
          pd->camera = new_camera;

   if(new_camera) eng_entity_notify(new_camera, "viewport_attach", &obj); // The (eng_entity_notify) may break after compile.
   if(old_camera) eng_entity_notify(old_camera, "viewport_detach", &obj);

   printf("Viewport Look Checkpoint. Viewport Pointer: %p\n", obj);

   return old_camera;
}

EOLIAN static Entity
_eng_viewport_camera_get(const Eo *obj EINA_UNUSED, Eng_Viewport_Data *pd)
{
   return pd->camera;
}

/*** Constructor/Destructor ***/

EOLIAN static Efl_Object *
_eng_viewport_efl_object_constructor(Eo *obj, Eng_Viewport_Data *pd EINA_UNUSED)
{
   return efl_constructor(efl_super(obj, ENG_VIEWPORT_CLASS));
}

EOLIAN static Efl_Object *
_eng_viewport_efl_object_finalize(Eo *obj, Eng_Viewport_Data *pd)
{
   obj = efl_finalize(efl_super(obj, ENG_VIEWPORT_CLASS));

   pd->camera = VOID;

   return obj;
}

EOLIAN static void
_eng_viewport_efl_object_destructor(Eo *obj EINA_UNUSED, Eng_Viewport_Data *pd EINA_UNUSED)
{
   efl_destructor(efl_super(obj, ENG_VIEWPORT_CLASS));
}

#include "eng_viewport.eo.c"
