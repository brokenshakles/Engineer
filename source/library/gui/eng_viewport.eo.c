
Efl_Object *_eng_viewport_efl_object_constructor(Eo *obj, Eng_Viewport_Data *pd);


Efl_Object *_eng_viewport_efl_object_finalize(Eo *obj, Eng_Viewport_Data *pd);


void _eng_viewport_efl_object_destructor(Eo *obj, Eng_Viewport_Data *pd);


Entity _eng_viewport_camera_get(const Eo *obj, Eng_Viewport_Data *pd);

EAPI EAPI_WEAK EFL_FUNC_BODY_CONST(eng_viewport_camera_get, Entity, 0);

Entity _eng_viewport_look(Eo *obj, Eng_Viewport_Data *pd, Entity new_camera);

EAPI EAPI_WEAK EFL_FUNC_BODYV(eng_viewport_look, Entity, 0, EFL_FUNC_CALL(new_camera), Entity new_camera);

static Eina_Bool
_eng_viewport_class_initializer(Efl_Class *klass)
{
   const Efl_Object_Ops *opsp = NULL;

   const Efl_Object_Property_Reflection_Ops *ropsp = NULL;

#ifndef ENG_VIEWPORT_EXTRA_OPS
#define ENG_VIEWPORT_EXTRA_OPS
#endif

   EFL_OPS_DEFINE(ops,
      EFL_OBJECT_OP_FUNC(efl_constructor, _eng_viewport_efl_object_constructor),
      EFL_OBJECT_OP_FUNC(efl_finalize, _eng_viewport_efl_object_finalize),
      EFL_OBJECT_OP_FUNC(efl_destructor, _eng_viewport_efl_object_destructor),
      EFL_OBJECT_OP_FUNC(eng_viewport_camera_get, _eng_viewport_camera_get),
      EFL_OBJECT_OP_FUNC(eng_viewport_look, _eng_viewport_look),
      ENG_VIEWPORT_EXTRA_OPS
   );
   opsp = &ops;

   return efl_class_functions_set(klass, opsp, ropsp);
}

static const Efl_Class_Description _eng_viewport_class_desc = {
   EO_VERSION,
   "Eng.Viewport",
   EFL_CLASS_TYPE_REGULAR,
   sizeof(Eng_Viewport_Data),
   _eng_viewport_class_initializer,
   NULL,
   NULL
};

EFL_DEFINE_CLASS(eng_viewport_class_get, &_eng_viewport_class_desc, EFL_UI_BOX_CLASS, NULL);
