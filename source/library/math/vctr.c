#include "math/vctr.h"

extern inline Vctr vctr(double input);

extern inline Vctr vctr_mult(Vctr multiplicand, Vctr multiplier);
extern inline Vctr vctr_divd(Vctr dividend,     Vctr divisor);

extern inline Sclr vctr_abs(Vctr input);
extern inline Vctr vctr_clamp(Vctr input, Vctr min, Vctr max);

