#include "math/aabb.h"

extern inline AABB * aabb(double x, double y, double z);

extern inline Sclr aabb_perimeter(AABB input);
extern inline AABB aabb_union(AABB a, AABB b);

extern inline Bool aabb_intersect(AABB a, AABB b);
extern inline Bool aabb_intersect_sphere(AABB a, Sphere b);
extern inline Bool aabb_subsect(AABB a, AABB b);

