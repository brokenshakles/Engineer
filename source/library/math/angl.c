#include "math/angl.h"

extern inline Angl angl(double input);

extern inline Angl angl_mult(Angl multiplicand, Angl multiplier);
extern inline Angl angl_divd(Angl dividend,     Angl divisor);

extern inline Sclr angl_sin(Angl input);
extern inline Sclr angl_cos(Angl input);
extern inline Sclr angl_tan(Angl input);
extern inline Angl angl_asin(Vctr input);
//extern inline Angl algn_acos(Vctr input);
extern inline Angl angl_atan(Vctr input);

extern inline Sclr angl_sinh(Angl input);
extern inline Sclr angl_cosh(Angl input);
extern inline Sclr angl_tanh(Angl input);
//extern inline Sclr angl_asinh(Angl input);
//extern inline Sclr angl_acosh(Angl input);
extern inline Sclr angl_atanh(Angl input);

