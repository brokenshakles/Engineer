#include "math/mtrx.h"

extern inline Mtrx mtrx_identity();
extern inline Mtrx mtrx_transpose(Mtrx input);

extern inline Mtrx mtrx_mult(int count, ...);
extern inline Quat mtrx_mult_by_quat(Mtrx multiplicand, Quat multiplier);

extern inline Mtrx mtrx_from_position(Vec3 input);  // Produces a translation matrix.
extern inline Mtrx mtrx_from_scale(Vec3 input);     // Produces a scaling(resizing) matrix.
extern inline Mtrx mtrx_from_quat(Quat input);      // Produces a rotation matrix.

