#include "math/vec2.h"

extern inline Vec2 vec2(double x, double y);

extern inline Vec2 vec2_mult(Vec2 input_a, Vec2 input_b);
extern inline Vec2 vec2_divd(Vec2 input_a, Vec2 input_b);

extern inline Sclr vec2_length(Vec2 input);
extern inline Vec2 vec2_scale(Vec2 input, Sclr factor);

extern inline Sclr vec2_dot(Vec2 input_a, Vec2 input_b);

extern inline Vec2 vec2_normalize(Vec2 input);
extern inline Vec2 vec2_invert(Vec2 input);

