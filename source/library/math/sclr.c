#include "math/sclr.h"

extern inline Sclr sclr(double input);

extern inline Sclr sclr_mult(Sclr multiplicand, Sclr multiplier);
extern inline Sclr sclr_divd(Sclr dividend,     Sclr divisor);

extern inline Sclr sclr_clamp(Sclr input, Sclr min, Sclr max);

extern inline Sclr sclr_exp(Sclr input);
extern inline Sclr sclr_ln(Sclr input);
extern inline Sclr sclr_sqrt(Sclr input);

