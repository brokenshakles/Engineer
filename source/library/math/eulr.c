#include "math/eulr.h"

extern inline Eulr eulr(double yaw, double pitch, double roll);

extern inline Quat eulr_to_quat(Eulr input);

