#include "math/quat.h"

extern inline Quat quat(double x, double y, double z, double a);
extern inline Quat quat_pure(Vec3 input);
extern inline Quat quat_multiply(Quat multiplier, Quat multiplicand);
extern inline Quat quat_invert(Quat input);
extern inline Quat quat_normalize(Quat input);
extern inline Quat quat_from_basis(Vec3 v);
extern inline Quat quat_from_orientations(Vec3 u, Vec3 v);

