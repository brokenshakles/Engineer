#include "math/vec3.h"

extern inline Vec3 vec3(double x, double y, double z);

extern inline Vec3 vec3_mult(Vec3 input_a, Vec3 input_b);
extern inline Vec3 vec3_divd(Vec3 input_a, Vec3 input_b);

extern inline Sclr vec3_length(Vec3 input);
extern inline Vec3 vec3_scale(Vec3 input, Sclr factor);

extern inline Sclr vec3_dot(Vec3 inputa, Vec3 inputb);
extern inline Vec3 vec3_cross(Vec3 inputa, Vec3 inputb);

extern inline Vec3 vec3_normalize(Vec3 input);
extern inline Vec3 vec3_invert(Vec3 input);

