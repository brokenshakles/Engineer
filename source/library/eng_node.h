#ifndef _ENG_NODE_H_PRIVATE_
#define _ENG_NODE_H_PRIVATE_

#include "eng_thread.h"

typedef struct Eng_Node_Module    Eng_Node_Module;
typedef struct Eng_Node_Component Eng_Node_Component;
typedef struct Eng_Node_System    Eng_Node_System;

// This is a redundant "overlay" struct used for accessing Component and System Scene_Module
//    structs with the same language.
struct Eng_Node_Module
{
   #define ENG_NODE_MODULE_HEADER(module_flavor)                                            \
      Digest       digest;  /* In the form of eng_digest("domain.module.flavor");        */ \
      Eina_Module *eina;    /* This is used in un/loading our class from a block device. */ \
      Set         *dependent_scenes;                                                        \
                                                                                            \
      String* (*get_symbol)();                                                              \
      String* (*get_domain)();                                                              \
      String* (*get_module)();                                                              \
      String* (*get_flavor)();                                                              \
                                                                                            \
      Indx (*node_module_init)(Digest, Indx (*)(Digest, Digest));                           \
      void (*node_module_free)();                                                           \
                                                                                            \
      Indx (*scene_module_init)(void*);                                                     \
      void (*scene_module_free)(void*);                                                     \
                                                                                            \
      Eng_Vault* (*shard_module_init)();                                                    \
      void       (*shard_module_free)(Eng_Vault*);                                          \

   ENG_NODE_MODULE_HEADER(Eng_Node_Module);
};

struct Eng_Node_Component
{
   ENG_NODE_MODULE_HEADER(Eng_Node_Component);

   Indx (*component_sizeof)();
   Indx (*component_column)(Digest);

   void (*entity_status)(Eng_Vault*, Entity, void*);
   Indx (*entity_attach)(Eng_Vault*, Entity, void*);
   void (*entity_detach)(Eng_Vault*, Entity, void*);
};

struct Eng_Node_System
{
   ENG_NODE_MODULE_HEADER(Eng_Node_System);

   Indx (*system_sizeof)();
   Indx (*entity_sizeof)();

   Eina_Hash* (*system_prereqs)();
   Eina_Hash* (*component_prereqs)();
   Eina_Hash* (*component_ignored)();

   Bool (*entity_is_targeted)(Byte*); // Note : Changed from String to Byte.
   Bool (*entity_is_ignored )(Byte*);

   void (*entity_attach)(Entity, Entity_State*, Entity*);
   void (*entity_detach)(Entity, Entity_State*, Entity*);

   void (*entity_import)(Indx, Entity*, Entity_State*, Eng_Vault* (*)(Digest));
   void (*entity_export)(Indx, Entity*, Entity_State*, Eng_Vault* (*)(Digest));

   void (*entity_upkeep)(Indx, Entity*, Entity_State*, Entity*);
   void (*entity_notify)(Indx, Entity*, Entity_State*, Entity*);
   void (*entity_update)(Indx, Entity*, Entity_State*, Entity*);
};


/*** Node Internal API ***/

Thread _eng_node_boot();
void   _eng_node_halt();

Eina_List * _eng_node_file_list(String *target);
Digest      _eng_node_file_digest(String *file);

Eng_Node_Module * _eng_node_has_module(Digest module);
Bool              _eng_node_module_has_scene(Eng_Node_Module *module, Entity scene);

Eng_Node_Component * _eng_node_component_load(Digest component, Entity scene);
Bool                 _eng_node_component_free(Digest component, Entity scene);

Eng_Node_System * _eng_node_system_load(Digest system, Entity scene);
Bool              _eng_node_system_free(Digest system, Entity scene);

#endif

