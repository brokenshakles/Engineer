#ifndef _INPUT_MOUSE_SYSTEM_H_
#define _INPUT_MOUSE_SYSTEM_H_

#include <eng_system.h>

ESML(
   SYMBOL(Input, Mouse)
   PREREQ()
   GLOBAL(
      STATE(
         FIELD(Vec2, delta)
      )
      EVENTS(
         EVENT(input_flush,     0)
         EVENT(mouse_listen,    8)
         EVENT(mouse_ignore,    8)
   )  )
   ENTITY(
      IMPORT(
         ASSET(Input,
            COMPONENT(Mouse,
               FIELD(Sclr, context)
      )  )  )
      EXPORT()
      EVENTS()
)  )

#endif

