#include "System.h"

void
entity_start(Entity_State *entity)
{
}

void
entity_upkeep(Entity_State *entity)
{
}

void
entity_update(Entity_State *entity)
{
   printf("Entity Update Checkpoint. Entity ID: %ld\n", entity->id);

   if(system->w & KEY_DOWN) eng_entity_notify(this, "move_forward",  NULL);
   if(system->a & KEY_DOWN) eng_entity_notify(this, "move_left",     NULL);
   if(system->s & KEY_DOWN) eng_entity_notify(this, "move_rearward", NULL);
   if(system->d & KEY_DOWN) eng_entity_notify(this, "move_right",    NULL);
   if(system->q & KEY_DOWN) eng_entity_notify(this, "roll_left",     NULL);
   if(system->e & KEY_DOWN) eng_entity_notify(this, "roll_right",    NULL);

   eng_entity_notify(this, "input_flush", NULL);
}

void
entity_stop(Entity_State *entity)
{
}

static void
_key_down_eo_cb(void *data, Evas *e EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event_info)
{
   Evas_Event_Key_Down *event  = event_info;
   System_State        *system = data;

   if(!strncmp(event->keyname, "a", 16)) system->a |= KEY_DOWN;
   if(!strncmp(event->keyname, "d", 16)) system->d |= KEY_DOWN;
   if(!strncmp(event->keyname, "e", 16)) system->e |= KEY_DOWN;
   if(!strncmp(event->keyname, "q", 16)) system->q |= KEY_DOWN;
   if(!strncmp(event->keyname, "s", 16)) system->s |= KEY_DOWN;
   if(!strncmp(event->keyname, "w", 16)) system->w |= KEY_DOWN;
}

void
entity_event(input_flush, System_State *system, Data *data)
{
   system->a = KEY_IDLE;
   system->d = KEY_IDLE;
   system->e = KEY_IDLE;
   system->q = KEY_IDLE;
   system->s = KEY_IDLE;
   system->w = KEY_IDLE;
}

void
entity_event(keyboard_listen, System_State *system, Data *data)
{
   Eo *target = *(Eo**)data;

   evas_object_event_callback_add(target, EVAS_CALLBACK_KEY_DOWN, _key_down_eo_cb, system);
}

void
entity_event(keyboard_ignore, System_State *system, Data *data)
{
   Eo *target = *(Eo**)data;

   evas_object_event_callback_del_full(target, EVAS_CALLBACK_KEY_DOWN, _key_down_eo_cb, system);
}

