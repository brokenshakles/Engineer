#ifndef _INPUT_KEYBOARD_SYSTEM_H_
#define _INPUT_KEYBOARD_SYSTEM_H_

#include <eng_system.h>

ESML(
   SYMBOL(Input, Keyboard)
   PREREQ()
   GLOBAL(
      STATE(
         FIELD(Byte, w)
         FIELD(Byte, a)
         FIELD(Byte, s)
         FIELD(Byte, d)
         FIELD(Byte, q)
         FIELD(Byte, e)
      )
      EVENTS(
         EVENT(input_flush,     0)
         EVENT(keyboard_listen, 8)
         EVENT(keyboard_ignore, 8)
   )  )
   ENTITY(
      IMPORT(
         ASSET(Input,
            COMPONENT(Keyboard,
               FIELD(Sclr, context)
      )  )  )
      EXPORT()
      EVENTS()
   )
)

enum Key_State
{
   KEY_IDLE     = 0,
   KEY_DOWN     = 1,
   KEY_HELD     = 2,
   KEY_RELEASED = 4,
};

#endif

