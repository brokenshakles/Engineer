#include "System.h"

void
entity_start(Entity_State *entity)
{
}

void
entity_upkeep(Entity_State *entity)
{
}

void
entity_update(Entity_State *entity)
{
   printf("Entity Update Checkpoint. Entity ID: %ld\n", entity->id);

   if(entity->delta.x > 0) entity_notify(this, "yaw_left",       &entity->delta);
   if(entity->delta.x < 0) entity_notify(this, "yaw_right",      &entity->delta);
   if(entity->delta.y > 0) entity_notify(this, "pitch_forward",  &entity->delta);
   if(entity->delta.y < 0) entity_notify(this, "pitch_rearward", &entity->delta);

   eng_entity_notify(this, "input_flush", NULL);
}

void
entity_stop(Entity_State *entity)
{
}

static void
_delta_set_eo_cb(void *data, Evas *e EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event_info)
{
   Evas_Event_Mouse_Move *event  = event_info;
   System_State          *system = data;

   system->delta.x += BASIS * (event->cur.output.x - event->prev.output.x);
   system->delta.y += BASIS * (event->cur.output.y - event->prev.output.y);
}

void
entity_event(input_flush, System_State *system, Data *data)
{
   system->delta.x = 0;
   system->delta.y = 0;
}

void
entity_event(mouse_listen, System_State *system, Data *data)
{
   Eo *target = *(Eo**)data;

   evas_object_event_callback_add(target, EVAS_CALLBACK_MOUSE_MOVE, _delta_set_eo_cb, system);
}

void
entity_event(mouse_ignore, System_State *system, Data *data)
{
   Eo *target = *(Eo**)data;

   evas_object_event_callback_del_full(target, EVAS_CALLBACK_MOUSE_MOVE, _delta_set_eo_cb, system);
}

