#include "System.h"

static void
entity_start(Entity_State *entity)
{
}

static void
entity_upkeep(Entity_State *entity)
{
}

static void
entity_update(Entity_State *entity)
{
}

static void
entity_stop(Entity_State *entity)
{
}

void
entity_event(entity_attach, Entity_State *entity, Data *data)
{
   Entity new_parent = *(Entity*)data;
   Entity target = entity->id;

   Entity *old_sibling_prev = NULL, *old_sibling_next = NULL, *old_parent;
   Entity *new_sibling_prev = NULL, *new_sibling_next = NULL, *new_first_child = NULL;
   Indx    touched[7] = VOID;

   LOCK : // This graph operation reads from and writes to multiple Entities.
   eina_rwlock_take_read(entity_mutex);

   // First make sure that our target exists.
   touched[0] = eng_vault_row(entity_vault, target);
   if(touched[0]) { eina_rwlock_release(entity_mutex); return; }

   // Then we figure out all possible candidates for touching.
   old_parent       = eng_vault_fetch(entity_vault, PARENT,       touched[0]);
   old_sibling_prev = eng_vault_fetch(entity_vault, SIBLING_PREV, touched[0]);
   old_sibling_next = eng_vault_fetch(entity_vault, SIBLING_NEXT, touched[0]);

   touched[1] = eng_vault_row(entity_vault, *old_parent);
   touched[2] = eng_vault_row(entity_vault, *old_sibling_prev);
   touched[3] = eng_vault_row(entity_vault, *old_sibling_next);

   if(new_parent)       { touched[4] = eng_vault_row(entity_vault, new_parent);
      new_first_child  = eng_vault_fetch(entity_vault, FIRST_CHILD,  touched[4]); }
   if(new_first_child)  { touched[5] = eng_vault_row(entity_vault, *new_first_child);
      new_sibling_prev = eng_vault_fetch(entity_vault, SIBLING_PREV, touched[5]); }
   if(new_sibling_prev) { touched[6] = eng_vault_row(entity_vault, *new_sibling_prev);
      new_sibling_next = eng_vault_fetch(entity_vault, SIBLING_NEXT, touched[6]); }

   // Here we lock everything we will touch.
   Indx mutex = 0;{ if(_lock(touched[0], ENTITY_GRAPH)) mutex = 1; else goto OPEN; }
   if(touched[1]) { if(_lock(touched[1], ENTITY_GRAPH)) mutex = 2; else goto OPEN; }
   if(touched[2] !=
      touched[3]) { if(_lock(touched[2], ENTITY_GRAPH)) mutex = 3; else goto OPEN;
                    if(_lock(touched[3], ENTITY_GRAPH)) mutex = 4; else goto OPEN; }
   if(touched[4]) { if(_lock(touched[4], ENTITY_GRAPH)) mutex = 5; else goto OPEN; }
   if(touched[5]) { if(_lock(touched[5], ENTITY_GRAPH)) mutex = 6; else goto OPEN; }
   if(touched[6] && touched[5] !=
      touched[6]) { if(_lock(touched[6], ENTITY_GRAPH)) mutex = 7; else goto OPEN; }
   else mutex = 7; // Okie-dokie!

   // Add entity to new parent.
   // Same for the new parent. Super robust!
   if(new_parent)
   {  // Check to see if new parent has any children. If not, set up the first child properly.
      if(!*new_first_child)
      {
         *new_first_child  = target;
         *old_sibling_prev = target;
         *old_sibling_next = target;
      }
      else // add the target Entity to the back end of the new parent's child list.
      {
         *old_sibling_prev = *new_sibling_prev;
         *old_sibling_next = *new_sibling_next;
         *new_sibling_prev =  target;
         *new_sibling_next =  target;
   }  }

   // Remove entity from old parent.
   // We only do this if there IS an old parent. Robust!
   if(*old_parent)
   {  // Check to see if the old parent has only one child. If so, unset the child reference.
      Entity *old_first_child = eng_vault_fetch(entity_vault, FIRST_CHILD, touched[1]);

      if(*old_sibling_prev == *old_sibling_next) *old_first_child = VOID;
      else
      {  // Check to see if the target is the old parents first child. If so, move the first child
         //    pointer to it's next child.
         if (*old_first_child == target) *old_first_child = *old_sibling_next;

         // Remove the current entity from the from it's current set of siblings by relinking it's
         //    adjacent siblings to one another.
         new_sibling_prev = eng_vault_fetch(entity_vault, SIBLING_PREV, touched[3]);
         new_sibling_next = eng_vault_fetch(entity_vault, SIBLING_NEXT, touched[2]);

        *new_sibling_prev = *old_sibling_prev;
        *new_sibling_next = *old_sibling_next;
   }  }

   *old_parent = new_parent;

   OPEN :  // Release all locked ENTITY_GRAPH Mutexes.
   switch(mutex)
   {
      case 7 :
      case 6 : if(touched[6] && touched[6] !=
                  touched[5])   _unlock(touched[6], ENTITY_GRAPH);
      case 5 : if(touched[5])   _unlock(touched[5], ENTITY_GRAPH);
      case 4 : if(touched[4])   _unlock(touched[4], ENTITY_GRAPH);
      case 3 :
      case 2 : if(touched[3] !=
                  touched[2]) { _unlock(touched[3], ENTITY_GRAPH);
                                _unlock(touched[2], ENTITY_GRAPH); }
      case 1 : if(touched[1])   _unlock(touched[1], ENTITY_GRAPH);
      case 0 :                  _unlock(touched[0], ENTITY_GRAPH);
   }
   eina_rwlock_release(entity_mutex);
   if(mutex < 7) goto LOCK;

   _eng_entity_migrate(target, parent); // We would want this to happen before the unlocking, but it would recursively lock and then bind.
}

// Takes the target Entity and attaches it to a new parent Entity.
// Scheduling: Next Frame.
ENG_API Bool
eng_entity_attach(Entity target, Entity new_parent)
{
   // If the specified parent is already set, we don't need to do anything.
   if(eng_entity_parent(target) == new_parent) return EINA_TRUE;

   // Since we want to wait until the upkeep of next frame to do this, send an event.
   eng_entity_notify(eng_entity_scene(target), "entity_attached", &new_parent);

   return EINA_TRUE;
}

ENG_API Entity
eng_entity_parent(Entity target)
{
   Entity parent;

   eina_rwlock_take_read(&entity_mutex);

   Indx row = eng_vault_row(entity_vault, target);

   while(!_lock(row, ENTITY_GRAPH)) continue;
   parent = *(Entity*)eng_vault_fetch(entity_vault, PARENT, row);
   _unlock(row, ENTITY_GRAPH);

   eina_rwlock_release(&entity_mutex);

   return parent;
}

static void
_entity_event__entity_attach__cord_old(void *args, Eng_Worker_Result *result EINA_UNUSED)
{
   Indx   count      = 2;
   Entity entity     = this;
   Entity new_parent = *(Entity*)args;

   Entity        batch[4];
   Entity        parents[2],             adjacent[4];
   Entity_State old_parent_relatives,   new_parent_relatives,
                entity_relatives,       new_first_child_relatives,
                sibling_prev_relatives, sibling_next_relatives;

  // Since this data rewrite interrelates multiple Entities, we need to block what we will touch.
   GET_ENTITY_BLOCKS :
      entity_relatives = eng_component_lookup(entity, "Scene.Graph");

      parents[0] = new_parent;
      parents[1] = entity_relatives.parent;

      memcpy(batch, parents, sizeof(Entity) * 2);
      if(!_eng_entityid_status_toggle_batch(parents, 2, ENTITY_BLOCK))
         { eng_worker_delay(); goto GET_ENTITY_BLOCKS; }

      old_parent_relatives = eng_component_lookup(entity_relatives.parent, "Scene.Graph");
      new_parent_relatives = eng_component_lookup(new_parent,              "Scene.Graph");

      adjacent[0] = entity_relatives.sibling_prev;
      adjacent[1] = entity_relatives.sibling_next;

      if(new_parent != NULL_ENTITY && new_parent_relatives.first_child != NULL_ENTITY)
      {
         new_first_child_relatives =
            eng_component_lookup(new_parent_relatives.first_child, "Scene.Graph");

         adjacent[2] = new_parent_relatives.first_child;
         adjacent[3] = new_first_child_relatives.sibling_prev;

         count += 2;
      }

      memcpy(batch, adjacent, sizeof(Entity) * 4);
      if(!_eng_entityid_status_toggle_batch(batch, count, ENTITY_BLOCK))
      {
         memcpy(batch, parents, sizeof(Entity) * 2);
         while(!_eng_entityid_status_toggle_batch(batch, 2, ENTITY_BLOCK)) eng_worker_delay();
         eng_worker_delay(); goto GET_ENTITY_BLOCKS;
      }

   // Subsection: Remove entity from old parent.
   // We only do this if there IS an old parent. Robust!
   if(entity_relatives.parent != NULL_ENTITY)
   {
      old_parent_relatives = eng_component_lookup(entity_relatives.parent, "Scene.Graph");

      // Check to see if the old parent has only one child. If so, unset the child reference.
      if(entity == entity_relatives.sibling_next)
         old_parent_relatives.first_child = NULL_ENTITY;
      else
      {  // relink the remaining child Entities.
         sibling_next_relatives = eng_component_lookup(entity_relatives.sibling_next, "Scene.Graph");
         sibling_prev_relatives = eng_component_lookup(entity_relatives.sibling_prev, "Scene.Graph");

         // Check to see if the target is the old parents firstborn. If so, move the firstborn
         //    pointer to it's next child.
         if (old_parent_relatives.first_child == entity)
             old_parent_relatives.first_child  = entity_relatives.sibling_next;

         // Remove the current entity from the from it's current set of siblings by relinking it's
         //    adjacent siblings to one another.
         sibling_next_relatives.sibling_prev = entity_relatives.sibling_prev;
         sibling_prev_relatives.sibling_next = entity_relatives.sibling_next;

         _eng_entity_relatives_set(entity_relatives.sibling_next, &sibling_next_relatives);
         _eng_entity_relatives_set(entity_relatives.sibling_prev, &sibling_prev_relatives);
      }

      _eng_entity_relatives_set(entity_relatives.parent, &old_parent_relatives);
   }

   // Subsection: Add entity to new parent.
   // Same for the new parent. Super robust!
   if(new_parent != NULL_ENTITY)
   {
      new_parent_relatives = eng_component_lookup(new_parent, "Scene.Graph");

      // Check to see if new parent has any children. If not, set up the first child properly.
      if(new_parent_relatives.first_child == NULL_ENTITY)
      {
         new_parent_relatives.first_child = entity;
         entity_relatives.sibling_next    = entity;
         entity_relatives.sibling_prev    = entity;
      }
      else // add the target Entity to the back end of the new parent's child list.
      {
         sibling_next_relatives = eng_component_lookup(new_parent_relatives.first_child,    "Scene.Graph");
         sibling_prev_relatives = eng_component_lookup(sibling_next_relatives.sibling_prev, "Scene.Graph");

         entity_relatives.sibling_prev = sibling_next_relatives.sibling_prev;
         entity_relatives.sibling_next = new_parent_relatives.first_child;

         sibling_next_relatives.sibling_prev = entity;
         sibling_prev_relatives.sibling_next = entity;

         _eng_entity_relatives_set(entity_relatives.sibling_next, &sibling_next_relatives);
         _eng_entity_relatives_set(entity_relatives.sibling_prev, &sibling_prev_relatives);
      }

      _eng_entity_relatives_set(new_parent, &new_parent_relatives);
   }

   // Set the new parent Entity for the target Entity.
   entity_relatives.parent = new_parent;
   _eng_entity_relatives_set(entity, &entity_relatives);

   memcpy(batch, adjacent, sizeof(Entity) * 4);
   while(!_eng_entityid_status_toggle_batch(batch, count, ENTITY_BLOCK)) eng_worker_delay();
   memcpy(batch, parents,  sizeof(Entity) * 2);
   while(!_eng_entityid_status_toggle_batch(batch,     2, ENTITY_BLOCK)) eng_worker_delay();
}

static Eina_Bool
entity_event(entity_attach, Entity_State *state, const void *notice)
{
   Entity *task = eng_worker_task(state->entity,  _entity_event__entity_attach__cord,
                                  sizeof(Entity), 0);

   *task = *(Entity*)notice;

   eng_worker_pass(task); return EINA_TRUE;
}

   //Entity *first_child, *this_child;

   // We mark things dead on the way down. If we come across an entity that is already marked dead,
   //    we return without recursing since another thread will finish the process.
   //if(!_unlock(target, ENTITY_ALIVE)) return;

   // Do a depth first traversal of all children, recursively destroying them.
   //first_child = eng_vault_fetch(entity_vault, FIRST_CHILD, ROW(target));
   //this_child  = first_child;
   //if(*first_child)
   //{  do
   //   {
   //      _eng_entity_delete(this_child);
   //      this_child = eng_vault_fetch(entity_vault, SIBLING_NEXT, ROW(*this_child));
   //   }
   //   while(*this_child != *first_child)
   //}

/*** Entity Status Mutex Locking & Unlocking ***/
/*
static Bool
_lock(Indx target_row, Status mutex)
{
   ATOMIC Entity_Metadata *metadata, new_metadata, old_metadata;

   metadata = eng_vault_fetch(entity_vault, METADATA, target_row);
   if(!metadata) return EINA_FALSE;

   do {
      old_metadata = LOAD(metadata);

      // If any of our target flags are already locked, fail.
      if(old_metadata.status & mutex) return EINA_FALSE;

      // Since this is a write, increment our generational counter.
      new_metadata.update  = old_metadata.update + 1;
      new_metadata.orders  = old_metadata.orders;
      old_metadata.status ^= mutex;
      new_metadata.status  = old_metadata.status;

   } while(!CAS(metadata, &old_metadata, new_metadata));

   return EINA_TRUE;
}

static Bool
_unlock(Indx target_row, Status mutex)
{
   ATOMIC Entity_Metadata *metadata, new_metadata, old_metadata;

   metadata = eng_vault_fetch(entity_vault, METADATA, target_row);
   if(!metadata) return EINA_FALSE;

   do {
      old_metadata = LOAD(metadata);

      // If any of our target flags are already unlocked, fail.
      if(~(old_metadata.status & mutex) & mutex) return EINA_FALSE;

      // Since this is a write, increment our generational counter.
      new_metadata.update  = old_metadata.update + 1;
      new_metadata.orders  = old_metadata.orders;
      old_metadata.status ^= mutex;
      new_metadata.status  = old_metadata.status;

   } while(!CAS(metadata, &old_metadata, new_metadata));

   return EINA_TRUE;
}
*/


