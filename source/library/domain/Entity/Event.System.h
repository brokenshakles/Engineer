#ifndef _ENTITY_EVENT_SYSTEM_H_
#define _ENTITY_EVENT_SYSTEM_H_

#include <eng_system.h>

ESML(
   SYMBOL(Entity.Event)
   PREREQ()
   ENTITY(
      IGNORE()
      TARGET(Entity.Event)
      IMPORT(
         ASSET(Entity,
            COMPONENT(Event,
               FIELD(Eina_Inarray*, outbox)
               FIELD(Eina_Inarray*, inbox)
      )  )  )
      EXPORT(
         ASSET(Entity,
            COMPONENT(Event,
               FIELD(Eina_Inarray*, outbox)
               FIELD(Eina_Inarray*, inbox)
      )  )  )
      EVENTS()
)  )

ENG_API void eng_entity_notify(Entity target, String *event, Indx size, void *payload);

#endif


