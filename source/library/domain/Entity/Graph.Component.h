#ifndef _ENTITY_GRAPH_COMPONENT_H_
#define _ENTITY_GRAPH_COMPONENT_H_

#include <eng_component.h>

ECML(
   SYMBOL(Entity.Graph)
   ENTITY(
      FIELD(Entity, parent)
      FIELD(Entity, first_child)
      FIELD(Entity, sibling_next)
      FIELD(Entity, sibling_prev)
)  )

#endif

//FIELD(Eng_Scene_Data*, scene)
//FIELD(String*,          name)
//FIELD(Vec3,             size)
//FIELD(Indx,            shape)


