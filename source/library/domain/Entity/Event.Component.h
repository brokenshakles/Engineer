#ifndef _ENTITY_EVENT_COMPONENT_H_
#define _ENTITY_EVENT_COMPONENT_H_

#include <eng_component.h>

ECML(
   SYMBOL(Entity.Event)
   ENTITY(
      FIELD(Eina_Inarray*, inbox)
      FIELD(Eina_Inarray*, outbox)
)  )

#endif


