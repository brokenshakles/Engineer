#ifndef _ENTITY_GRAPH_SYSTEM_H_
#define _ENTITY_GRAPH_SYSTEM_H_

#include <eng_system.h>

ESML(
   SYMBOL(Entity.Graph)
   PREREQ(
      DOMAIN(Entity,
         SYSTEM(Event)
   )  )
   ENTITY(
      IGNORE()
      TARGET(Entity.Graph)
      IMPORT(
         ASSET(Entity,
            COMPONENT(Graph,
               FIELD(Entity, parent)
               FIELD(Entity, first_child)
               FIELD(Entity, sibling_next)
               FIELD(Entity, sibling_prev)
      )  )  )
      EXPORT(
         ASSET(Entity,
            COMPONENT(Graph,
               FIELD(Entity, parent)
               FIELD(Entity, first_child)
               FIELD(Entity, sibling_next)
               FIELD(Entity, sibling_prev)
      )  )  )
      EVENTS(
         FIELD(Entity, new_parent)

         EVENT(entity_attach, sizeof(Entity))
)  )  )

ENG_API void   eng_entity_attach(Entity target, Entity parent);
ENG_API Entity eng_entity_parent(Entity target);

#endif


