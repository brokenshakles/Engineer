#include "System.h"

static void
entity_attach(Entity_State *entity) {}
// Do not call any function that would cause this entity to detach from this System inside this
//    function. The results are presumed undefined at this point.

static void
entity_upkeep(Entity_State *entity)
{
   // Remove the Entity.Event Component if no Entity_Notice(s) are left.
   if(!entity->outbox && !entity->inbox) eng_component_delete(this, "Entity.Event");
}

static void
entity_update(Entity_State *entity)
{
   // We cycle the mailboxen here.
   if(entity->outbox) { eina_inarray_free(entity->outbox);  entity->outbox = NULL; }
   if(entity->inbox)  { entity->outbox = entity->inbox;     entity->inbox  = NULL; }
}

static void
entity_detach(Entity_State *entity)
{
   if(entity->outbox) eina_inarray_free(entity->outbox);
   if(entity->inbox)  eina_inarray_free(entity->inbox);
}


/*** Entity Notification API ***/

static void
_notify__work(void *args, void *result)
{
   Entity_Notice *info = args;

   Entity_Event *mail = eng_component_status(info->target, "Entity.Event"), box;
   if(!mail)   { mail = &box;  box = { NULL, eina_inarray_new(sizeof(Byte), 0) };
                        eng_component_create(info->target, "Entity.Event", mail); }

   if(!mail->inbox) mail->inbox = eina_inarray_new(sizeof(Byte), 0);

   Entity_Notice *notice = eina_inarray_grow(mail->inbox, sizeof(Entity_Notice) + info->size);

   memcpy(notice->payload, info, info->size);  // The payoff.

   RETURN : *(Bool*)result = EINA_TRUE;
}

static Bool
_notify(Entity target, Digest event, Indx size, void *payload)
{
   Entity_Notice *call = eng_work_call(target, _eng_entity_notify__work,
                                       sizeof(Entity_Notice) + size, sizeof(Bool));

   if(call) *call = { this, target, event, size };
   else return EINA_FALSE;

   if(payload) memcpy(call->payload, payload, size);
   else        memset(call->payload,    VOID, size);

   return *(Bool*)eng_work_wait(call);
}

ENG_API void
eng_entity_notify(Entity target, String *event, Indx size, void *payload)
{
   char     event_with_size[strlen(event) + 12];
   snprintf(event_with_size,strlen(event) + 12, "%s.%d" event, size);

   if(!_notify(target, eng_digest(event_with_size), size, payload)) return;

   printf("Entity Notified.    | Entity: %ld, Event: %s, Size %d\n", target, event, size);
}


