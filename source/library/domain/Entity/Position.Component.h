#ifndef _ENTITY_POSITION_COMPONENT_H_
#define _ENTITY_POSITION_COMPONENT_H_

#include <eng_component.h>

ECML(
   SYMBOL(Entity.Position)
   ENTITY(
      FIELD(Vec3, translation)
      FIELD(Sclr, translock)

      FIELD(Quat, orientation)
      FIELD(Sclr, orilock)
)  )

#endif


