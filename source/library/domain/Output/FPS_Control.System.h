#ifndef _CORE_FPS_CONTROL_SYSTEM_H_
#define _CORE_FPS_CONTROL_SYSTEM_H_

#include <eng_system.h>

SYMBOL(Core, FPS_Control)

PREREQS()

GLOBAL()

ENTITY(
   INPUTS(
      ASSET(Core,
         COMPONENT(FPS_Control,
            FIELD(Sclr, topspeed)
            FIELD(Sclr, acceleration)
         )
         COMPONENT(Transform,
            FIELD(Vec3, position)
            FIELD(Quat, orientation)
         )
      )
   )
   OUTPUTS(
      ASSET(Core,
         COMPONENT(Transform,
            FIELD(Vec3, position)
            FIELD(Quat, orientation)
         )
      )
   )
   EVENTS(
      FIELD(Vec3, translation)
      FIELD(Eulr, torque)

      EVENT(move_forward,   0)
      EVENT(move_rearward,  0)
      EVENT(move_left,      0)
      EVENT(move_right,     0)
      EVENT(roll_left,      0)
      EVENT(roll_right,     0)
      EVENT(yaw_left,       sizeof(Vec2))
      EVENT(yaw_right,      sizeof(Vec2))
      EVENT(pitch_forward,  sizeof(Vec2))
      EVENT(pitch_rearward, sizeof(Vec2))
   )
)

#endif

