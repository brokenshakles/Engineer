#ifndef _ENGINEER_H_PRIVATE_
#define _ENGINEER_H_PRIVATE_

#include "../headers/engineer.h"
#include "../headers/eng_thread.h"

//#include "gui/eng_console.h"
//#include "gui/eng_viewport.h"

#define ENG_PRIVATE

// Some shorthand for atomic operations from <stdatomic.h>.
#define ATOMIC _Atomic

#define LOAD(a)       atomic_load(a)
#define STORE(a, b)   atomic_store(a, b)
#define AFA(a, b)     atomic_fetch_add(a, b)
#define AFS(a, b)     atomic_fetch_sub(a, b)
#define CAS(a, b, c)  atomic_compare_exchange_weak(a, b, c)
#define CASS(a, b, c) atomic_compare_exchange_strong(a, b, c)

#define THREAD_LOCAL    _Thread_local  // Or __thread

#define VOID            0x00
#define INDEXING_OFFSET 0x01

// Opaque Structures for Component and System Modules.
typedef struct Entity_State Entity_State;
typedef struct System_State System_State;

// Misc Utility Functions.
void eng_empty_cb(void *data);
void eng_free_cb(void *data);

void _cord_thread_init();
void _cord_thread_free();


/*** Extra/Scratchpad ***/

#define INDX_MAX ~((Indx)0)

#endif

