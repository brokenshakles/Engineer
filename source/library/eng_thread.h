#ifndef _ENG_THREAD_H_PRIVATE_
#define _ENG_THREAD_H_PRIVATE_

#include "eng_identity.h"

typedef struct Eng_Thread_Init Eng_Thread_Init;
typedef struct Eng_Thread_Data Eng_Thread_Data;

struct Eng_Thread_Init
{
   String      *name;
   Thread       scene;
   Thread       shard;
   Eina_Barrier dispatch;
};

Thread _eng_thread_boot();
void   _eng_thread_halt();

Thread _eng_thread_init(Thread parent, Eina_Thread eina_identity);
void   _eng_thread_free();

Thread            _eng_thread_this();
Eng_Thread_Data * _eng_thread_that(Thread identity);

void   _eng_thread_dispatch();

Entity * _eng_work_this();
void   * _eng_work_sync(Set *target, void *method, Indx args_size, Indx result_size);

#endif

// This is Engineer's Threading & async Work infrastructure, which is the backend shared by
//   eng_node, eng_scene, and eng_shard.  It itself is a wrapper/frontend around both util/cord.h's
//   and Eina_Thread's architecture.

