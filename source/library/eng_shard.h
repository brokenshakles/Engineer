#ifndef _ENG_SHARD_H_PRIVATE_
#define _ENG_SHARD_H_PRIVATE_

#include "eng_scene.h"

typedef struct Eng_Shard_Module    Eng_Shard_Module;
typedef struct Eng_Shard_Component Eng_Shard_Component;
typedef struct Eng_Shard_System    Eng_Shard_System;

// This is a redundant "overlay" struct used for accessing Component and System Scene_Module
//    structs with the same language.
struct Eng_Shard_Module
{
   #define ENG_SHARD_MODULE_HEADER(flavor) \
   Eng_Node_##flavor  *node;               \
   Eng_Scene_##flavor *scene;              \
                                           \
   Eina_Hash *dependent_systems;           \
   Eng_Vault *operatives;                  \

   ENG_SHARD_MODULE_HEADER(Module);
};

struct Eng_Shard_Component
{
   ENG_SHARD_MODULE_HEADER(Component);
};

struct Eng_Shard_System
{
   ENG_SHARD_MODULE_HEADER(System);
   Eng_Vault *candidates;  // Stores the Entities that have some but not all of the requirements.
   Set       *invalid;     // Contains Entities that have been detached from the System before
                           //    completion.
   Entity_State *buffer;
};


/*** Shard Internal API ***/

void * _eng_shard_init(void *data, Eina_Thread id);
void   _eng_shard_free();

Bool   _eng_shard_component_attach_entity(Digest component, Entity target, void *initial);
void * _eng_shard_component_detach_entity(Digest component, Entity target);

#endif

