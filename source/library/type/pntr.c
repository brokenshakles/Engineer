#define SWITCH

#include "type/pntr.h"
#include "type.c"

extern inline void Pntr_VAULT(Eng_Vault **vault, String *symbol);
extern inline void Pntr_STORE(Eng_Vault **vault, Entity target, Indx column, Pntr *field);
extern inline void Pntr_FETCH(Eng_Vault **vault, Entity target, Indx column, Pntr *field);


