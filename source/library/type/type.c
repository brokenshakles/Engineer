#ifdef TYPE
#ifdef BASE
#ifdef ROOT

#define DEFINE(type) DEFINE_(type)
#define DEFINE_(type)                                                        \
   extern inline void                                                        \
   type##_INDEX(Eina_Inarray *columns, String *symbol);                      \
                                                                             \
   extern inline void                                                        \
   type##_STORE(Eng_Vault *operatives, Indx *column, Indx row, type *field); \
                                                                             \
   extern inline void                                                        \
   type##_FETCH(Eng_Vault *operatives, Indx *column, Indx row, type *field); \
                                                                             \
   extern inline void                                                        \
   type##_TABLE(Eng_Vault *operatives, String *field);                       \
                                                                             \
   extern inline void                                                        \
   type##_INOUT(Eng_Vault *info, String *field, Indx *column);
DEFINE(TYPE)
#undef DEFINE_
#undef DEFINE

#ifdef SIGN
   #undef SIGN
#endif

//#ifdef SWITCH
   #undef ROOT
   #undef BASE
   #undef TYPE
//#endif

#endif
#endif
#endif


#ifdef COMPOUND
#ifdef SUBTYPES

#define SYMBOL(type) extern inline void type##_INDEX
#define DEFINE(type) SYMBOL(type)(Eina_Inarray *columns, String *field);
DEFINE(COMPOUND)
#undef DEFINE
#undef SYMBOL

#define SYMBOL(type) extern inline void type##_STORE
#define DEFINE(type) SYMBOL(type)(Eng_Vault *operatives, Indx *column, Indx row, type *field);
DEFINE(COMPOUND)
#undef DEFINE
#undef SYMBOL

#define SYMBOL(type) extern inline void type##_FETCH
#define DEFINE(type) SYMBOL(type)(Eng_Vault *operatives, Indx *column, Indx row, type *field);
DEFINE(COMPOUND)
#undef DEFINE
#undef SYMBOL

#define SYMBOL(type) extern inline void type##_TABLE
#define DEFINE(type) SYMBOL(type)(Eng_Vault *operatives, String *field);
DEFINE(COMPOUND)
#undef DEFINE
#undef SYMBOL

#define SYMBOL(type) extern inline void type##_INOUT
#define DEFINE(type) SYMBOL(type)(Eng_Vault *info, String *field, Indx *column);
DEFINE(COMPOUND)
#undef DEFINE
#undef SYMBOL

//#ifdef SWITCH
   #undef SUBTYPES
   #undef COMPOUND
//#endif

#endif
#endif

