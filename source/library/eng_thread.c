#include "eng_thread.h"

#define NULL_RESULT_SIZE 8

#define WORD_ALIGN(target, word_size) ((target + (word_size - 1)) & ~(word_size - 1))

typedef enum   Eng_Work_Mode   Eng_Work_Mode;
typedef struct Eng_Work_Header Eng_Work_Header;
typedef struct Eng_Work_Order  Eng_Work_Order;
typedef struct Eng_Work_Result Eng_Work_Result;


/*** Thread Definitions ***/

struct Eng_Thread_Data
{
   Thread identity;       // An improper Entity that represents this Thread.
   Thread parent;

   Eina_Thread eina_identity;

   Eng_Work_Order *work;  // This Thread's currently running Eng_Work_Order.
   Set            *call;  // A persistent Set used in setting up non-echo Calls and Tasks.

   Eina_Lock      wake_mutex;
   Eina_Condition wake;

   Eng_Work_Order *yielding_busy_plea_result;  Eina_Thread_Queue *busy_plea_results;
   Eng_Work_Order *yielding_busy_plea_order;   Eina_Thread_Queue *busy_plea_orders;
   Eng_Work_Order *yielding_busy_call_result;  Eina_Thread_Queue *busy_call_results;
   Eng_Work_Order *yielding_busy_call_order;   Eina_Thread_Queue *busy_call_orders;
   Eng_Work_Order *yielding_busy_task_result;  Eina_Thread_Queue *busy_task_results;
   Eng_Work_Order *yielding_busy_task_order;   Eina_Thread_Queue *busy_task_orders;

   Eng_Work_Order *yielding_lazy_plea_result;  Eina_Thread_Queue *lazy_plea_results;
   Eng_Work_Order *yielding_lazy_plea_order;   Eina_Thread_Queue *lazy_plea_orders;
   Eng_Work_Order *yielding_lazy_call_result;  Eina_Thread_Queue *lazy_call_results;
   Eng_Work_Order *yielding_lazy_call_order;   Eina_Thread_Queue *lazy_call_orders;
   Eng_Work_Order *yielding_lazy_task_result;  Eina_Thread_Queue *lazy_task_results;
   Eng_Work_Order *yielding_lazy_task_order;   Eina_Thread_Queue *lazy_task_orders;
};


/*** Work Definitions ***/

enum Eng_Work_Mode
{
   ENG_WORK_PLEA,
   ENG_WORK_ECHO,
   ENG_WORK_CALL,
   ENG_WORK_SYNC, // Syncronizing Echo, always called from parent Node/Scene.  Busy only.
   ENG_WORK_TASK, // Everything that is not a Task is considered a variety of Call.
};

struct Eng_Work_Header
{
   Eina_Thread_Queue_Msg msg;

   Bool          busy;
   Eng_Work_Mode mode;

   void *alloc; // Needed for use with eina_thread_queue_*()'s allocref *s.

   Entity           target;
   Eng_Work_Order  *parent;
   Eng_Work_Result *result;  // This is a linked list that enables result garbage collection.
};

struct Eng_Work_Order
{
   Eng_Work_Header header;

   Indx result_size;
   Indx echoes;       // This is how many subcords are currently outstanding on this cord.

   Cord  *cord;
   void (*method)(void*, Eng_Work_Result*);
   Byte   args[];
};

struct Eng_Work_Result
{
   Eng_Work_Header  header;
   Eng_Thread_Order priority;

   void *padding[2];  // Make sure our payload is 64-byte aligned.
   Byte  payload[];
};


/*** Thread Management ***/

static Eina_RWLock   thread_mutex;
static Eina_Inarray *thread_index;

THREAD_LOCAL Eng_Thread_Data *thread;

ENG_PRIVATE Thread
_eng_thread_boot()
{
   eina_rwlock_new(&thread_mutex);
   thread_index = eina_inarray_new(sizeof(Eng_Thread_Data*), VOID);

   _eng_identity_boot();
   _eng_thread_init(THREAD_MIN, eina_thread_self());

   return thread->identity;
}

ENG_PRIVATE void
_eng_thread_halt()
{
   eina_inarray_free(thread_index);
   eina_rwlock_free(&thread_mutex);
}

ENG_PRIVATE Thread
_eng_thread_init(Thread parent, Eina_Thread eina_identity)
{
   _cord_thread_init();

   thread           = malloc(sizeof(Eng_Thread_Data));
   thread->identity = _eng_thread_take_id();
   thread->parent   = parent;

   thread->eina_identity = eina_identity;

   thread->work = NULL;
   thread->call = eng_set_create(64, 1, 0);

   eina_lock_new(&thread->wake_mutex);
   eina_condition_new(&thread->wake, &thread->wake_mutex);

   thread->yielding_busy_plea_result = NULL;  thread->busy_plea_results = eina_thread_queue_new();
   thread->yielding_busy_plea_order  = NULL;  thread->busy_plea_orders  = eina_thread_queue_new();
   thread->yielding_busy_call_result = NULL;  thread->busy_call_results = eina_thread_queue_new();
   thread->yielding_busy_call_order  = NULL;  thread->busy_call_orders  = eina_thread_queue_new();
   thread->yielding_busy_task_result = NULL;  thread->busy_task_results = eina_thread_queue_new();
   thread->yielding_busy_task_order  = NULL;  thread->busy_task_orders  = eina_thread_queue_new();

   thread->yielding_lazy_plea_result = NULL;  thread->lazy_plea_results = eina_thread_queue_new();
   thread->yielding_lazy_plea_order  = NULL;  thread->lazy_plea_orders  = eina_thread_queue_new();
   thread->yielding_lazy_call_result = NULL;  thread->lazy_call_results = eina_thread_queue_new();
   thread->yielding_lazy_call_order  = NULL;  thread->lazy_call_orders  = eina_thread_queue_new();
   thread->yielding_lazy_task_result = NULL;  thread->lazy_task_results = eina_thread_queue_new();
   thread->yielding_lazy_task_order  = NULL;  thread->lazy_task_orders  = eina_thread_queue_new();

   eina_rwlock_take_write(&thread_mutex);

   Eng_Thread_Data **_thread = eina_inarray_nth(thread_index, thread->identity - THREAD_MIN);
   if(_thread) *_thread = thread;
   else eina_inarray_push(thread_index, &thread);

   eina_rwlock_release(&thread_mutex);

   return thread->identity;
}

ENG_PRIVATE void
_eng_thread_free()
{
   eina_rwlock_take_write(&thread_mutex);

   Eng_Thread_Data **_thread = eina_inarray_nth(thread_index, thread->identity - THREAD_MIN);
   *_thread = NULL;

   Indx thread_population = eina_inarray_count(thread_index);
   if(thread->identity - THREAD_MIN == thread_population - INDEXING_OFFSET)
      do {
         eina_inarray_pop(thread_index);
         _thread = eina_inarray_nth(thread_index, --thread_population);
      }
      while(!*_thread);

   eina_rwlock_release(&thread_mutex);

   eina_thread_queue_free(thread->lazy_task_orders);
   eina_thread_queue_free(thread->lazy_task_results);
   eina_thread_queue_free(thread->lazy_call_orders);
   eina_thread_queue_free(thread->lazy_call_results);
   eina_thread_queue_free(thread->lazy_plea_orders);
   eina_thread_queue_free(thread->lazy_plea_results);

   eina_thread_queue_free(thread->busy_task_orders);
   eina_thread_queue_free(thread->busy_task_results);
   eina_thread_queue_free(thread->busy_call_orders);
   eina_thread_queue_free(thread->busy_call_results);
   eina_thread_queue_free(thread->busy_plea_orders);
   eina_thread_queue_free(thread->busy_plea_results);

   eina_condition_free(&thread->wake);
   eina_lock_free(&thread->wake_mutex);

   eng_set_delete(thread->call);

   _eng_thread_give_id(thread->identity);
   free(thread);

   _cord_thread_free();
}

ENG_PRIVATE Thread
_eng_thread_this() { return thread->identity; }

ENG_PRIVATE Eng_Thread_Data *
_eng_thread_that(Thread identity)
{
   Eng_Thread_Data **get, *that = NULL;

   eina_rwlock_take_read(&thread_mutex);

   get = eina_inarray_nth(thread_index, identity - THREAD_MIN);  if(get) that = *get;

   eina_rwlock_release(&thread_mutex);

   return that;
}

static void
_thread_wakeup(Eng_Thread_Data *that, Eng_Thread_Order new_priority)
{
   // Attempt to escalate the target Thread's priority, and if the target's Thread of residence is
   //    asleep, wake it up.
   if(_eng_thread_priority(that->identity, new_priority) >= PRIORITY_ASLEEP)
      eina_condition_signal(&that->wake);
}

static Eina_Thread_Queue *
_thread_queue(Eng_Thread_Data *that, Eng_Thread_Order priority)
{
   switch(priority)
   {
      case PRIORITY_AWAKE : return NULL;

      case PRIORITY_BUSY_PLEA_RESULT : return that->busy_plea_results;
      case PRIORITY_BUSY_PLEA_ORDER  : return that->busy_plea_orders;
      case PRIORITY_BUSY_CALL_RESULT : return that->busy_call_results;
      case PRIORITY_BUSY_CALL_ORDER  : return that->busy_call_orders;
      case PRIORITY_BUSY_TASK_RESULT : return that->busy_task_results;
      case PRIORITY_BUSY_TASK_ORDER  : return that->busy_task_orders;

      case PRIORITY_LAZY_PLEA_RESULT : return that->lazy_plea_results;
      case PRIORITY_LAZY_PLEA_ORDER  : return that->lazy_plea_orders;
      case PRIORITY_LAZY_CALL_RESULT : return that->lazy_call_results;
      case PRIORITY_LAZY_CALL_ORDER  : return that->lazy_call_orders;
      case PRIORITY_LAZY_TASK_RESULT : return that->lazy_task_results;
      case PRIORITY_LAZY_TASK_ORDER  : return that->lazy_task_orders;

      case PRIORITY_ASLEEP : return NULL;
      case PRIORITY_LOOKUP : return NULL;
   }
   return NULL;
}


/*** Eng_Work_Order Identification ***/

THREAD_LOCAL Entity this; // Stores the Entity of the current Work.

ENG_PRIVATE Entity *
_eng_work_this() { return &thread->work->header.target; }


/*** Dispatcher Algorithim ***/

static void
_dispatch_cleanup(Eng_Thread_Order priority)
{
   Eng_Work_Result   *next, *result;
   Eina_Thread_Queue *thread_queue;

   // Here we cleanup the accumulated resolved results that are listed in work->header.result.
   next = thread->work->header.result;
   while(next)
   {
      result       = next;
      next         = result->header.result;
      thread_queue = _thread_queue(thread, result->priority);
      eina_thread_queue_wait_done(thread_queue, result->header.alloc);
      _eng_entity_detach_reference(result->header.target);
   };

   // Finally we cleanup our work struct allocation.
   thread_queue = _thread_queue(thread, priority);
   eina_thread_queue_wait_done(thread_queue, thread->work->header.alloc);
   _eng_thread_detach_reference(thread->identity);
}

static void
_dispatch_task(void *data)
{
   Eng_Thread_Order priority = *(Eng_Thread_Order*)data;

   thread->work->method(thread->work->args, NULL);

   _eng_entity_detach_reference(thread->work->header.target);

   _dispatch_cleanup(priority);
}

static void
_dispatch_result(void *payload, Eng_Thread_Order priority)
{
   // Figure out to which thread the result is sent to.
   Eng_Work_Order  *parent = thread->work->header.parent;
   Eng_Thread_Data *that   = _eng_thread_that(parent->header.target);

   // Copy our result from our *payload into the parent work's thread result queue.
   Eng_Work_Result   *result;
   Eina_Thread_Queue *queue = _thread_queue(that, priority);
   Indx               total = WORD_ALIGN(sizeof(Eng_Work_Result) + thread->work->result_size, 64);
   void              *alloc;

   result                = eina_thread_queue_send(queue, total, &alloc);
   result->header.mode   = parent->header.mode;
   result->header.alloc  = alloc;
   result->header.target = thread->work->header.target;
   result->header.parent = thread->work->header.parent;
   result->priority      = priority;

   if(thread->work->result_size) memcpy(result->payload, payload, thread->work->result_size);

   eina_thread_queue_send_done(queue, result->header.alloc);

   // Let the recieving thread know a new result has been sent to one of it's queues.
   _thread_wakeup(that, priority);
}

static void
_dispatch_call(void *data)
{
   Eng_Thread_Order priority = *(Eng_Thread_Order*)data;

   // Allocate some space for our return value on the stack, then run our call's function.
   uint8_t result[thread->work->result_size ? thread->work->result_size : NULL_RESULT_SIZE];
   thread->work->method(thread->work->args, thread->work->result_size ? (void*)result : NULL);

   // Once the work is done, we need to return a result on it's parent's result queue.
   _dispatch_result(result, --priority);
   _dispatch_cleanup(priority);
}

static void
_dispatch_queue_item(Eng_Thread_Order *priority)
{
   // If the work->cord is NULL, the queue_item is new, so we create a new work_order.
   if(!thread->work->cord) switch(thread->work->header.mode)
   {
      case ENG_WORK_PLEA :
      case ENG_WORK_ECHO :
      case ENG_WORK_CALL : // A Sync is an Echo, but with Task-like priority.
      case ENG_WORK_SYNC : thread->work->cord = cord_spawn(_dispatch_call, priority); return;
      case ENG_WORK_TASK : thread->work->cord = cord_spawn(_dispatch_task, priority); return;
   }

   // If we fall thru to here, then our queue_item is a result.
   Eng_Work_Result *head, *result = (Eng_Work_Result*)thread->work;

   // Set our "work" global back to the parent work_order that spawned the current result.
   thread->work = result->header.parent;

   // Insert the newest result at the old head of the result echo chain (linked list).
   head                        = thread->work->header.result;
   thread->work->header.result = result;
   result->header.result       = head;

   // If no outstanding echoes are left, continue working on our results' parent work_order.
   if(!--thread->work->echoes) cord_await(thread->work->cord, thread->work);
}

static Bool
_dispatch_yield_item(Eng_Thread_Order *priority)
{
   Eng_Work_Order **yielding;

   switch(*priority)
   {
      case PRIORITY_AWAKE : return EINA_FALSE;

      case PRIORITY_BUSY_PLEA_RESULT : yielding = &thread->yielding_busy_plea_result; break;
      case PRIORITY_BUSY_PLEA_ORDER  : yielding = &thread->yielding_busy_plea_order;  break;
      case PRIORITY_BUSY_CALL_RESULT : yielding = &thread->yielding_busy_call_result; break;
      case PRIORITY_BUSY_CALL_ORDER  : yielding = &thread->yielding_busy_call_order;  break;
      case PRIORITY_BUSY_TASK_RESULT : yielding = &thread->yielding_busy_task_result; break;
      case PRIORITY_BUSY_TASK_ORDER  : yielding = &thread->yielding_busy_task_order;  break;

      case PRIORITY_LAZY_PLEA_RESULT : yielding = &thread->yielding_lazy_plea_result; break;
      case PRIORITY_LAZY_PLEA_ORDER  : yielding = &thread->yielding_lazy_plea_order;  break;
      case PRIORITY_LAZY_CALL_RESULT : yielding = &thread->yielding_lazy_call_result; break;
      case PRIORITY_LAZY_CALL_ORDER  : yielding = &thread->yielding_lazy_call_order;  break;
      case PRIORITY_LAZY_TASK_RESULT : yielding = &thread->yielding_lazy_task_result; break;
      case PRIORITY_LAZY_TASK_ORDER  : yielding = &thread->yielding_lazy_task_order;  break;

      case PRIORITY_ASLEEP : return EINA_FALSE;
      case PRIORITY_LOOKUP : return EINA_FALSE;
   }
   if(!*yielding) return EINA_FALSE;

   thread->work = *yielding;  *yielding = NULL;
   this = thread->work->header.target;

   cord_await(thread->work->cord, thread->work);
   return EINA_TRUE;
}

ENG_PRIVATE void
_eng_thread_dispatch()
{
   Eng_Thread_Order priority = _eng_thread_priority(thread->identity, PRIORITY_LOOKUP);

   // Process the available work on each priority level, from top to bottom.
   while(!_eng_thread_is_defunct(thread->identity) && eina_condition_wait(&thread->wake))
      do {
         // If we have a yielding work_order at the current priority, resume it.
         if(_dispatch_yield_item(&priority)) continue;

         // If not, process the next queue_item off of the current queue.
         Eina_Thread_Queue *queue = _thread_queue(thread, priority);  void *alloc;
         if(queue && (thread->work = eina_thread_queue_poll(queue, &alloc)) )
         {
            this = thread->work->header.target;  thread->work->header.alloc = alloc;
            _dispatch_queue_item(&priority); continue;
         }
         // Move down to the next priority level, or up to the new top priority level.
         priority = _eng_thread_priority_next(thread->identity, priority);
      }
      while(priority != PRIORITY_ASLEEP);

   _eng_thread_free();
}


/*** API Tail Calls ***/

static Eng_Thread_Order
_work_priority(Bool busy, Eng_Work_Mode mode)
{
   if(busy) switch(mode)
   {
      case ENG_WORK_PLEA : return PRIORITY_BUSY_PLEA_ORDER;
      case ENG_WORK_ECHO :
      case ENG_WORK_CALL : return PRIORITY_BUSY_CALL_ORDER;
      case ENG_WORK_SYNC :
      case ENG_WORK_TASK : return PRIORITY_BUSY_TASK_ORDER;
   }
   else switch(mode)
   {
      case ENG_WORK_PLEA : return PRIORITY_LAZY_PLEA_ORDER;
      case ENG_WORK_ECHO :
      case ENG_WORK_CALL : return PRIORITY_LAZY_CALL_ORDER;
      case ENG_WORK_SYNC :
      case ENG_WORK_TASK : return PRIORITY_LAZY_TASK_ORDER;
   }
   __builtin_unreachable();
}

static Bool
_work_attach(Set *target, Eng_Thread_Data **that)
{
   Entity *resident;  Indx thread_echo = 0, entity_echo = 0, dead_echo;
   ENG_SET_FOREACH(target, resident)
   {
          that[thread_echo] = _eng_thread_that(_eng_entity_residence(*resident));
      if(!that[thread_echo] || !_eng_thread_attach_reference(that[thread_echo++]->identity))
         goto THREAD_FAIL;
   }

   ENG_SET_FOREACH(target, resident)
   {
      entity_echo++;
      if(!_eng_entity_attach_reference(*resident)) goto ENTITY_FAIL;
   }

   return EINA_TRUE;

   ENTITY_FAIL :

   ENG_SET_FOREACH(target, resident)
   {
      dead_echo = 0;
      if(dead_echo++ < entity_echo) _eng_entity_detach_reference(*resident);
      else goto THREAD_FAIL;
   }

   THREAD_FAIL :

   ENG_SET_FOREACH(target, resident)
   {
      dead_echo = 0;
      if(dead_echo < thread_echo) _eng_thread_detach_reference(that[dead_echo++]->identity);
      else goto FAILURE;
   }

   FAILURE : return EINA_FALSE;
}

static void *
_work_setup__tail(Set  *target, Bool busy,      Eng_Work_Mode mode,
                  void *method, Indx args_size, Indx          result_size)
{
   Eng_Thread_Data *that[eng_set_find_size(target)];  Indx echo = 0;

   // Make sure that all of our Entity and Thread targets exist, if not, fail.
   if(!_work_attach(target, that)) return NULL;

   args_size = WORD_ALIGN(args_size + sizeof(Eng_Work_Order), 64);

   // Lazy Work is not allowed to spawn Busy Work.
   if(busy && thread->work->header.busy == EINA_FALSE)                   busy = EINA_FALSE; // Does the root work of a given thread have thread->work->header.busy set properly?
   if(mode == ENG_WORK_CALL && that[echo]->identity == thread->parent)   mode = ENG_WORK_PLEA;
   if(mode == ENG_WORK_CALL && that[echo]->identity == thread->identity) mode = ENG_WORK_PLEA;

   // Let's allocate a new Cord and return it to the downstream programmer for completion.
   Entity          *resident;
   Eng_Thread_Order priority = _work_priority(busy, mode);
   Eng_Work_Order  *old_work = NULL, *new_work;
   Eng_Work_Header  header;

   ENG_SET_FOREACH(target, resident)
   {
      Eina_Thread_Queue *queue  = _thread_queue(that[echo], priority);  void *alloc;
      Eng_Work_Order    *parent = (void*)that[echo++];
      Eng_Work_Result   *result = (void*)old_work;
      Indx               echoes = (Indx )priority;
      Cord              *cord   = (void*)queue;

      new_work = eina_thread_queue_send(queue, args_size, &alloc);

      header   = (Eng_Work_Header) { { args_size }, busy, mode, alloc, *resident, parent, result };
     *new_work = (Eng_Work_Order)  { header, result_size, echoes, cord, method };

      old_work = new_work;
   }
   return new_work->args;
}

static void *
_work_order__tail(Eng_Work_Order *new_work)
{
   Eng_Work_Order *old_work = NULL;

   if(!new_work) return NULL;
   while(new_work)
   {
      // Buffering in some values before they are overwritten by a proper initialization.
      Eng_Thread_Data   *that     = (Eng_Thread_Data  *)new_work->header.parent;
      Eng_Thread_Order   priority = (Eng_Thread_Order  )new_work->echoes;
      Eina_Thread_Queue *queue    = (Eina_Thread_Queue*)new_work->cord;

      new_work->header.parent = thread->work;
      new_work->echoes        = VOID;
      new_work->cord          = NULL;

      // Copy over the input arguments given by the downstream programmer to each Echo.
      if(old_work) memcpy(new_work->args, old_work->args, old_work->header.msg.size);

      // If this is any kind of Call, add an Echo reference to our parent Eng_Work_Order.
      if(new_work->header.mode != ENG_WORK_TASK) thread->work->echoes++;

      eina_thread_queue_send_done(queue, new_work->header.alloc);

      _thread_wakeup(that, priority);

      // Pop off the old_work from the temporary linked list and process the next new_work.
      old_work = new_work;
      new_work = (Eng_Work_Order*)old_work->header.result;  old_work->header.result = NULL;
   }

   switch(old_work->header.mode)
   {
      case ENG_WORK_PLEA :
      case ENG_WORK_ECHO :
      case ENG_WORK_CALL :
      case ENG_WORK_SYNC : cord_yield(thread->work); break;
      case ENG_WORK_TASK : eng_work_yield();         break;
   }

   Eng_Work_Result  *result = thread->work->header.result;
   if(result) return result->payload;
   else       return NULL;
}


/*** Flow Control API ***/

ENG_API void
eng_work_yield()
{
   switch(_eng_thread_priority(thread->identity, PRIORITY_LOOKUP))
   {
      case PRIORITY_BUSY_PLEA_RESULT : thread->yielding_busy_plea_result = thread->work;  break;
      case PRIORITY_BUSY_PLEA_ORDER  : thread->yielding_busy_plea_order  = thread->work;  break;
      case PRIORITY_BUSY_CALL_RESULT : thread->yielding_busy_call_result = thread->work;  break;
      case PRIORITY_BUSY_CALL_ORDER  : thread->yielding_busy_call_order  = thread->work;  break;
      case PRIORITY_BUSY_TASK_RESULT : thread->yielding_busy_task_result = thread->work;  break;
      case PRIORITY_BUSY_TASK_ORDER  : thread->yielding_busy_task_order  = thread->work;  break;

      case PRIORITY_LAZY_PLEA_RESULT : thread->yielding_lazy_plea_result = thread->work;  break;
      case PRIORITY_LAZY_PLEA_ORDER  : thread->yielding_lazy_plea_order  = thread->work;  break;
      case PRIORITY_LAZY_CALL_RESULT : thread->yielding_lazy_call_result = thread->work;  break;
      case PRIORITY_LAZY_CALL_ORDER  : thread->yielding_lazy_call_order  = thread->work;  break;
      case PRIORITY_LAZY_TASK_RESULT : thread->yielding_lazy_task_result = thread->work;  break;
      case PRIORITY_LAZY_TASK_ORDER  : thread->yielding_lazy_task_order  = thread->work;  break;

      case PRIORITY_AWAKE  : return;
      case PRIORITY_ASLEEP : return;
      case PRIORITY_LOOKUP : return;
   }
   cord_yield(thread->work);
}


/*** Syncronous Work API ***/

ENG_API void *
eng_work_call(Entity target, void *method, Indx args_size, Indx result_size)
{
   Entity *old = eng_set_find_smallest(thread->call);
   if(old) eng_set_detach(thread->call, old);
   eng_set_attach(thread->call, &target);

   Bool make_busy = thread->work->header.busy;
   return _work_setup__tail(thread->call, make_busy, ENG_WORK_CALL,
                            method,       args_size, result_size);
}

ENG_API void *
eng_work_echo(Set *target, void *method, Indx args_size, Indx result_size)
{
   Bool make_busy = thread->work->header.busy;
   return _work_setup__tail(target, make_busy, ENG_WORK_ECHO,
                            method, args_size, result_size);
}

ENG_PRIVATE void *
_eng_work_sync(Set *target, void *method, Indx args_size, Indx result_size)
{
   Bool make_busy = thread->work->header.busy;
   return _work_setup__tail(target, make_busy, ENG_WORK_SYNC,
                            method, args_size, result_size);
}

ENG_API void *
eng_work_wait(void *call)
{
   return _work_order__tail((Eng_Work_Order*)call - 1);
}


/*** Asyncronous Work API ***/

ENG_API void *
eng_work_task(Entity target, void *method, Indx args_size, Bool make_busy)
{
   Entity *old = eng_set_find_smallest(thread->call);
   if(old) eng_set_detach(thread->call, old);
   eng_set_attach(thread->call, &target);

   return _work_setup__tail(thread->call, make_busy, ENG_WORK_TASK,
                            method,       args_size, VOID);
}

ENG_API void
eng_work_pass(void *task)
{
   _work_order__tail((Eng_Work_Order*)task - 1);
}

