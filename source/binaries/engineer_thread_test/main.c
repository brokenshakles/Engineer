#include "main.h"

// This binary is a test case for a multi-producer/single-consumer threaded queue setup.

#define FAA(a, b) atomic_fetch_add(a, b)
#define BATCH_SIZE 64
#define MAX_TASKS (BATCH_SIZE * 64)

Eng_Queue    *test_queue;
Eina_Inarray *test_verifier;

_Atomic uint32_t test_sent;
_Atomic uint32_t test_recieved;
_Atomic uint32_t test_producer_starts;
_Atomic uint32_t test_consumer_starts;

typedef struct
{
   uint32_t min;
   uint32_t max;
}
Batch;

void _verifier(void *data, Ecore_Thread *thread);
void _producer(void *data, Ecore_Thread *thread);
void _consumer(void *data, Ecore_Thread *thread);

int
main()
{
   uint32_t   task;
   Batch     *batch;
   Eina_Bool  false = EINA_FALSE;

   if (!ecore_init())
   {
      printf("ERROR: Cannot init Ecore!\n");
      return -1;
   }

   ecore_thread_max_set(7);

   test_verifier = eina_inarray_new(sizeof(Eina_Bool), 0);
   test_queue    = eng_queue_new((64 * MAX_TASKS) / 2);

   eng_queue_fill_task_set(test_queue, _consumer, NULL, NULL);
   eng_queue_fill_task_max_set(test_queue, 1);

   for(task = 1; task <= MAX_TASKS; task++)
      eina_inarray_push(test_verifier, &false);

   for(task = 1; task <= MAX_TASKS; task += BATCH_SIZE)
   {
      batch = calloc(1, sizeof(Batch));

      batch->min = task;
      batch->max = task + BATCH_SIZE;
      ecore_thread_run(_producer, NULL, NULL, batch);
   }

   ecore_main_loop_begin();

   ecore_shutdown();
}

void
_shutdown(void *data EINA_UNUSED)
{
   printf("\nEcore Thread Max: %d.\n", ecore_thread_max_get());

   eina_inarray_free(test_verifier);
   eng_queue_free(test_queue);

ecore_main_loop_quit();
}

void
_verifier(void *data EINA_UNUSED, Ecore_Thread *thread EINA_UNUSED)
{
   uint32_t   count = 0;
   uint32_t   task;
   Eina_Bool *check;

   for(task = MAX_TASKS; task > 0; task--)
   {
      check = eina_inarray_pop(test_verifier);
      if(*check == EINA_TRUE)
         count++;
   }

   printf("\nTask Verification.\n");
   printf("----------------------------------------\n");
   printf("%d Messages Sent.\n",     test_sent);
   printf("%d Messages Recieved.\n", test_recieved);
   printf("%d Producer(s) Started.\n", test_producer_starts);
   printf("%d Consumer(s) Started.\n", test_consumer_starts);

   printf("Message Verification: \n");
   printf("   %d Expected, %d Verified.\n", MAX_TASKS, count);

   fflush(stdout);

   ecore_main_loop_thread_safe_call_async(_shutdown, NULL);
}

void
_verifier_cb(void *data EINA_UNUSED)
{
   ecore_thread_run(
      _verifier,
      NULL,
      NULL,
      NULL
   );
}

void
_producer(void *data, Ecore_Thread *thread EINA_UNUSED)
{
   uint32_t sent = 0;
   uint32_t *value, number;

   Batch *batch = data;

   FAA(&test_producer_starts, 1);

   for(number = batch->min; number < batch->max; number++)
   {
      if((value = eng_queue_fill(test_queue, sizeof(uint32_t))))
      {
        *value = number;
         sent += 1;
         eng_queue_fill_done(test_queue, value);
      }
   }

   FAA(&test_sent, sent);

   free(batch);
}

void
_consumer(void *data EINA_UNUSED, Ecore_Thread *thread EINA_UNUSED)
{
   uint32_t  recieved = 0;
   uint32_t *value;
   Eina_Bool true = EINA_TRUE;

   FAA(&test_consumer_starts, 1);

   while((value = eng_queue_drain(test_queue)))
   {
     eina_inarray_replace_at(test_verifier, *value - 1, &true);
     recieved += 1;

     eng_queue_drain_done(test_queue, value);
   }

   FAA(&test_recieved, recieved);
   if(atomic_load(&test_recieved) == MAX_TASKS)
      ecore_main_loop_thread_safe_call_async(_verifier_cb, NULL);
}

