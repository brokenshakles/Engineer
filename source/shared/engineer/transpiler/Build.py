#!/usr/bin/env python3

import os, sys, subprocess

domain     = sys.argv[1]
module     = sys.argv[2]
type       = sys.argv[3]
transpiler = sys.argv[4]
source     = sys.argv[5]
out_h      = sys.argv[6]
out_c      = sys.argv[7]

if os.path.isfile(os.path.join(source, type + ".h")):

   src_h = open(out_h, "w+")
   subprocess.call([ "m4",
                     "-DDOMAIN_SYMBOL="   + domain,
                     "-DMODULE_SYMBOL="   + module,
                     "-DTYPE_SYMBOL="     + type,
                     "-DTRANSPILER_PATH=" + transpiler,
                     "-DSOURCE_PATH="     + source,
                     os.path.join(transpiler, type + ".h.m4")],
                     stdout = src_h)
   src_h.close()

   src_c = open(out_c,"w+")
   subprocess.call([ "m4",
                     "-DDOMAIN_SYMBOL="   + domain,
                     "-DMODULE_SYMBOL="   + module,
                     "-DTYPE_SYMBOL="     + type,
                     "-DTRANSPILER_PATH=" + transpiler,
                     "-DSOURCE_PATH="     + source,
                     os.path.join(transpiler, type + ".c.m4")],
                     stdout = src_c)
   src_c.close()

