include(TRANSPILER_PATH/Common.m4)dnl
divert(-1)

define(`TYPE_STRUCTS', ``
typedef struct
{
   // Each System is represented in it's Scene with an Entity for notification, and also holds a
   //    reference to its parent scene and to its Eng_Node_System functions.
   Entity           scene;
   Eng_Node_System *system;

   struct
   {
   `divert(-1)'
   SUBSECTION($`'*, `dnl', `dnl', `dnl', `dnl', `dnl', `dnl')

   define(`ASSET',
      struct
      {
         #define TARGET(input) \
         $`'2
         TARGET($`'1)
         #undef TARGET
      }
      $`'1;)
   define(`SYSTEM',  `input##`_'$`'1_System_State *$`'1;')

   `divert(0)dnl'
   ifdef(`SYSTEM_PAYLOAD', `SYSTEM_PAYLOAD', `')`'dnl
   }
   prereq;

   `divert(-1)'
   SUBSECTION(`dnl', $`'*, `dnl', `dnl', `dnl', `dnl', `dnl')

   define(`FIELD',  $`'1 $`'2;)
   define(`QUEUE',  $`'1 $`'2;)

   `divert(0)dnl'
   ifdef(`SYSTEM_PAYLOAD', `SYSTEM_PAYLOAD', `')`'dnl
}
DOMAIN_SYMBOL`_'MODULE_SYMBOL`_System_State';

typedef struct
{
   const Entity identity;

   `divert(-1)'
   SUBSECTION(`dnl', `dnl', `dnl', `dnl', $`'*, `dnl', `dnl')

   define(`ASSET',
   struct
   {
      $`'2 dnl
   }
   $`'1;
   )

   define(`COMPONENT',
   struct
   {
      $`'2 dnl
   }
   $`'1;
   )

   define(`FIELD', $`'1 $`'2;)

   `divert(0)dnl'
   ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl

   `divert(-1)'
   SUBSECTION(`dnl', `dnl', `dnl', `dnl', `dnl', `dnl', $`'*)

   define(`FIELD', $`'1 $`'2;)
   define(`EVENT',  `dnl')

   `divert(0)dnl'
   struct
   {
      ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl
   }
   event;
}
DOMAIN_SYMBOL`_'MODULE_SYMBOL`_Entity_State';
')

divert(0)dnl
HEADER

#ifdef DOMAIN_SYMBOL`_'MODULE_SYMBOL`_DECLARATIONS'

//static void system_attach(System_State *system);
//static void system_detach(System_State *system);

//static void system_upkeep(System_State *system);
//static void system_update(System_State *system);

static void entity_attach(const System_State *system, Entity_State *entity);
static void entity_detach(const System_State *system, Entity_State *entity);

static void entity_upkeep(const System_State *system, Entity_State *entity);
static void entity_update(const System_State *system, Entity_State *entity);

divert(-1)
SUBSECTION(`dnl', `dnl', `dnl', `dnl', `dnl', `dnl', `$*dnl')

define(`FIELD', `dnl')
define(`EVENT',
`static Eina_Bool                           '
`_eng_system_event_$1(System_State *system  '
`                     const void   *notice);'

`#define system_event__$1(state, notice)  \'
`_eng_system_event_$1(state  EINA_UNUSED, \'
`                     notice EINA_UNUSED)  ')
divert(0)dnl
ifdef(`SYSTEM_PAYLOAD', `SYSTEM_PAYLOAD', `')`'dnl

divert(-1)
SUBSECTION(`dnl', `dnl', `dnl', `dnl', `dnl', `dnl', `$*dnl')

define(`FIELD', `dnl')
define(`EVENT',
`static Eina_Bool                           '
`_eng_entity_event_$1(Entity_State *entity, '
`                     const void   *notice);'

`#define entity_event__$1(state, notice)  \'
`_eng_entity_event_$1(state  EINA_UNUSED, \'
`                     notice EINA_UNUSED)  ')

divert(0)dnl
ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl

#endif

