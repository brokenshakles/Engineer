include(TRANSPILER_PATH/Common.m4)dnl
divert(-1)

HEADER

divert(0)dnl
#define Component_State DOMAIN_SYMBOL`_'MODULE_SYMBOL`_State'

#include "Component.h"

static Eina_Inarray *columns;

enum Component_Field
{
   FIELD_NULL,
   define(`FIELD', FIELD_$2``,'')dnl
   ifdef(`ENTITY_PAYLOAD', ENTITY_PAYLOAD, `')dnl
};


/*** Symbol Queries ***/

ENG_PRIVATE String *
_eng_module__get_symbol() { return `"'DOMAIN_SYMBOL`.'MODULE_SYMBOL`.'Component`"'; }

ENG_PRIVATE String *
_eng_module__get_domain() { return `"'DOMAIN_SYMBOL`"'; }

ENG_PRIVATE String *
_eng_module__get_module() { return `"'MODULE_SYMBOL`"'; }

ENG_PRIVATE String *
_eng_module__get_flavor() { return `"'Component`"'; }


/*** Node Interfaces ***/

ENG_PRIVATE void
_eng_node__module_init(Digest *component, Indx (*_component_column)(Digest, Digest) EINA_UNUSED)
{
   columns = eina_inarray_new(sizeof(Digest), 0);

   divert(-1)
   define(`FIELD', $1_INDEX(columns, eina_stringshare_printf("$2"));)

   divert(0)dnl
   ENTITY_PAYLOAD
}

ENG_PRIVATE void
_eng_node__module_free()
{
   eina_inarray_free(columns);
}


/*** Scene Interfaces ***/

ENG_PRIVATE void
_eng_scene__module_init() {}

ENG_PRIVATE void
_eng_scene__module_free() {}


/*** Shard Interfaces ***/

ENG_PRIVATE Eng_Vault *
_eng_shard__module_init()
{
   Eng_Vault *operatives = eng_vault_create();

   divert(-1)
   define(`FIELD', $1_TABLE(operatives, eina_stringshare_printf("$2"));)

   divert(0)dnl
   ENTITY_PAYLOAD

   return operatives;
}

ENG_PRIVATE void
_eng_shard__module_free(Eng_Vault *operatives)
{
   eng_vault_delete(operatives);
}


/*** Public Constants ***/

ENG_PRIVATE Indx
_eng_component__column(Digest field)
{
   for(column = 0; column < eina_inarray_count(columns); column++)
   {
      Digest *current = eina_inarray_nth(columns, column);
      if(current && *current == field) return ++column;
   }
   return VOID;
}

ENG_PRIVATE Indx
_eng_component__size() { return sizeof(Component_State); }


/*** Entity Management ***/

ENG_PRIVATE void
_eng_component__entity_status(Eng_Vault *operatives, Entity target, void *result)
{
   Component_State *state  = result;
   Indx             column = 1;
   Indx             row    = eng_vault_row(operatives, target);

   divert(-1)
   define(`FIELD', $1_FETCH(operatives, &column, row, &state->$2);)

   divert(0)dnl
   ENTITY_PAYLOAD
}

ENG_PRIVATE Indx
_eng_component__entity_attach(Eng_Vault *operatives, Entity target, void *initial)
{
   Component_State *state  = initial;
   Indx             column = 1;
   Indx             row    = eng_vault_row_create(operatives, target);

   if(!row) return VOID;

   divert(-1)
   define(`FIELD', $1_STORE(operatives, &column, row, &state->$2);)

   divert(0)dnl
   ENTITY_PAYLOAD

   return row;
}

ENG_PRIVATE void
_eng_component__entity_detach(Eng_Vault *operatives, Entity target, void *result)
{
   _eng_component__entity_status(operatives, target, result);

   eng_vault_row_delete(operatives, target);
}

