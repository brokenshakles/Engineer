divert(-1)

debugmode(teq)
traceon()
changecom(/*,*/)dnl

define(`ECML', `$*')
define(`ESML', `$*')

define(`HEADER', `include(SOURCE_PATH/TYPE_SYMBOL.h)')
define(`SOURCE', `include(SOURCE_PATH/TYPE_SYMBOL.c)')

define(`UNDOT',    `translit($*, `.',           `_')')
define(`DEGROUP',  `translit($*, `()',           `')')
define(`DELOGIC',  `translit($*, `&|!',          `')')
define(`WHITEOUT', `translit($*, ` ',            `')')
define(`CLEAN',    `DEGROUP(DELOGIC($*))')

define(`TYPE_STRUCTS', `')

define(`SPLIT',  `define(`DOMAIN_PAYLOAD', `$1') define(`MODULE_PAYLOAD', `$2')')
define(`SYMBOL', `SPLIT(translit($*, `.', `,'))')
define(`SYSTEM', `define(`SYSTEM_PAYLOAD', `$*')')
define(`ENTITY', `define(`ENTITY_PAYLOAD', `$*') TYPE_STRUCTS')

define(`SUBSECTION',
   `define(`PREREQ', `$1') define(`IGNORE', `$3') define(`IMPORT', `$5') define(`EVENTS', `$7')'
   `define(`FIELDS', `$2') define(`TARGET', `$4') define(`EXPORT', `$6')')

divert(0)dnl
