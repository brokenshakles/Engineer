include(TRANSPILER_PATH/Common.m4)dnl
divert(-1)

HEADER

divert(0)dnl
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wshadow"

#define DOMAIN_SYMBOL`_'MODULE_SYMBOL`_DECLARATIONS'
#define System_State DOMAIN_SYMBOL`_'MODULE_SYMBOL`_System_State'
#define Entity_State DOMAIN_SYMBOL`_'MODULE_SYMBOL`_Entity_State'

#include "System.h"

extern THREAD_LOCAL const Entity this;  // See eng_thread.h:27 : _eng_work_this().
//extern THREAD_LOCAL const

SOURCE


/*** Definitions ***/

typedef struct Eng_System_Data Eng_System_Data;
typedef struct Entity_Notice   Entity_Notice;

struct Eng_System_Data
{
   Eina_Hash *entity_events;      // Contains Event enumerations, referenced by Event Digest.
   Eina_Hash *component_ignored;  // Contains enum, referenced by ignored      Component Digest.
   Eina_Hash *component_prereqs;  // Contains enum, referenced by prerequisite Component Digest.
   Eina_Hash *system_prereqs;     // Contains enum, referenced by prerequisite System    Digest.
   Eng_Vault *input_output_info;  // This contains a Component Symbol and *field_info column,
};                                //   indexed by the matching System field io column.

struct Entity_Notice
{
   Entity sender;
   Entity target;
   Digest event;
   Indx   size;
   Byte   payload[];
};


/*** Globals ***/

static Eng_System_Data *locals;

/*** Validity-Checking Enumerations ***/

enum Entity_Event
{
   ENTITY_EVENT_VOID,
   divert(-1)
   SUBSECTION(`dnl', `dnl', `dnl', `dnl', `dnl', `dnl', `$*dnl')

   define(`FIELD',              ``dnl'')
   define(`EVENT', ``ENTITY_EVENT_$1,'')

   divert(0)dnl
   ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl
};

enum Prerequisite_Component
{
   divert(-1)
   SUBSECTION(`dnl', `dnl', `APPEND($*)dnl', `APPEND($*)dnl', `dnl', `dnl', `dnl')

   define(`APPEND', `patsubst(CLEAN($*), `\w+\.\w+', `PREREQ_`'UNDOT(\&)`'_Component,'
   )')

   divert(0)dnl
   ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl
};

enum Prerequisite_System
{
   PREREQ_Entity_Event_System, // We include this by default.
   divert(-1)
   SUBSECTION(`$*dnl', `dnl', `dnl', `dnl', `dnl', `dnl', `dnl')

   define(`ASSET',  `define(`asset',  `$1') $2 undefine(`asset')')
   define(`SYSTEM', `ifelse(``PREREQ_`'asset`'_$1_System``,'''', ``PREREQ_Entity_Event_System'','dnl
                    ``dnl', ``PREREQ_`'asset`'_$1_System``,'''' ')
   divert(0)dnl
   ifdef(`SYSTEM_PAYLOAD', `SYSTEM_PAYLOAD', `')`'dnl
};

enum Input_Field
{
   INPUT_FIELD_NULL,
   divert(-1)
   SUBSECTION(`dnl', `dnl', `dnl', `dnl', `$*dnl', `dnl', `dnl')

   define(`ASSET',     `define(`domain', `$1') $2 undefine(`domain')')
   define(`COMPONENT', `define(`module', `$1') $2 undefine(`module')')
   define(`FIELD',     $1_CHECK(INPUT_FIELD_``''domain``''_`'module`', $2)'')
   divert(0)dnl
   ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD',  `')`'dnl
};

enum Output_Field
{
   OUTPUT_FIELD_NULL,
   divert(-1)
   SUBSECTION(`dnl', `dnl', `dnl', `dnl', `dnl', `$*dnl', `dnl')

   define(`ASSET',     `define(`domain', `$1') $2 undefine(`domain')')
   define(`COMPONENT', `define(`module', `$1') $2 undefine(`module')')
   define(`FIELD',     $1_CHECK(OUTPUT_FIELD_``''domain``''_`'module`', $2)'')

   divert(0)dnl
   ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl
};


/*** Symbol Queries ***/

static String * _eng_module__get_symbol() { return `"'DOMAIN_SYMBOL`.'MODULE_SYMBOL`.'System`"'; }
static String * _eng_module__get_domain() { return `"'DOMAIN_SYMBOL`"'; }
static String * _eng_module__get_module() { return `"'MODULE_SYMBOL`"'; }
static String * _eng_module__get_flavor() { return `"'System`"'; }


/*** Node Interfaces ***/

static Bool
_eng_node__module_init(Digest *system, Indx (*_component_column)(Digest, Digest))
{
   Eng_Node_Component *component EINA_UNUSED;
   Digest              prereq    EINA_UNUSED, event EINA_UNUSED;
   Indx                column    EINA_UNUSED, state EINA_UNUSED, row EINA_UNUSED;
   Eng_System_IO_Flag  flag      EINA_UNUSED;
   Entity_State        prototype EINA_UNUSED;
   String             *symbol    EINA_UNUSED;

   locals = malloc(sizeof(Eng_System_Data));


   // Create an Entity Event table.
   // -----------------------------
   locals->entity_events = eina_hash_int64_new(eng_empty_cb);

   divert(-1)
   SUBSECTION2(`dnl', `dnl', `dnl', `dnl', `dnl', `dnl', `$*dnl')

   define(`FIELD',  `dnl')
   define(`EVENT',
   ``event = eng_hash_digest("$1");''
   ``eina_hash_add(entity_events, &event, (Indx*)$2);'')

   divert(0)dnl
   ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl


   // Create the Ignored_Component table.
   // -----------------------------------
   locals->component_ignored = eina_hash_int64_new(eng_empty_cb);

   divert(-1)
   SUBSECTION(`dnl', `dnl', `APPEND($*)', `dnl', `dnl', `dnl', `dnl')

   define(`APPEND', `ifelse(`CLEAN($*)', `', `dnl', `patsubst(CLEAN($*), `\w+\.\w+',
   `prereq = eng_hash_digest("\&.Component");'
   `eina_hash_set(locals->component_ignored, &prereq, (void*)PREREQ_`'UNDOT(\&)`'_Component);'
   )')dnl')

   divert(0)dnl
   ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl


   // Create the Prerequisite_Component table.
   // ----------------------------------------
   locals->component_prereqs = eina_hash_int64_new(eng_empty_cb);

   divert(-1)
   SUBSECTION(`dnl', `dnl', `dnl', `APPEND($*)', `dnl', `dnl', `dnl')

   define(`APPEND', `ifelse(`CLEAN($*)', `', `dnl', `patsubst(CLEAN($*), `\w+\.\w+',
   `prereq = eng_hash_digest("\&.Component");'
   `eina_hash_set(locals->component_prereqs, &prereq, (void*)PREREQ_`'UNDOT(\&)`'_Component);'
   )')dnl')

   divert(0)dnl
   ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl


   // Create the Prerequisite_System table.
   // -------------------------------------
   locals->system_prereqs = eina_hash_int64_new(eng_empty_cb);

   divert(-1)
   SUBSECTION(`$*dnl', `dnl', `dnl', `dnl', `dnl', `dnl', `dnl')

   define(`ASSET',  `define(`domain', $1) $2 undefine(`domain')')dnl
   define(`SYSTEM',
   `prereq = eng_hash_digest("`'domain`'.$1.System");'
   `if(system != prereq)'
   `   eina_hash_set(locals->system_prereqs, &prereq, (void*)PREREQ_`'domain`'.$1_System);''')

   divert(0)dnl
   prereq = eng_hash_digest("Entity.Event.System");
   if(system != eina_hash_digest("Entity.Graph.System") &&
      system != prereq                                  )
      eina_hash_set(locals->system_prereqs, &prereq, (void*)PREREQ_Entity_Event_System);
   ifdef(`SYSTEM_PAYLOAD', `SYSTEM_PAYLOAD', `')`'dnl


   // Create the Input_Output_Info table.
   // -----------------------------------
   locals->input_output_info = eng_vault_new();
   eng_vault_column_create(locals->input_output_info, "symbol", sizeof(Digest), NULL);
   eng_vault_column_create(locals->input_output_info, "offset", sizeof(Indx),   NULL);

   divert(-1)
   SUBSECTION(`dnl', `dnl', `dnl', `dnl', `$*dnl', `dnl', `dnl')

   define(`ASSET',     `define(`domain', `$1') $2 undefine(`domain')')
   define(`COMPONENT', `define(`module', `$1') $2 undefine(`module')')
   define(`FIELD', ``dnl
   column = _component_column(eng_digest("``''domain``''.`'module`'.Component"), eng_digest($2));
   symbol = eina_stringshare_printf("``''domain``''.`'module`'.$2");
   $1_INOUT(locals->input_output_info, symbol, column);'')

   divert(0)dnl
   ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl

   divert(-1)
   SUBSECTION(`dnl', `dnl', `dnl', `dnl', `dnl', `$*dnl', `dnl')

   define(`ASSET',     `define(`domain', `$1') $2 undefine(`domain')')
   define(`COMPONENT', `define(`module', `$1') $2 undefine(`module')')
   define(`FIELD', ``dnl
   column = _component_column(eng_digest("``''domain``''.`'module`'.Component"), eng_digest($2));
   symbol = eina_stringshare_printf("``''domain``''.`'module`'.$2");
   $1_INOUT(locals->input_output_info, symbol, column);'')

   divert(0)dnl
   ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl

   return EINA_TRUE;
}

static void
_eng_node__module_free()
{
   eng_vault_delete(locals->input_output_info);

   eina_hash_free(locals->system_prereqs);
   eina_hash_free(locals->component_prereqs);
   eina_hash_free(locals->component_ignored);
   eina_hash_free(locals->entity_events);

   free(locals);
}


/*** Scene Interfaces ***/

static void
_eng_scene__module_init()
{
   vault = calloc(sizeof(System_State), 1);
}


static void
_eng_scene__module_free() {}


/*** Shard Interfaces ***/

static Eng_Vault *
_eng_shard__module_init()
{
   Indx credentials  = eina_hash_population(component_ignored);
        credentials += eina_hash_population(component_prereqs);

   Eng_Vault              *operatives = eng_vault_create();
   eng_vault_column_create(operatives, "credentials", credentials,  NULL);

   return operatives;
}

static void
_eng_shard__module_free(Eng_Vault *operatives)
{
   eng_vault_delete(operatives);
}


/*** Public Constants ***/

ENG_PRIVATE Indx
_eng_system__system_sizeof() { return sizeof(System_State); }

ENG_PRIVATE Indx
_eng_system__entity_sizeof() { return sizeof(Entity_State); }


/*** Prerequisite Iterators ***/

static Eina_Hash *
_eng_system__system_prereqs()    { return locals->system_prereqs; }

static Eina_Hash *
_eng_system__component_prereqs() { return locals->component_prereqs; }

static Eina_Hash *
_eng_system__component_ignored() { return locals->component_ignored; }


/*** Target Management ***/

static Bool
_eng_system__entity_is_targeted(Byte *credential)
{
   divert(-1)
   SUBSECTION(`dnl', `dnl', `dnl', `APPEND($*)', `dnl', `dnl', `dnl')

   define(`APPEND', `ifelse(`$*', `', `dnl', `if(patsubst($*, `\w+\.\w+',
   `*(credential + PREREQ_`'UNDOT(\&)`'_Component)'))
   return EINA_TRUE; else')')

   divert(0)dnl
   ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl
   return EINA_FALSE;
}

static Bool
_eng_system__entity_is_ignored(Byte *credential)
{
   divert(-1)
   SUBSECTION(`dnl', `dnl', `APPEND($*)', `dnl', `dnl', `dnl', `dnl')

   define(`APPEND', `ifelse(`$*', `', `dnl', `if(patsubst($*, `\w+\.\w+',
   `*(credential + PREREQ_`'UNDOT(\&)`'_Component)'))
   return EINA_TRUE; else')')

   divert(0)dnl
   ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl
   return EINA_FALSE;
}


/*** State Management ***/

static void
_eng_system__entity_import(Indx length, Entity *target, Entity_State *state,
   Eng_Vault* (*_get_operatives)(Digest))
{
   Eng_Vault *table     EINA_UNUSED;  Indx row    EINA_UNUSED;
   Digest     component EINA_UNUSED;  Indx column EINA_UNUSED;

   divert(-1)
   SUBSECTION(`dnl', `dnl', `dnl', `dnl', `$*dnl', `dnl', `dnl')

   define(`ASSET',     `define(`asset', `$1') $2 undefine(`asset')')
   define(`COMPONENT', `define(`label', `$1') dnl
   component = eng_digest("`'asset`'.label.Component");
   Eng_Vault *`'asset`'_label_component = _get_operatives(component);
                                              $2 undefine(`label')')
   define(`FIELD', ``dnl
   column = INFO_OFFSET;
   table  = input_output_info;
   row    = eng_vault_row(table, eng_digest("``''asset``''.`'label`'.$2"));
   Indx ``''asset``''_`'label`'_$2 = eng_vault_fetch(table, column, row)'')

   divert(0)dnl
   ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl

   for(Indx index = 0; index < length; index++)
   {
      divert(-1)
      SUBSECTION(`dnl', `dnl', `dnl', `dnl', `$*dnl', `dnl', `dnl')

      define(`ASSET',     `define(`asset', `$1') $2 undefine(`asset')')
      define(`COMPONENT', `define(`label', `$1') dnl
      table = ``''asset``''_`'label`'_component;
      row   = eng_vault_row(table, *target);  $2 undefine(`label')')
      define(`FIELD',            ``column = ``''asset``''_`'label`'_$2;
      $1_FETCH(table, &column, row, &state->``''asset``''.`'label`'.$2);'')

      divert(0)dnl
      ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl

      target++;  state++;
   }
}

static void
_eng_system__entity_export(Indx length, Entity *target, Entity_State *state,
    Eng_Vault* (*_get_operatives)(Digest))
{
   Eng_Vault *table     EINA_UNUSED;  Indx row    EINA_UNUSED;
   Digest     component EINA_UNUSED;  Indx column EINA_UNUSED;

   divert(-1)
   SUBSECTION(`dnl', `dnl', `dnl', `dnl', `dnl', `$*dnl', `dnl')

   define(`ASSET',     `define(`asset', `$1') $2 undefine(`asset')')
   define(`COMPONENT', `define(`label', `$1') dnl
   component = eng_digest("`'asset`'.label.Component");
   Eng_Vault *`'asset`'_label_component = _get_operatives(component);
                                              $2 undefine(`label')')
   define(`FIELD', ``dnl
   table  = input_output_info;
   column = INFO_OFFSET;
   row    = eng_vault_row(table, eng_digest("``''asset``''.`'label`'.$2"));
   Indx ``''asset``''_`'label`'_$2 = eng_vault_fetch(table, column, row)'')

   divert(0)dnl
   ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl

   // if(!*target) { target++; state++; continue; }

   for(Indx index = 0; index < length; index++)
   {
      divert(-1)
      SUBSECTION(`dnl', `dnl', `dnl', `dnl', `dnl', `$*dnl', `dnl')

      define(`ASSET',     `define(`asset', `$1') $2 undefine(`asset')')
      define(`COMPONENT', `define(`label', `$1') dnl
      table = ``''asset``''_`'label`'_component;
      row   = eng_vault_row(table, *target);  $2 undefine(`label')')
      define(`FIELD',           ``column = ``''asset``''_`'label`'_$2;
      $1_STORE(table, column, row, &state->``''asset``''.`'label`'.$2);'')

      divert(0)dnl
      ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl

      target++;  state++;
   }
}


/*** Entity Operations and Event Calls ***/

static void
_eng_system__entity_attach(Entity target, Entity_State *state, Entity *work)
{
   Entity first = this, *that = &this;  *that = target;  *work = target;
   entity_attach(NULL, state);          *that = first;   *work = first;
}

static void
_eng_system__entity_detach(Entity target, Entity_State *state, Entity *work)
{
   Entity first = this, *that = &this;  *that = target;  *work = target;
   entity_detach(NULL, state);          *that = first;   *work = first;
}

static void
_eng_system__entity_upkeep(Indx length, Entity *target, Entity_State *state, Entity *work)
{
   Entity first = this, *that = &this;

   for(Indx index = 0; index < length; index++)
      { *that = *target++; *work = *that; entity_upkeep(state++); }
   *that = first;
   *work = first;
}

static void
_eng_system__entity_notify(Indx length, Entity *target, Entity_State *state, Entity *work)
{
   Entity first = this, *that = &this;

   for(Indx index = 0; index < length; index++)
   {
      *that = *target++;
      *work = *that;

      Eina_Inarray **outbox = eng_component_status(this, "Entity.Event");
      if(outbox)
      {
         Byte *root = eina_inarray_nth(*outbox, 0), *head = root;
         if(root) while((head - root) < eina_inarray_count(*outbox))
         {
            const Entity_Notice *notice = head;

            const Indx keyhole = (Indx)eina_hash_find(entity_events, &notice->event);
            switch(keyhole)
            {
               case ENTITY_EVENT_VOID : break;  // Nothing Happens!
               divert(-1)
               SUBSECTION(`dnl', `dnl', `dnl', `dnl', `dnl', `dnl', `$*')

               define(`FIELD', `dnl')
               define(`EVENT',
               case ENTITY_EVENT_$1_S$2 : entity_event_$1_S$2(state++, notice);  break;)

               divert(0)dnl
               ifdef(`ENTITY_PAYLOAD', `ENTITY_PAYLOAD', `')`'dnl
            }

            // Move the head to the next Entity_Notice.
            head += PAGE_ALIGN(sizeof(Entity_Notice) + notice->size);
         }
      }
   }
   *that = first;
   *work = first;
}

static void
_eng_system__entity_update(Indx length, Entity *target, Entity_State *state, Entity *work)
{
   Entity first = this, *that = &this;

   for(Indx index = 0; index < length; index++)
      { *that = *target++; *work = *that; entity_update(state++); }
   *that = first;
   *work = first;
}

pragma GCC diagnostic pop

