#ifndef _ENG_CONSOLE_EO_H_
#define _ENG_CONSOLE_EO_H_

#ifndef _ENG_CONSOLE_EO_CLASS_TYPE
#define _ENG_CONSOLE_EO_CLASS_TYPE

typedef Eo Eng_Console;

#endif

#ifndef _ENG_CONSOLE_EO_TYPES
#define _ENG_CONSOLE_EO_TYPES


#endif
#define ENG_CONSOLE_CLASS eng_console_class_get()

EAPI EAPI_WEAK const Efl_Class *eng_console_class_get(void) EINA_CONST;

#endif
