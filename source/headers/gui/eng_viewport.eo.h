#ifndef _ENG_VIEWPORT_EO_H_
#define _ENG_VIEWPORT_EO_H_

#ifndef _ENG_VIEWPORT_EO_CLASS_TYPE
#define _ENG_VIEWPORT_EO_CLASS_TYPE

typedef Eo Eng_Viewport;

#endif

#ifndef _ENG_VIEWPORT_EO_TYPES
#define _ENG_VIEWPORT_EO_TYPES


#endif
#define ENG_VIEWPORT_CLASS eng_viewport_class_get()

EAPI EAPI_WEAK const Efl_Class *eng_viewport_class_get(void) EINA_CONST;

EAPI EAPI_WEAK Entity eng_viewport_camera_get(const Eo *obj);

EAPI EAPI_WEAK Entity eng_viewport_look(Eo *obj, Entity new_camera);

#endif
