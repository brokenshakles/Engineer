#ifndef _ENG_TYPE_PNTR_H_
#define _ENG_TYPE_PNTR_H_

#define TYPE Pntr
#define BASE
#define ROOT

#include "../tool/vault.h"

typedef void*         Pntr;
typedef Eina_Inarray* Pntr_SOA;

inline void
Pntr_VAULT(Eng_Vault **vault, String *symbol)
{
   eng_vault_column_create(*vault, symbol, sizeof(Pntr));
}

inline void
Pntr_STORE(Eng_Vault **vault, Entity target, Indx column, Pntr *field)
{
   eng_vault_store(*vault, target, column, field);
}

inline void
Pntr_FETCH(Eng_Vault **vault, Entity target, Indx column, Pntr *field)
{
   *field = *(Pntr*)eng_vault_fetch(*vault, target, column);
}

#endif

