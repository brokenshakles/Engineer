#ifndef _ENG_TYPE_SPHERE_H_
#define _ENG_TYPE_SPHERE_H_

#include "type/sclr.h"
#include "type/vec3.h"

#define COMPOUND Sphere
#define SUBTYPES FIELD(Vec3, position) \
                 FIELD(Sclr, radius)   \
                 FIELD(Sclr, hollow)   \

#include "type.h"

#endif

