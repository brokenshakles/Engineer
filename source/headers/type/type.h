#ifndef _ENG_TYPE_H_
#define _ENG_TYPE_H_

#include "../tool/vault.h"

enum Column_Info
{
   INFO_SYMBOL = 1, // Contains the String * used to generate the digest for a given field.
   INFO_OFFSET = 2, // Contains the leftmost vault column offset of a given Component field symbol.
};

#endif

// This is a set of macros for tying together everything in the ../type/ dir and the transpiler.

#ifdef TYPE
#ifdef BASE
#ifdef ROOT

#ifdef SIGN
   #undef  SIGN
   #define SIGN
#else
   #define SIGN u
#endif

#define DEFINE(type, base, root, sign) DEFINE_(type, base, root, sign)
#define DEFINE_(type, base, root, sign)                                     \
   typedef sign##int##base##_t type;                                        \
                                                                            \
   enum type##_META                                                         \
   {                                                                        \
      type##_SCALE = sizeof(type) * 8,                                      \
      type##_RADIX = root,                                                  \
      type##_BASIS = (type)1 << root,                                       \
      type##_SIZE  = 0x7fffffffffffffff                                     \
   };                                                                       \
                                                                            \
   inline void                                                              \
   type##_INDEX(Eina_Inarray *columns, String *symbol)                      \
   {                                                                        \
      Digest column = eng_digest(symbol);                                   \
      eina_stringshare_del(symbol);                                         \
      eina_inarray_push(columns, &column);                                  \
   }                                                                        \
                                                                            \
   inline void                                                              \
   type##_STORE(Eng_Vault *operatives, Indx *column, Indx row, type *field) \
   {                                                                        \
      eng_vault_store(operatives, *column, row, field);                     \
      *column += 1;                                                         \
   }                                                                        \
                                                                            \
   inline void                                                              \
   type##_FETCH(Eng_Vault *operatives, Indx *column, Indx row, type *field) \
   {                                                                        \
      *field = *(type*)eng_vault_fetch(operatives, *column, row);           \
      *column += 1;                                                         \
   }                                                                        \
                                                                            \
   inline void                                                              \
   type##_TABLE(Eng_Vault *operatives, String *symbol)                      \
   {                                                                        \
      eng_vault_column_create(operatives, symbol, sizeof(type), NULL);      \
      eina_stringshare_del(symbol);                                         \
   }                                                                        \
                                                                            \
   inline void                                                              \
   type##_INOUT(Eng_Vault *info, String *symbol, Indx *column)              \
   {                                                                        \
      Indx row = eng_vault_row_create(info, eng_digest(symbol));            \
      eng_vault_store(info, INFO_SYMBOL, row, &symbol);                     \
      eng_vault_store(info, INFO_OFFSET, row,  column);                     \
      eina_stringshare_del(symbol);                                         \
      *column += 1;                                                         \
   }
   DEFINE(TYPE, BASE, ROOT, SIGN)
#undef DEFINE_
#undef DEFINE

#ifdef SIGN
   #undef SIGN
#endif

//#ifndef SWITCH
   #undef TYPE
   #undef BASE
   #undef ROOT
//#endif

#endif
#endif
#endif


#ifdef COMPOUND
#ifdef SUBTYPES

#define APPEND eina_stringshare_printf

#define FIELD(type, field) type field;
#define DEFINE(type, subtypes) \
   typedef struct              \
   {                           \
      subtypes                 \
   }                           \
   type;
   DEFINE(COMPOUND, SUBTYPES)
#undef DEFINE
#undef FIELD

#define SYMBOL(type) inline void type##_INDEX
#define FIELD(type, subfield)    type##_INDEX(columns, APPEND("%s.##subfield##", symbol));
#define DEFINE(type, subtypes)                         \
   SYMBOL(type)(Eina_Inarray *columns, String *symbol) \
   {                                                   \
      subtypes                                         \
                                                       \
      eina_stringshare_del(symbol);                    \
   }
   DEFINE(COMPOUND, SUBTYPES)
#undef DEFINE
#undef FIELD
#undef SYMBOL

#define SYMBOL(type) inline void type##_STORE
#define FIELD(type, subfield)    type##_STORE(operatives, column, row, &field->subfield);
#define DEFINE(type, subtypes)                                              \
   SYMBOL(type)(Eng_Vault *operatives, Indx *column, Indx row, type *field) \
   {                                                                        \
      subtypes                                                              \
   }
   DEFINE(COMPOUND, SUBTYPES)
#undef DEFINE
#undef FIELD
#undef SYMBOL

#define SYMBOL(type) inline void type##_FETCH
#define FIELD(type, subfield)    type##_FETCH(operatives, column, row, &field->subfield);
#define DEFINE(type, subtypes)                                              \
   SYMBOL(type)(Eng_Vault *operatives, Indx *column, Indx row, type *field) \
   {                                                                        \
      subtypes                                                              \
   }
   DEFINE(COMPOUND, SUBTYPES)
#undef DEFINE
#undef FIELD
#undef SYMBOL

#define SYMBOL(type) inline void type##_TABLE
#define FIELD(type, subfield)    type##_TABLE(operatives, APPEND("%s.##subfield##", symbol));
#define DEFINE(type, subtypes)                         \
   SYMBOL(type)(Eng_Vault *operatives, String *symbol) \
   {                                                   \
      subtypes                                         \
                                                       \
      eina_stringshare_del(symbol);                    \
   }
   DEFINE(COMPOUND, SUBTYPES)
#undef DEFINE
#undef FIELD
#undef SYMBOL

#define SYMBOL(type) inline void type##_INOUT
#define FIELD(type, subfield)    type##_INOUT(info, APPEND("%s.##subfield##", symbol), column);
#define DEFINE(type, subtypes)                                   \
   SYMBOL(type)(Eng_Vault *info, String *symbol, Indx *column)   \
   {                                                             \
      Indx row = eng_vault_row_create(info, eng_digest(symbol)); \
      eng_vault_store(info, INFO_SYMBOL, row, &symbol);          \
      eng_vault_store(info, INFO_OFFSET, row,  column);          \
                                                                 \
      subtypes                                                   \
                                                                 \
      eina_stringshare_del(symbol);                              \
   }
   DEFINE(COMPOUND, SUBTYPES)
#undef DEFINE
#undef FIELD
#undef SYMBOL

#undef APPEND

//#ifndef SWITCH
   #undef SUBTYPES
   #undef COMPOUND
//#endif

#endif
#endif

