#ifndef _ENG_TYPE_EULR_H_
#define _ENG_TYPE_EULR_H_

#include "type/angl.h"

#define COMPOUND Eulr
#define SUBTYPES FIELD(Angl, yaw)   \
                 FIELD(Angl, pitch) \
                 FIELD(Angl, roll)  \

#include "type.h"

#endif

