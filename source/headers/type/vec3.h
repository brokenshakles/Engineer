#ifndef _ENG_TYPE_VEC3_H_
#define _ENG_TYPE_VEC3_H_

#include "type/vctr.h"

#define COMPOUND Vec3
#define SUBTYPES FIELD(Vctr, x) \
                 FIELD(Vctr, y) \
                 FIELD(Vctr, z) \

#include "type.h"

#endif

