#ifndef _ENG_TYPE_QUAT_H_
#define _ENG_TYPE_QUAT_H_

#include "type/angl.h"
#include "type/vctr.h"

#define COMPOUND Quat
#define SUBTYPES FIELD(Angl, w) \
                 FIELD(Vctr, x) \
                 FIELD(Vctr, y) \
                 FIELD(Vctr, z) \

#include "type.h"

#endif

