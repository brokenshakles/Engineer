#ifndef _ENG_TYPE_COLOR_H_
#define _ENG_TYPE_COLOR_H_

#include "type/byte.h"

#define COMPOUND Color
#define SUBTYPES FIELD(Byte, red)   \
                 FIELD(Byte, blue)  \
                 FIELD(Byte, green) \
                 FIELD(Byte, alpha) \

#include "type.h"

#endif

