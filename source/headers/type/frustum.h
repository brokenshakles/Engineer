#ifndef _ENG_TYPE_FRUSTUM_H_
#define _ENG_TYPE_FRUSTUM_H_

#include "type/vec3.h"

#define COMPOUND Frustum
#define SUBTYPES FIELD(Vec3, position) \
                 FIELD(Vec3, bounds)   \

#include "type.h"

#endif

