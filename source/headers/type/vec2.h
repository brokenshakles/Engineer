#ifndef _ENG_TYPE_VEC2_H_
#define _ENG_TYPE_VEC2_H_

#include "type/vctr.h"

#define COMPOUND Vec2
#define SUBTYPES FIELD(Vctr, x) \
                 FIELD(Vctr, y) \

#include "type.h"

#endif

