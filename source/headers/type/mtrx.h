#ifndef _ENG_TYPE_MTRX_H_
#define _ENG_TYPE_MTRX_H_

#include "type/vctr.h"

#define COMPOUND Mtrx
#define SUBTYPES FIELD(Vctr, r0c0) \
                 FIELD(Vctr, r0c1) \
                 FIELD(Vctr, r0c2) \
                 FIELD(Vctr, r0c3) \
                 FIELD(Vctr, r1c0) \
                 FIELD(Vctr, r1c1) \
                 FIELD(Vctr, r1c2) \
                 FIELD(Vctr, r1c3) \
                 FIELD(Vctr, r2c0) \
                 FIELD(Vctr, r2c1) \
                 FIELD(Vctr, r2c2) \
                 FIELD(Vctr, r2c3) \
                 FIELD(Vctr, r3c0) \
                 FIELD(Vctr, r3c1) \
                 FIELD(Vctr, r3c2) \
                 FIELD(Vctr, r3c3) \

#include "type.h"

#endif

