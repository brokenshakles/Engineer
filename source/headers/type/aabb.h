#ifndef _ENG_TYPE_AABB_H_
#define _ENG_TYPE_AABB_H_

#include "type/vec3.h"

#define COMPOUND AABB
#define SUBTYPES FIELD(Vec3, position) \
                 FIELD(Vec3, bounds)   \

#include "type.h"

#endif

