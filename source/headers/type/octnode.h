#ifndef _ENG_TYPE_OCTNODE_H_
#define _ENG_TYPE_OCTNODE_H_

#include "type/byte.h"
#include "type/indx.h"

#define COMPOUND Oct_Node
#define SUBTYPES FIELD(Indx, parent)    \
                 FIELD(Byte, firstborn) \
                 FIELD(Byte, childmask) \
                                        \
                 FIELD(Byte, depth)  \
                 FIELD(Indx, upper)  \
                 FIELD(Indx, middle) \
                 FIELD(Indx, lower)  \

#include "type.h"

#endif

