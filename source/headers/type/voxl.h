#ifndef _ENG_TYPE_VOXL_H_
#define _ENG_TYPE_VOXL_H_

#include "type/color.h"

#define COMPOUND Voxl
#define SUBTYPES FIELD(Color, material) \
                 FIELD(Color, light)    \

#include "type.h"

#endif


