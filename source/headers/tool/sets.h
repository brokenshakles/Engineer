#ifndef _ENG_SETS_H_
#define _ENG_SETS_H_

#include "../engineer.h"

#define ENG_SET_FOREACH_BREAK { page = (Indx)-2; break; }

#define ENG_SET_FOREACH(set, target)                                                         \
   for(Indx page = 1;  (target = eina_inarray_nth(*(Eina_Inarray**)set, page));  page++)     \
      for(target = memcpy(eina_inarray_nth(*(void**)set, VOID), target, sizeof(target) * 2); \
         *target <= *(target + 1); (*target)++)                                              \

#define ENG_SET_REVERSE_FOREACH(set, target)                                                 \
   for(Indx page   = eina_inarray_count(*(void**)set);                                       \
           (target = eina_inarray_nth(*(void**)set, page)); page--)                          \
      for(target = memcpy(eina_inarray_nth(*(void**)set, VOID), target, sizeof(target) * 2); \
         *target <= *(target + 1); (*(target + 1))--)                                        \

typedef struct _Set Set;

// The Set type == bit-width of its elements. Positive for unsigned, negative for signed.
ENG_API Set  * eng_set_create(int8_t type, Indx modulus, Indx offset);
ENG_API void   eng_set_delete(Set *set);

ENG_API Bool   eng_set_attach_interval(Set *set, void *lower, void *upper);
ENG_API Bool   eng_set_detach_interval(Set *set, void *lower, void *upper);
ENG_API Indx   eng_set_has_interval(Set *set, void *lower, void *upper);

ENG_API Bool   eng_set_attach(Set *set, void *element);
ENG_API Bool   eng_set_detach(Set *set, void *element);
ENG_API Indx   eng_set_has_element(Set *set, void *element);

ENG_API Sclr   eng_set_find_size(Set *set);
ENG_API void * eng_set_find_smallest(Set *set);
ENG_API void * eng_set_find_largest(Set *set);

ENG_API Set * eng_set_union(Set *a, Set *b);         // Returns a set with every element of both sets.
//ENG_API Set * eng_set_intersection(Set *a, Set *b);  // Returns a set with every element shared by both sets.
//ENG_API Set * eng_set_difference(Set *a, Set *b);    // Returns a set with every element not shared by both sets.
//ENG_API Set * eng_set_complement(Set *set);          // Returns a set with every element not in the set.

#endif
