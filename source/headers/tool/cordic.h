#ifndef _ENG_CORDIC_H_
#define _ENG_CORDIC_H_

#include "type/sclr.h"
#include "type/vctr.h"
#include "type/angl.h"

// Define this to perform all multiplications and divisions with the linear CORDIC method.
// Protip: DO NOT DEFINE THIS.  It does NOT output correct values.
//#define CORDIC_LINEAR    1

void cordic_init();

void cordic_linear_init();
void cordic_linear_ymode(Vctr *x, Vctr *y, Vctr *z);
void cordic_linear_zmode(Vctr *x, Vctr *y, Vctr *z);

void cordic_circular_init();
void cordic_circular_ymode(Vctr *x, Vctr *y, Vctr *z);
void cordic_circular_aymode(Vctr *x, Vctr *y, Vctr *z);
void cordic_circular_zmode(Vctr *x, Vctr *y, Vctr *z);

void cordic_hyperbolic_init();
void cordic_hyperbolic_ymode(Vctr *x, Vctr *y, Vctr *z);
void cordic_hyperbolic_zmode(Vctr *x, Vctr *y, Vctr *z);

#endif

