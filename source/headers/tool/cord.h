#ifndef _ENG_CORD_H_
#define _ENG_CORD_H_

#include "../engineer.h"

typedef struct Cord Cord;

ENG_API Cord * cord_spawn(void (*main)(void *), void *payload);
ENG_API void * cord_query(Cord *cord);     // This will return the given child Cord's current payload.
ENG_API void * cord_await(Cord *cord, void *payload);
ENG_API void * cord_yield(void *payload);
ENG_API void * cord_annul(Cord *cord);     // Immediately terminates a child Cord without continuing.
ENG_API void   cord_abort(void *payload);  // This is called by a child Cord to make its last _yield().

#endif

// Cords are a coroutine/continuation implementaion for generator functions, sequential pipelines,
//   state machines, and other uses in C. Significant conceptual contribution & implementation
//   provided by the good fellows at #musl. See readme text at the bottom of cord.c
