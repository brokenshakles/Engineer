#ifndef _ENG_VAULT_H_
#define _ENG_VAULT_H_

#include <Eina.h>

typedef Eina_Bool Bool;
typedef uint32_t  Indx;

typedef  int64_t Thread;
typedef uint64_t Entity;

typedef const char String;
typedef uint64_t   Digest;

typedef struct Eng_Vault Eng_Vault;

Digest eng_digest(String *key);

Eng_Vault * eng_vault_create();
void        eng_vault_delete(Eng_Vault *vault);

Indx eng_vault_width(Eng_Vault *vault);
Indx eng_vault_length(Eng_Vault *vault);

Indx     eng_vault_column(Eng_Vault *vault, String *field);
Indx     eng_vault_column_create(Eng_Vault *vault, String *field, Indx size, void *initial);
Bool     eng_vault_column_delete(Eng_Vault *vault, Indx column);
String * eng_vault_column_symbol(Eng_Vault *vault, Indx column);

Indx eng_vault_row(Eng_Vault *vault, Entity target);
Indx eng_vault_row_create(Eng_Vault *vault, Entity target);
Bool eng_vault_row_delete(Eng_Vault *vault, Entity target);

Indx   eng_vault_gauge(Eng_Vault *vault, Indx column);
Bool   eng_vault_store(Eng_Vault *vault, Indx column, Indx row, void *data);
void * eng_vault_fetch(Eng_Vault *vault, Indx column, Indx row);

#endif

