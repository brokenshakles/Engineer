#ifndef ENGINEER_H_
#define ENGINEER_H_

#ifdef EAPI
# undef EAPI
#endif

#ifdef _WIN32
# ifdef EFL_ENGINEER_BUILD
#  ifdef DLL_EXPORT
#   define EAPI __declspec(dllexport)
#  else
#   define EAPI
#  endif /* ! DLL_EXPORT */
# else
#  define EAPI __declspec(dllimport)
# endif /* ! EFL_ENGINEER_BUILD */
#else
# ifdef __GNUC__
#  if __GNUC__ >= 4
#   define EAPI __attribute__ ((visibility("default")))
#  else
#   define EAPI
#  endif
# else
#  define EAPI
# endif
#endif /* ! _WIN32 */

#ifdef __cplusplus
extern "C" {
#endif

#define EFL_EO_API_SUPPORT   1
#define EFL_BETA_API_SUPPORT 1
#define ENG_ECS_API_SUPPORT  1

#define PROJECT Engineer

#define ENG_API EAPI

#include <stdlib.h>
#include <stdatomic.h>
//#include <stdio.h>
//#include <string.h>
#include <math.h>

#include <Eina.h>
#include <Ecore_Getopt.h>
#include <Elementary.h>

#include "math/aabb.h"
#include "math/angl.h"
#include "math/byte.h"
#include "math/color.h"
#include "math/eulr.h"
#include "math/frustum.h"
#include "math/half.h"
#include "math/indx.h"
#include "math/mtrx.h"
#include "math/octnode.h"
//#include "math/pntr.h"
#include "math/quat.h"
#include "math/sclr.h"
#include "math/sphere.h"
#include "math/vctr.h"
#include "math/vec2.h"
#include "math/vec3.h"
#include "math/voxl.h"

#include "tool/cord.h"
//#include "tool/cordic.h"
#include "tool/sets.h"
#include "tool/vault.h"

//  File "eng_thread.h"    intentionally not included.
//  File "eng_entity.h"    is not a thing, don't go looking for it.
//  File "eng_component.h" intentionally not included.
//  File "eng_system.h"    intentionally not included.

#include "gui/eng_console.h"
#include "gui/eng_viewport.h"

typedef enum Eng_Scene_Phase  Eng_Scene_Phase;
typedef enum Eng_System_Phase Eng_System_Phase;

enum Eng_Scene_Phase
{
   SCENE_SLEEP,

   SCENE_UPKEEP, // Component and System Shard internal data gets sorted here.
   SCENE_NOTIFY,
   SCENE_UPDATE,

   SCENE_RENDER_PHASE,
   SCENE_ASYNC_PHASE,

   SCENE_CLEANUP,
};

enum Eng_System_Phase
{
   SYSTEM_SLEEP,

   SYSTEM_ACQUIRE_READ,
   SYSTEM_IMPORT,
   SYSTEM_RELEASE_READ,

   SYSTEM_UPKEEP,
   SYSTEM_NOTIFY,
   SYSTEM_UPDATE,

   SYSTEM_ACQUIRE_WRITE,
   SYSTEM_EXPORT,
   SYSTEM_RELEASE_WRITE,
};


// Global Library Methods
EAPI Thread eng_init(const Efl_Event *event, void (*efl_opts)(int argc, char **argv));
EAPI int    eng_halt();
EAPI void   eng_run();

// Scene Management API
ENG_API Thread eng_scene_create(const char *name);
ENG_API void   eng_scene_delete(Thread scene);

// Entity API
ENG_API Entity eng_entity_create(Thread scene);
ENG_API void   eng_entity_delete(Entity target);

ENG_API Thread eng_entity_scene(Entity target);
ENG_API Thread eng_entity_shard(Entity target);

// Component API
ENG_API Bool   eng_component_create(Entity parent, String *component, void *initial);
ENG_API void * eng_component_delete(Entity target, String *component);
//ENG_API void * eng_component_status(Entity target, String *component);

// System Module & Domain Loading API
ENG_API Bool eng_system_load(Thread scene, String *system);
ENG_API Bool eng_system_free(Thread scene, String *system);

ENG_API Bool eng_domain_load(Thread scene, String *domain);
ENG_API Bool eng_domain_free(Thread scene, String *domain);

#ifdef __cplusplus
}
#endif

#endif /* ENGINEER_H_ */
