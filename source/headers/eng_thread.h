#ifndef _ENG_THREAD_H_
#define _ENG_THREAD_H_

#include <Ecore_Getopt.h>
#include <Eina.h>

#include "tool/sets.h"
#include "tool/cord.h"

#define ENG_API EAPI

// Work Flow Control API
ENG_API void   eng_work_yield(); // Suspend execution so that Work of a higher rank may run.

// Syncronous Work API
ENG_API void * eng_work_call(Entity target, void *method, Indx args_size, Indx result_size);
ENG_API void * eng_work_echo(Set   *target, void *method, Indx args_size, Indx result_size);
ENG_API void * eng_work_wait(void  *call);

// Asyncronous Work API
ENG_API void * eng_work_task(Entity target, void *method, Indx args_size, Bool make_busy);
ENG_API void   eng_work_pass(void  *task);

#endif

