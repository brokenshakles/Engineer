#ifndef _ENG_SYSTEM_H_
#define _ENG_SYSTEM_H_
#define _ENG_COMPONENT_H_

#include "engineer.h"

// Engineer System Markup Language Macro Stub.
#define ESML(markup) // Engineer System Markup Language Macro Stub.

// System Internal Invocation API Stubs
#define entity_upkeep(state)           _eng_system__entity_operate__upkeep(state EINA_UNUSED)
#define entity_update(state)           _eng_system__entity_operate__update(state EINA_UNUSED)
#define entity_start(state)            _eng_system__entity_event__start(state EINA_UNUSED)
#define entity_stop(state)             _eng_system__entity_event__stop(state EINA_UNUSED)
#define entity_invoke(symbol, payload) _eng_system__entity_event__##symbol(entity, payload)

typedef struct System_State System_State;

extern const Entity this;

#endif
