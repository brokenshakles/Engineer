#ifndef _ENG_MATH_BYTE_H_
#define _ENG_MATH_BYTE_H_

#include "type/byte.h"

inline Byte
byte(double input)
{
   return (Byte)(input * Byte_BASIS);
}

#endif
