#ifndef _ENG_MATH_SPHERE_H_
#define _ENG_MATH_SPHERE_H_

#include "type/sphere.h"

inline Sphere *
sphere(double x, double y, double z, double radius, double hollow)
{
   Sphere *output = malloc(sizeof(Sphere));

   output->position.x = (Vctr)(x * Sclr_BASIS);
   output->position.y = (Vctr)(y * Sclr_BASIS);
   output->position.z = (Vctr)(z * Sclr_BASIS);

   output->radius = (Sclr)(radius * Sclr_BASIS);
   output->hollow = (Sclr)(hollow * Sclr_BASIS);

   return output;
}

#endif

