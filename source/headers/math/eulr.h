#ifndef _ENG_MATH_EULR_H_
#define _ENG_MATH_EULR_H_

#include "type/eulr.h"

#include "math/sclr.h"
#include "math/angl.h"
#include "math/quat.h"

inline Eulr
eulr(double yaw, double pitch, double roll)
{
   Eulr output;

   output.yaw   = (Angl)(yaw   * Angl_BASIS);
   output.pitch = (Angl)(pitch * Angl_BASIS);
   output.roll  = (Angl)(roll  * Angl_BASIS);

   return output;
}

inline Quat
eulr_to_quat(Eulr input)
{
   Sclr cosr, cosp, cosy, sinr, sinp, siny, cycr, sysr;
   Quat output;

   cosy = sclr_mult(COS(input.yaw), Sclr_BASIS/2);
   siny = sclr_mult(SIN(input.yaw), Sclr_BASIS/2);

   cosp = sclr_mult(COS(input.pitch), Sclr_BASIS/2);
   sinp = sclr_mult(SIN(input.pitch), Sclr_BASIS/2);

   cosr = sclr_mult(COS(input.roll), Sclr_BASIS/2);
   sinr = sclr_mult(SIN(input.roll), Sclr_BASIS/2);

   cycr = sclr_mult(cosy, cosr);
   sysr = sclr_mult(siny, sinr);

   output.w = sclr_mult(cosp, cycr) + sclr_mult(sinp, sysr);
   output.x = sclr_mult(sinp, cycr) - sclr_mult(cosp, sysr);
   output.y = sclr_mult(sclr_mult(sinp, sinr), cosy) + sclr_mult(sclr_mult(cosp, cosr), siny);
   output.z = sclr_mult(sclr_mult(sinp, cosr), siny) - sclr_mult(sclr_mult(cosp, sinr), cosy);

   return quat_normalize(output);
}

#endif

