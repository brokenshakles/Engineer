#ifndef _ENG_MATH_FRUSTUM_H_
#define _ENG_MATH_FRUSTUM_H_

#include "type/frustum.h"

inline Frustum *
frustum(double x, double y, double z)
{
   Frustum *output = malloc(sizeof(Frustum));

   output->position.x = (Vctr)(x * Vctr_BASIS);
   output->position.y = (Vctr)(y * Vctr_BASIS);
   output->position.z = (Vctr)(z * Vctr_BASIS);

   return output;
}

#endif

