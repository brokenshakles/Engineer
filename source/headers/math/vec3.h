#ifndef _ENG_MATH_VEC3_H_
#define _ENG_MATH_VEC3_H_

#include "type/vec3.h"

#include "math/vctr.h"

inline Vec3
vec3(double x, double y, double z)
{
   Vec3 output;

   output.x = (Vctr)(x * Vctr_BASIS);
   output.y = (Vctr)(y * Vctr_BASIS);
   output.z = (Vctr)(z * Vctr_BASIS);

   return output;
}

inline Vec3
vec3_mult(Vec3 input_a, Vec3 input_b)
{
   Vec3 output;

   output.x = vctr_mult(input_a.x, input_b.x);
   output.y = vctr_mult(input_a.y, input_b.y);
   output.z = vctr_mult(input_a.z, input_b.z);

   return output;
}

inline Vec3
vec3_divd(Vec3 input_a, Vec3 input_b)
{
   Vec3 output;

   output.x = vctr_divd(input_a.x, input_b.x);
   output.y = vctr_divd(input_a.y, input_b.y);
   output.z = vctr_divd(input_a.z, input_b.z);

   return output;
}

inline Sclr
vec3_dot(Vec3 input_a, Vec3 input_b)
{
   Sclr output;

   output = (Sclr)vctr_mult(input_a.x, input_b.x)
          + (Sclr)vctr_mult(input_a.y, input_b.y)
          + (Sclr)vctr_mult(input_a.z, input_b.z);

   return output;
}

inline Vec3
vec3_cross(Vec3 input_a, Vec3 input_b)
{
   Vec3 output;

   output.x = vctr_mult(input_a.y, input_b.z) - vctr_mult(input_a.z, input_b.y);
   output.y = vctr_mult(input_a.z, input_b.x) - vctr_mult(input_a.x, input_b.z);
   output.z = vctr_mult(input_a.x, input_b.y) - vctr_mult(input_a.y, input_b.x);

   return output;
}

inline Sclr
vec3_length(Vec3 input)
{
   Sclr output;

   output = sclr_sqrt(vec3_dot(input, input));

   return output;
}

inline Vec3
vec3_scale(Vec3 input, Sclr factor)
{
   Vec3 output;

   output.x = vctr_mult(input.x, factor);
   output.y = vctr_mult(input.y, factor);
   output.z = vctr_mult(input.z, factor);

   return output;
}

inline Vec3
vec3_normalize(Vec3 input)
{
   Vec3 output;
   Vctr basis, length, inverse;

   basis    = Vctr_BASIS;
   length   = vec3_length(input);
   inverse  = vctr_divd(basis, length);
   output   = vec3_scale(input, inverse);

   return output;
}

inline Vec3
vec3_invert(Vec3 input)
{
   Vec3 output;

   output.x = -input.x;
   output.y = -input.y;
   output.z = -input.z;

   return output;
}

#endif
