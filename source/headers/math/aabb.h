#ifndef _ENG_MATH_AABB_H_
#define _ENG_MATH_AABB_H_

#include "type/aabb.h"

#include "type/sclr.h"
#include "type/sphere.h"

#include "math/vec3.h"

//Fixme: not implemented, copied from elsewhere.
inline AABB *
aabb(double x, double y, double z)
{
   AABB *output = malloc(sizeof(AABB));

   output->position.x = (Vctr)(x * Vctr_BASIS);
   output->position.y = (Vctr)(y * Vctr_BASIS);
   output->position.z = (Vctr)(z * Vctr_BASIS);

   return output;
}

inline Sclr
aabb_perimeter(AABB input)
{
   return 2 * (vctr_mult(input.bounds.x, input.bounds.x)
             + vctr_mult(input.bounds.y, input.bounds.y)
             + vctr_mult(input.bounds.z, input.bounds.z));
}

inline AABB
aabb_union(AABB a, AABB b)
{
   AABB output;

   Vec3
      amax = {a.position.x + a.bounds.x, a.position.y + a.bounds.y, a.position.z + a.bounds.z},
      amin = {a.position.x - a.bounds.x, a.position.y - a.bounds.y, a.position.z - a.bounds.z},
      bmax = {b.position.x + b.bounds.x, b.position.y + b.bounds.y, b.position.z + b.bounds.z},
      bmin = {b.position.x - b.bounds.x, b.position.y - b.bounds.y, b.position.z - b.bounds.z};

   amax.x = (amax.x > bmax.x) ? amax.x : bmax.x;
   amax.y = (amax.y > bmax.y) ? amax.y : bmax.y;
   amax.z = (amax.z > bmax.z) ? amax.z : bmax.z;
   amin.x = (amin.x < bmin.x) ? amin.x : bmin.x;
   amin.y = (amin.y < bmin.y) ? amin.y : bmin.y;
   amin.z = (amin.z < bmin.z) ? amin.z : bmin.z;

   output.bounds.x = (amax.x - amin.x) / 2;
   output.bounds.y = (amax.y - amin.y) / 2;
   output.bounds.z = (amax.z - amin.z) / 2;

   output.position.x = amin.x + output.bounds.x;
   output.position.y = amin.y + output.bounds.y;
   output.position.z = amin.z + output.bounds.z;

   return output;
}

inline Bool
aabb_intersect(AABB a, AABB b)
{
   Vec3
      amax = {a.position.x + a.bounds.x, a.position.y + a.bounds.y, a.position.z + a.bounds.z},
      amin = {a.position.x - a.bounds.x, a.position.y - a.bounds.y, a.position.z - a.bounds.z},
      bmax = {b.position.x + b.bounds.x, b.position.y + b.bounds.y, b.position.z + b.bounds.z},
      bmin = {b.position.x - b.bounds.x, b.position.y - b.bounds.y, b.position.z - b.bounds.z};

   return
      (amin.x <= bmax.x) && (amax.x >= bmin.x) &&
      (amin.y <= bmax.y) && (amax.y >= bmin.y) &&
      (amin.z <= bmax.z) && (amax.z >= bmin.z);
}

inline Bool
aabb_intersect_sphere(AABB a, Sphere b)
{
   Vec3
      amax = {a.position.x + a.bounds.x, a.position.y + a.bounds.y, a.position.z + a.bounds.z},
      amin = {a.position.x - a.bounds.x, a.position.y - a.bounds.y, a.position.z - a.bounds.z};

  Sclr distance;
  Vec3 separation;

  separation.x = vctr_clamp(b.position.x, amin.x, amax.x) - b.position.x;
  separation.y = vctr_clamp(b.position.y, amin.y, amax.y) - b.position.y;
  separation.z = vctr_clamp(b.position.z, amin.z, amax.z) - b.position.z;
  distance = vec3_length(separation);

  // The AABB and the sphere overlap if the closest point within the rectangle is
  // within the sphere's radius.
  return distance < b.radius;

}

inline Bool
aabb_subsect(AABB a, AABB b)
{
   Vec3
      amax = {a.position.x + a.bounds.x, a.position.y + a.bounds.y, a.position.z + a.bounds.z},
      amin = {a.position.x - a.bounds.x, a.position.y - a.bounds.y, a.position.z - a.bounds.z},
      bmax = {b.position.x + b.bounds.x, b.position.y + b.bounds.y, b.position.z + b.bounds.z},
      bmin = {b.position.x - b.bounds.x, b.position.y - b.bounds.y, b.position.z - b.bounds.z};

   return
      (amax.x <= bmax.x) && (amin.x >= bmin.x) &&
      (amax.y <= bmax.y) && (amin.y >= bmin.y) &&
      (amax.x <= bmax.z) && (amin.z >= bmin.z);
}

#endif

