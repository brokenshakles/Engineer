#ifndef _ENG_MATH_ANGL_H_
#define _ENG_MATH_ANGL_H_

#include "type/angl.h"

#include "math/sclr.h"
#include "math/vctr.h"

#define SINCOS(a) angl_sincos(a)
#define SIN(a)    angl_sin(a)
#define COS(a)    angl_cos(a)
#define TAN(a)    angl_tan(a)
#define ATAN(a)   angl_atan(a)
#define ASIN(a)   angl_asin(a)

//#define SINCOSH(a) angl_sincosh(a)
#define TANH(a)    angl_tanh(a)
#define ATANH(a)   angl_atanh(a)

#define SCLR_FROM_ANGL(a) (Sclr)(a >> (Angl_RADIX - Sclr_RADIX));
#define ANGL_FROM_SCLR(a) (Angl)(a << (Angl_RADIX - Sclr_RADIX));

#define PI (Sclr)(3.1415926536897932384626 * Sclr_BASIS)

extern Sclr cordic_gain_c;
extern Sclr cordic_gain_h;

inline Angl
angl(double input)
{
   return (Angl)(input * Angl_BASIS);
}

/*** Arithmatic Transformation Functions ***/

inline Angl
angl_mult(Angl multiplicand, Angl multiplier)
{
   Angl output;

   output   = multiplicand * multiplier;
   output  += (output & ((Angl)1 << (Angl_RADIX - 1)) ) << 1;
   output >>= Angl_RADIX;

   return output;
}

inline Angl
angl_divd(Angl dividend, Angl divisor)
{
   Angl output;

   output = (dividend << Angl_RADIX) / divisor;

   return output;
}


/*** Trigonomic Transformation Functions ***/

inline Sclr
angl_sin(Angl input)
{
   // A sine approximation via a fifth-order polynomial.
   static const Angl
      A  =  PI / 2,
      B  =  PI - ((Angl)5 * Angl_BASIS) / 2,
      C  = (PI - ((Angl)3 * Angl_BASIS))/ 2,
      S  =       ((Angl)2 * Angl_BASIS) / PI;

   Angl x, x2, sin;
   x = angl_mult(input, S);

   x = x << (Angl_SCALE - Angl_RADIX - 2);            // Shift to full s64 range (Q31->Q62).
   if( (x ^ (x << 1)) < 0)  x = ((Angl)1 << 63) - x;  // Test for quadrant 1 or 2.
   x = x >> (Angl_SCALE - Angl_RADIX - 2);

   x2  =     angl_mult(x,   x);                       // Ax - Bx^3 + Cx^5
   sin = B - angl_mult(C,   x2);                      // Ax - x(Bx^2 - Cx^4)
   sin = A - angl_mult(sin, x2);                      // x(A - (Bx^2 - Cx^4)
   sin =     angl_mult(sin, x);                       // x(A - x^2(B - Cx^2))

   return SCLR_FROM_ANGL(sin);
}

inline Sclr
angl_cos(Angl input)
{
   // A cosine approximation via a fifth-order polynomial.
   static const Angl
      A  =  PI / 2,
      B  =  PI - ((Angl)5 * Angl_BASIS) / 2,
      C  = (PI - ((Angl)3 * Angl_BASIS))/ 2,
      S  =       ((Angl)2 * Angl_BASIS) / PI;

   Angl x, x2, sin;
   x  = angl_mult(input, S);
   x += Angl_BASIS;                                   // Sine -> cosine calculation.

   x = x << (Angl_SCALE - Angl_RADIX - 2);            // Shift to full s64 range (Q31->Q62).
   if( (x ^ (x << 1)) < 0)  x = ((Angl)1 << 63) - x;  // Test for quadrant 1 or 2.
   x = x >> (Angl_SCALE - Angl_RADIX - 2);

   x2  =     angl_mult(x,   x);                       // Ax - Bx^3 + Cx^5
   sin = B - angl_mult(C,   x2);                      // Ax - x(Bx^2 - Cx^4)
   sin = A - angl_mult(sin, x2);                      // x(A - (Bx^2 - Cx^4)
   sin =     angl_mult(sin, x);                       // x(A - x^2(B - Cx^2))

   return SCLR_FROM_ANGL(sin);
}

inline Sclr
angl_tan(Angl input)
{
   Sclr output;

   output = sclr_divd(angl_sin(input), angl_cos(input));

   return output;

}

inline Angl
angl_asin(Vctr input)
{
  // Domain : -1 < input < 1 units.  All other values will be truncated.
  static const Vctr
     A = ((Angl_BASIS * 1) << Angl_RADIX) / (Angl_BASIS *   6),
     B = ((Angl_BASIS * 3) << Angl_RADIX) / (Angl_BASIS *  40),
     C = ((Angl_BASIS * 5) << Angl_RADIX) / (Angl_BASIS * 112);

  Angl x, x2, x3, x5, x7;
  x   = (Angl)vctr_abs(input);
  x <<= Vctr_SCALE - Vctr_RADIX - 2; // Shift to full s64 range (Q16->Q62).
  x >>= Angl_SCALE - Angl_RADIX - 2; // Shift back to Angl's radix. Truncation happens in prev line.

  x2 = angl_mult(x,   x);  x3 = angl_mult(x,  x2);
  x5 = angl_mult(x3, x2);  x7 = angl_mult(x5, x2);

  return x + angl_mult(x3, A) + angl_mult(x5, B) + angl_mult(x7, C);
}
/*
inline Sclr
angl_acos(Angl input)
{
}
*/
inline Angl
angl_atan(Vctr input)
{
   // Domain: all a
   Vctr x = Angl_BASIS,
        y = input,
        z = 0;

   cordic_circular_ymode(&x, &y, &z);
   return ANGL_FROM_SCLR(z);
}


/*** Hyperbolic Functions ***/

inline Sclr
angl_sinh(Angl input)
{
   // Domain: |a| < 1.13 OR |a| <= 1.125, after scaling,
   Vctr x = cordic_gain_h, // Cosh()
        y = 0,             // Sinh()
        z = input;

   cordic_hyperbolic_zmode(&x, &y, &z);

   return x;
}

inline Sclr
angl_cosh(Angl input)
{
   // Domain: |a| < 1.13 OR |a| <= 1.125, after scaling,
   Angl x = cordic_gain_h, // Cosh()
        y = 0,             // Sinh()
        z = input;

   cordic_hyperbolic_zmode(&x, &y, &z);

   return y;
}

inline Sclr
angl_tanh(Angl input)
{
   // Domain: |a| < 1.13 OR |a| <= 1.125, after scaling,
   Angl x = cordic_gain_h, // Cosh()
        y = 0,             // Sinh()
        z = input;

   cordic_hyperbolic_zmode(&x, &y, &z);

   return angl_divd(y, x);
}

inline Sclr
angl_atanh(Angl input)
{
   // Domain: |a| < 1.13 units.
   Angl x = Angl_BASIS,
        y = input,
        z = 0;  // Atanh()

   cordic_hyperbolic_ymode(&x, &y, &z);

   return z;
}

/*
inline Angl
angl_asin(Vctr input)
   // Domain: |a| < 0.98
   Vctr x = cordic_gain_c,
        y = input,
        z = 0;

   cordic_circular_aymode(&x, &y, &z);
   return SCLR_FROM_ANGL(z);
*/

#endif

