#ifndef _ENG_MATH_QUAT_H_
#define _ENG_MATH_QUAT_H_

#include "type/quat.h"

#include "math/angl.h"
#include "math/vec3.h"

inline Quat
quat(double x, double y, double z, double a)
{
   Quat output;
   double sinbuffer, xsqr, ysqr, zsqr, l;

   // Normalize our vector before submitting it.
   xsqr = x * x;
   ysqr = y * y;
   zsqr = z * z;
   l =  1 / sqrt(xsqr + ysqr + zsqr);
   x = x * l;
   y = y * l;
   z = z * l;

   // Convert our Axis/Angle input into Quaternion form.
   sinbuffer = sin(a / 2);
   a  = cos(a / 2);
   x *= sinbuffer;
   y *= sinbuffer;
   z *= sinbuffer;

   output.w = (Vctr)(a * Vctr_BASIS);
   output.x = (Vctr)(x * Vctr_BASIS);
   output.y = (Vctr)(y * Vctr_BASIS);
   output.z = (Vctr)(z * Vctr_BASIS);

   return output;
}

inline Quat
quat_pure(Vec3 input)
{
   Quat output = {
      0,
      input.x,
      input.y,
      input.z
   };

   return output;
}

inline Quat
quat_multiply(Quat multiplier, Quat multiplicand)
{
   #define MULT vctr_mult
   #define q1   multiplier
   #define q2   multiplicand

   Quat output;

   output.x = MULT(q1.w, q2.x) + MULT(q1.x, q2.w) + MULT(q1.y, q2.z) - MULT(q1.z, q2.y);
   output.y = MULT(q1.w, q2.y) - MULT(q1.x, q2.z) + MULT(q1.y, q2.w) + MULT(q1.z, q2.x);
   output.z = MULT(q1.w, q2.z) + MULT(q1.x, q2.y) - MULT(q1.y, q2.x) + MULT(q1.z, q2.w);
   output.w = MULT(q1.w, q2.w) - MULT(q1.x, q2.x) - MULT(q1.y, q2.y) - MULT(q1.z, q2.z);

   return output;
   #undef q1
   #undef q2
   #undef MULT
}

inline Quat
quat_invert(Quat input)
{
   Quat output;

   output.w =  input.w;
   output.x = -input.x;
   output.y = -input.y;
   output.z = -input.z;

   return output;
}

inline Quat
quat_normalize(Quat input)
{
   Quat output;
   Vctr basis,  inverse;
   Sclr square, sqrt;

   basis    = Vctr_BASIS;
   square   = vctr_mult(input.x, input.x) + vctr_mult(input.y, input.y)
            + vctr_mult(input.z, input.z) + vctr_mult(input.w, input.w);
   sqrt     = sclr_sqrt(square);
   inverse  = vctr_divd(basis, sqrt);
   output.x = vctr_mult(input.x, inverse);
   output.y = vctr_mult(input.y, inverse);
   output.z = vctr_mult(input.z, inverse);
   output.w = vctr_mult(input.w, inverse);

   return output;
}

// Note : BOTH Vec3 inputs MUST be normalized before invocation!
inline Quat
quat_from_orientations(Vec3 u, Vec3 v)
{
   Vec3 w = vec3_cross(u, v);
   Quat q = { Vctr_BASIS + vec3_dot(u, v),  w.x,  w.y,  w.z };
   return quat_normalize(q);
}

inline Quat
quat_from_basis(Vec3 v)
{
   return quat_from_orientations(vec3(0, Vctr_BASIS, 0), v);
}

#endif

