#ifndef _ENG_MATH_VEC2_H_
#define _ENG_MATH_VEC2_H_

#include "type/vec2.h"

#include "math/vctr.h"

inline Vec2
vec2(double x, double y)
{
   Vec2 output;

   output.x = (Vctr)(x * Vctr_BASIS);
   output.y = (Vctr)(y * Vctr_BASIS);

   return output;
}

inline Vec2
vec2_mult(Vec2 input_a, Vec2 input_b)
{
   Vec2 output;

   output.x = vctr_mult(input_a.x, input_b.x);
   output.y = vctr_mult(input_a.y, input_b.y);

   return output;
}

inline Vec2
vec2_divd(Vec2 input_a, Vec2 input_b)
{
   Vec2 output;

   output.x = vctr_divd(input_a.x, input_b.x);
   output.y = vctr_divd(input_a.y, input_b.y);

   return output;
}

inline Sclr
vec2_dot(Vec2 input_a, Vec2 input_b)
{
   Sclr output;

   output = vctr_mult(input_a.x, input_b.x)
          + vctr_mult(input_a.y, input_b.y);

   return output;
}

inline Sclr
vec2_length(Vec2 input)
{
   Sclr output;

   output = sclr_sqrt(vec2_dot(input, input));

   return output;
}

inline Vec2
vec2_scale(Vec2 input, Sclr factor)
{
   Vec2 output;

   output.x = vctr_mult(input.x, factor);
   output.y = vctr_mult(input.y, factor);

   return output;
}

inline Vec2
vec2_normalize(Vec2 input)
{
   Vec2 output;
   Vctr basis, length, inverse;

   basis    = Vctr_BASIS;
   length   = vec2_length(input);
   inverse  = vctr_divd(basis, length);
   output   = vec2_scale(input, inverse);

   return output;
}

inline Vec2
vec2_invert(Vec2 input)
{
   Vec2 output;

   output.x = -input.x;
   output.y = -input.y;

   return output;
}

#endif

