#ifndef _ENG_MATH_MTRX_H_
#define _ENG_MATH_MTRX_H_

#include "type/mtrx.h"

#include "math/vec3.h"
#include "math/quat.h"

// This is some boilerplate used to determine the number of arguments passed to a variadic function.
#define PP_NARG(...)  PP_NARG_(__VA_ARGS__, PP_RSEQ_N())
#define PP_NARG_(...) PP_ARG_N(__VA_ARGS__)

#define PP_ARG_N( _1, _2, _3, _4, _5, _6, _7, _8, _9,_10,_11,_12,_13,_14,_15,_16, \
                 _17,_18,_19,_20,_21,_22,_23,_24,_25,_26,_27,_28,_29,_30,_31,_32, \
                 _33,_34,_35,_36,_37,_38,_39,_40,_41,_42,_43,_44,_45,_46,_47,_48, \
                 _49,_50,_51,_52,_53,_54,_55,_56,_57,_58,_59,_60,_61,_62,_63,  N, \
                 ...) N

#define PP_RSEQ_N() 63,62,61,60,59,58,57,56,55,54,53,52,51,50,49,48, \
                    47,46,45,44,43,42,41,40,39,38,37,36,35,34,33,32, \
                    31,30,29,28,27,26,25,24,23,22,21,20,19,18,17,16, \
                    15,14,13,12,11,10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0

#define MULT           vctr_mult
#define MTRX_MULT(...) mtrx_mult(PP_NARG(__VA_ARGS__), __VA_ARGS__)

inline Mtrx
mtrx_identity()
{
   const Mtrx identity =
   {
      Sclr_BASIS,           0,           0,           0,
               0,  Sclr_BASIS,           0,           0,
               0,           0,  Sclr_BASIS,           0,
               0,           0,           0,  Sclr_BASIS
   };

   return identity;
}

inline Mtrx
mtrx_transpose(Mtrx input)
{
   Mtrx output;

   output.r0c0 = input.r0c0;  output.r0c1 = input.r1c0;
   output.r1c0 = input.r0c1;  output.r1c1 = input.r1c1;
   output.r2c0 = input.r0c2;  output.r2c1 = input.r1c2;
   output.r3c0 = input.r0c3;  output.r3c1 = input.r1c3;

   output.r0c2 = input.r2c0;  output.r0c3 = input.r3c0;
   output.r1c2 = input.r2c1;  output.r1c3 = input.r3c1;
   output.r2c2 = input.r2c2;  output.r2c3 = input.r3c2;
   output.r3c2 = input.r2c3;  output.r3c3 = input.r3c3;

   return output;
}

inline Mtrx
mtrx_mult(int count, ...)
{
   Mtrx     multiplicand, multiplier, output;
   int      arg, row, col, cell, sum = 0;
   Sclr    *input_row, *input_col, *output_cell;
   va_list  args;

   va_start(args, count);
   output = va_arg(args, Mtrx); // Set up our initial multiplier.
   for(arg = 1; arg < count; arg++)
   {
      multiplicand = mtrx_transpose(va_arg(args, Mtrx));
      multiplier   = output;

      memset(&output, 0, sizeof(Mtrx));
      output_cell = (Sclr*)&output;

      for(row = 0; row < 4; row++)
      {
         input_row = ((Sclr*)&multiplier) + (row * 4);

         for(col = 0; col < 4; col++)
         {
            input_col = ((Sclr*)&multiplicand) + (col * 4);

            for(cell = 0; cell < 4; cell++) sum += MULT(*(input_row + cell), *(input_col + cell));

            *output_cell = sum; output_cell++; sum = 0;
         }
      }
   }

   va_end(args);
   return output;
}

// This should be renamed eng_math_quat_mult_by_mtrx() and moved into quat.c.
inline Quat
mtrx_mult_by_quat(Mtrx multiplicand, Quat multiplier)
{
   Quat output;

   output.x = MULT(multiplicand.r0c0, multiplier.x) + MULT(multiplicand.r0c1, multiplier.y) +
              MULT(multiplicand.r0c2, multiplier.z) + MULT(multiplicand.r0c3, multiplier.w);

   output.y = MULT(multiplicand.r1c0, multiplier.x) + MULT(multiplicand.r1c1, multiplier.y) +
              MULT(multiplicand.r1c2, multiplier.z) + MULT(multiplicand.r1c3, multiplier.w);

   output.z = MULT(multiplicand.r2c0, multiplier.x) + MULT(multiplicand.r2c1, multiplier.y) +
              MULT(multiplicand.r2c2, multiplier.z) + MULT(multiplicand.r2c3, multiplier.w);

   output.w = MULT(multiplicand.r3c0, multiplier.x) + MULT(multiplicand.r3c1, multiplier.y) +
              MULT(multiplicand.r3c2, multiplier.z) + MULT(multiplicand.r3c3, multiplier.w);

   return output;
}

inline Mtrx
mtrx_from_position(Vec3 input)
{
   Mtrx output = mtrx_identity();

   output.r3c0 = input.x;
   output.r3c1 = input.y;
   output.r3c2 = input.z;

   return output;
}

inline Mtrx
mtrx_from_scale(Vec3 input)
{
   Mtrx output = mtrx_identity();

   output.r0c0 = input.x;
   output.r1c1 = input.y;
   output.r2c2 = input.z;

   return output;
}

inline Mtrx
mtrx_from_quat(Quat input)
{
   Mtrx output;
   Vctr factor, twoW, twoX, twoY, wSq, xSq, ySq, zSq, xy, xz, yz, wx, wy, wz;

   // Helper quantities, these are calculated up front to avoid redundancies.
   factor = Vctr_BASIS << 1;
   twoW = MULT(input.w, factor);  twoX = MULT(input.x, factor);  twoY = MULT(input.y, factor);

   wSq = MULT(input.w, input.w);  xSq = MULT(input.x, input.x);
   ySq = MULT(input.y, input.y);  zSq = MULT(input.z, input.z);

   xy = MULT(twoX, input.y);  xz = MULT(twoX, input.z);  yz = MULT(twoY, input.z);
   wx = MULT(twoW, input.x);  wy = MULT(twoW, input.y);  wz = MULT(twoW, input.z);

   // Here is where we build the output Mtrx.
   output.r0c0 = wSq + xSq - ySq - zSq;  output.r1c0 = xy  + wz;
   output.r0c1 = xy  - wz;               output.r1c1 = wSq - xSq + ySq - zSq;
   output.r0c2 = xz  + wy;               output.r1c2 = yz  - wx;
   output.r0c3 = 0;                      output.r1c3 = 0;

   output.r2c0 = xz  - wy;               output.r3c0 = 0;
   output.r2c1 = yz  + wx;               output.r3c1 = 0;
   output.r2c2 = wSq - xSq - ySq + zSq;  output.r3c2 = 0;
   output.r2c3 = 0;                      output.r3c3 = Vctr_BASIS;

   return output;
}

#undef MULT

#endif

