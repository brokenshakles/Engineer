#ifndef _ENG_MATH_VCTR_H_
#define _ENG_MATH_VCTR_H_

#include "type/vctr.h"

#include "math/sclr.h"

inline Vctr
vctr(double input)
{
   return (Vctr)(input * Vctr_BASIS);
}

inline Vctr
vctr_mult(Vctr multiplicand, Vctr multiplier)
{
   Vctr output;

   output   = multiplicand * multiplier;
   output  += (output & (2 * (Vctr_RADIX - 1)) ) * 2;
   output >>= Vctr_RADIX;

   return output;
}

inline Vctr
vctr_divd(Vctr dividend, Vctr divisor)
{
   Vctr output;

   output = (dividend << Vctr_RADIX) / divisor;

   return output;
}

inline Sclr
vctr_abs(Vctr input)
{
   Sclr output;

   output = (input ^ (input >> (Vctr_SCALE - 1))) - (input >> (Vctr_SCALE - 1));

   return output;
}

inline Vctr
vctr_clamp(Vctr input, Vctr min, Vctr max)
{
   Vctr buffer, output;

   min <<= 1;
   max <<= 1;

   buffer = input - min;
   input  = ((input + min) - vctr_abs(buffer)) / 2;
   buffer = input - max;
   output = ((input + max) + vctr_abs(buffer)) / 2;

   return output;
}

#endif

