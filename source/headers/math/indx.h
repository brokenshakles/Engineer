#ifndef _ENG_MATH_INDX_H_
#define _ENG_MATH_INDX_H_

#include "type/indx.h"

inline Indx
indx(double input)
{
   return (Indx)(input * Indx_BASIS);
}

#endif

