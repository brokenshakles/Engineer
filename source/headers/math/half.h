#ifndef _ENG_MATH_HALF_H_
#define _ENG_MATH_HALF_H_

#include "type/half.h"

inline Half
half(double input)
{
   return (Half)(input * Half_BASIS);
}

#endif

