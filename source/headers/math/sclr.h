#ifndef _ENG_MATH_SCLR_H_
#define _ENG_MATH_SCLR_H_

#include "type/sclr.h"

#include "tool/cordic.h"

extern Sclr cordic_gain_h;

inline Sclr
sclr(double input)
{
   return (Sclr)(input * Sclr_BASIS);
}

inline Sclr
sclr_mult(Sclr multiplicand, Sclr multiplier)
{
   Sclr output;

   output   = multiplicand * multiplier;
   output  += (output & (2 * (Sclr_RADIX - 1)) ) * 2;
   output >>= Sclr_RADIX;

   return output;
}

inline Sclr
sclr_divd(Sclr dividend, Sclr divisor)
{
   Sclr output;

   output = (dividend << Sclr_RADIX) / divisor;

   return output;
}

inline Sclr
sclr_clamp(Sclr input, Sclr min, Sclr max)
{
   Sclr buffer, output;

   min <<= 1;
   max <<= 1;

   buffer = input - min;
   input  = ((input + min) - buffer) / 2;
   buffer = input - max;
   output = ((input + max) + buffer) / 2;

   return output;
}

inline Sclr
sclr_exp(Sclr input) // Fixme //
{
   Vctr x = cordic_gain_h, // Cosh()
        y = 0,             // Sinh()
        z = input;

   cordic_hyperbolic_zmode(&x, &y, &z);

   return (Sclr)(y + x);
}

inline Sclr
sclr_ln(Sclr input) // Fixme //
{
   // Domain: 0.1 < a < 9.58 units.
   Vctr x = input + Sclr_BASIS,
        y = input - Sclr_BASIS,
        z = 0;

   cordic_hyperbolic_ymode(&x, &y, &z);

   return (Sclr)(z * 2);
}

inline Sclr
sclr_sqrt(Sclr input)
{
   // This is a branchless variant of Christophe Meessen's shift-and-add algorithim
   //    for approximating square roots of fixed point numbers.
   Sclr x, y, z, output, mask;

   x = input;
   y = (Sclr)1 << (Sclr_RADIX + 14);
   z = 0;

   for (uint32_t i = 0; i < (Sclr_SCALE - 8); i++)
   {
      output =  z + y;
      mask   = (x - output) >> (Sclr_SCALE - 1);
      x      =  x         - ((    output) & ~mask);
      z      = (z & mask) + ((y + output) & ~mask);
      x      =  x << 1;
      y      =  y >> 1;
   }
   output = z >> 8;

   return output;
}

#endif

